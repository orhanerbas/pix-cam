//
//  BanubaLocal.swift
//  PixCam
//
//  Created by Orhan Erbas on 26.07.2021.
//

import Foundation
import UIKit

struct BanubaLocal {
    let id: Int
    let title: String
    let img: UIImage?
}

struct ToonifyLocal {
    let id: Int
    let title: String
    let img: UIImage?
}

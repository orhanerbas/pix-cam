//
//  Model.swift
//  PixCam
//
//  Created by Orhan Erbas on 20.05.2021.
//

import Foundation
import UIKit
import Firebase
import SwiftyJSON

struct BanubaEffects {
    let id: Int
    let title: String?
    let description: String?
    let imageURL: String!
    let isPro: Bool?
    let isNew: Bool?
    
    init(id: Int, title: String, description: String, imageURL: String, isPro: Bool, isNew: Bool) {
        self.id = id
        self.title = title
        self.description = description
        self.imageURL = imageURL
        self.isPro = isPro
        self.isNew = isNew
    }
    
    init(snapshot: DataSnapshot) {
        let json = JSON(snapshot.value!)
        self.id = json["id"].intValue
        self.title = json["title"].stringValue
        self.description = json["description"].stringValue
        self.imageURL = json["imageURL"].stringValue
        self.isPro = json["isPro"].boolValue
        self.isNew = json["isNew"].boolValue
    }
}

//
//  Mask.swift
//  PixCam
//
//  Created by Orhan Erbas on 22.06.2021.
//

import Foundation
import UIKit
import Firebase
import SwiftyJSON

struct Mask {
    let id: Int
    let path: String?
    
    init(id: Int, path: String) {
        self.id = id
        self.path = path
    }
    
    init(snapshot: DataSnapshot) {
        let json = JSON(snapshot.value!)
        self.id = json["id"].intValue
        self.path = json["path"].stringValue
    }
}

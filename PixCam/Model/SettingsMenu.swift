//
//  SettingsMenu.swift
//  PixCam
//
//  Created by Orhan Erbas on 26.05.2021.
//

import Foundation

import UIKit

struct SettingsMenu {
    let id: Int
    let menuTitle: String
    let menuDescription: String
    let menuArrow: UIImage
    let bgColor: UIColor
    let titleColor: UIColor
}

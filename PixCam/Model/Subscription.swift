//
//  Subscription.swift
//  PixCam
//
//  Created by Orhan Erbas on 14.08.2021.
//

import Foundation
import UIKit

struct SubscriptionModel {
    var id : Int?
    var name: String?
    var price: String?
    var isWeekly: Bool?
    var bgImage: UIImage?
    var detailText: String?
    
    init(id: Int?, name: String?, named bgImage: String?, isWeeklyFormat: Bool?, price: String?, detailText: String?) {
        self.id = id
        self.name = name ?? "unrecognized"
        self.bgImage = UIImage(named: bgImage ?? "nil")
        self.isWeekly = isWeeklyFormat
        self.price = price
        self.detailText = detailText
    }
}

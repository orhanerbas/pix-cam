//
//  ShareIcons.swift
//  PixCam
//
//  Created by Orhan Erbas on 26.05.2021.
//

import Foundation
import UIKit

struct ShareIcons {
    let id: Int
    let img: UIImage
    let text: String
}

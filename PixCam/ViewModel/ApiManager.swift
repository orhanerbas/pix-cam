//
//  ApiManager.swift
//  PixCam
//
//  Created by Orhan Erbas on 18.06.2021.
//

import Foundation
import Firebase

class ApiManager {
    
    static let instance = ApiManager()
    private init() { }
    
    func getBanubaEffects(success successCallback: @escaping (BanubaEffects) -> Void) {
        Database.database().reference().child("BanubaEffects").observe(.childAdded) { (snapshot) in
            let data = BanubaEffects(snapshot: snapshot)
            DispatchQueue.main.async {
                successCallback(data)
            }
        }
    }
    
    func getToonifyEffects(success successCallback: @escaping (ToonifyEffects) -> Void) {
        Database.database().reference().child("ToonifyEffects").observe(.childAdded) { (snapshot) in
            let data = ToonifyEffects(snapshot: snapshot)
            DispatchQueue.main.async {
                successCallback(data)
                NotificationCenter.default.post(name: NSNotification.Name.init("loadedData"), object: nil, userInfo: nil)
            }
            
        }
      
    }
    
    func getMasks(completionHandler: @escaping(Result<Mask, Error>) -> Void) {
        Database.database().reference().child("MaskEffects").observe(.childAdded) { (snapshot) in
            let datas = Mask(snapshot: snapshot)
            DispatchQueue.main.async { 
                let splited = datas.path?.split(separator: "%")[1].split(separator: "-")[0]
                print("mask effects = ", splited ?? "")
                completionHandler(.success(datas))
                FileDownloader().downloadFiles(folderName: "MaskEffects", fileName: String(splited!), fileUrl: String(datas.path!)) { int, error in }
            }
        }
    }
     
    func getLips(success successCallback: @escaping (LipsEffects) -> Void) {
        /*
         Database.database().reference().child("LipsEffects").observe(.childAdded) { (snapshot) in
             let datas = LipsEffects(snapshot: snapshot)
             DispatchQueue.main.async {
                 let splited = datas.path?.split(separator: "%")[1].split(separator: ".")[0]
                 self.getHairs { succes in
                     print(succes)
                 }
             }
         }
         */
    }
    
    func getHairs(success successCallback: @escaping (LipsEffects) -> Void) {
        Database.database().reference().child("HairEffects").observe(.childAdded) { (snapshot) in
            let datas = LipsEffects(snapshot: snapshot)
            DispatchQueue.main.async {
                let splited = datas.path?.split(separator: "%")[1].split(separator: ".")[0]
                print("hair effects = ", String(datas.path!))
                
                //FileDownloader().downloadFiles(folderName: "MaskEffects", fileName: String(splited!), fileUrl: String(datas.path!)) { int, error in }
            }
        }
    }
    
    func getMakeup(success successCallback: @escaping (LipsEffects) -> Void) {
        Database.database().reference().child("MakeupEffect").observe(.childAdded) { (snapshot) in
            let datas = LipsEffects(snapshot: snapshot)
            DispatchQueue.main.async {
                let splited = datas.path?.split(separator: "%")[1].split(separator: ".")[0]
                print("Makeup effects = ",  splited)
                
                FileDownloader().downloadFiles(folderName: "MaskEffects/Makeup", fileName: String(splited!), fileUrl: String(datas.path!)) { int, error in }
            }
        }
    }
}

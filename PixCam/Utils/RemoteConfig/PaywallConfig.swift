//
//  PaywallConfig.swift
//  PixCam
//
//  Created by Orhan Erbas on 5.07.2021.
//

import Foundation
import FirebaseRemoteConfig

class PaywallConfig {
    private let remoteConfig = RemoteConfig.remoteConfig()
    static let instance = PaywallConfig()
    private init() { }

     func fetchPaywallUIValue() {
        let group = DispatchGroup()
        group.enter()
        let defaults : [String: NSObject] = [
            "subscriptionShowSingleButton": true as NSObject
        ]
        
        remoteConfig.setDefaults(defaults)
        
        let settings = RemoteConfigSettings()
        settings.minimumFetchInterval = 0
        remoteConfig.configSettings = settings
        group.leave()
        
        group.notify(queue: .main) {
            
            self.remoteConfig.fetch { (status, error) -> Void in
              if status == .success {
                print("Config fetched!")
                self.remoteConfig.activate { changed, error in
                    let subscriptionShowSingleButton = self.remoteConfig.configValue(forKey: "subscriptionShowSingleButton").numberValue
                    Definations.subscriptionShowSingleButton = Int(truncating: subscriptionShowSingleButton)
                    
                    let subscriptionCTATitle = self.remoteConfig.configValue(forKey: "subscriptionCTATitle").stringValue
                    if !(subscriptionCTATitle ?? "").isEmpty {
                        Definations.subscriptionCTATitle = String(subscriptionCTATitle ?? "Try Free & Subscribe".localized())
                    } else {
                        Definations.subscriptionCTATitle = "Try Free & Subscribe".localized()
                    }
                    
                    let subscriptionShowPrice = self.remoteConfig.configValue(forKey: "subscriptionShowPrice").boolValue
                    Definations.subscriptionShowPrice = subscriptionShowPrice
                    
                    let subscriptionCloseAppearTime = self.remoteConfig.configValue(forKey: "subscriptionCloseAppearTime").numberValue
                    Definations.subscriptionCloseAppearTime = Double(truncating: subscriptionCloseAppearTime)
                    
                    let subscriptionHeader = self.remoteConfig.configValue(forKey: "subscriptionHeader").stringValue
                    if !(subscriptionHeader ?? "").isEmpty {
                        Definations.subscriptionHeader = String(subscriptionHeader ?? "Pixcam Premium".localized())
                    } else {
                        Definations.subscriptionHeader = " Pixcam Premium".localized()
                    }
                    
                    let subscriptionDescription = self.remoteConfig.configValue(forKey: "subscriptionDescription").stringValue
                    if !(subscriptionDescription ?? "").isEmpty {
                        Definations.subscriptionDescription = String(subscriptionDescription ?? "Join the fun with the Premium Package.".localized())
                    } else {
                        Definations.subscriptionDescription = "Join the fun with the Premium Package.".localized()
                    }
                    
                    let subscriptionCTAButtonColor = self.remoteConfig.configValue(forKey: "subscriptionCTAButtonColor").stringValue
                    if !(subscriptionCTAButtonColor ?? "").isEmpty {
                        Definations.subscriptionCTAButtonColor = String(subscriptionCTAButtonColor ?? "")
                    } else {
                        Definations.subscriptionCTAButtonColor = ""
                    }
                    
                    let subscriptionCTATextColor = self.remoteConfig.configValue(forKey: "subscriptionCTATextColor").stringValue
                    if !(subscriptionCTATextColor ?? "").isEmpty {
                        Definations.subscriptionCTATextColor = String(subscriptionCTATextColor ?? "")
                    } else {
                        Definations.subscriptionCTATextColor = ""
                    }
                    
                    let subscriptionBGcolor = self.remoteConfig.configValue(forKey: "subscriptionBGcolor").stringValue
                    if !(subscriptionBGcolor ?? "").isEmpty {
                        Definations.subscriptionBGcolor = String(subscriptionBGcolor ?? "")
                    } else {
                        Definations.subscriptionBGcolor = ""
                    }
                    
                    let SplashHeader_1 = self.remoteConfig.configValue(forKey: "SplashHeader_1").stringValue
                    if !(SplashHeader_1 ?? "").isEmpty {
                        Definations.SplashHeader_1 = String(SplashHeader_1 ?? "Try the masks".localized())
                    } else {
                        Definations.SplashHeader_1 = "Try the masks".localized()
                    }
                    
                    let SplashHeader_2 = self.remoteConfig.configValue(forKey: "SplashHeader_2").stringValue
                    if !(SplashHeader_2 ?? "").isEmpty {
                        Definations.SplashHeader_2 = String(SplashHeader_2 ?? "Turn your photos".localized())
                    } else {
                        Definations.SplashHeader_2 = "Turn your photos".localized()
                    }
                    
                    let SplashHeader_3 = self.remoteConfig.configValue(forKey: "SplashHeader_3").stringValue
                    if !(SplashHeader_3 ?? "").isEmpty {
                        Definations.SplashHeader_3 = String(SplashHeader_3 ?? "Change your".localized())
                    } else {
                        Definations.SplashHeader_3 = "Change your".localized()
                    }
                    
                    let SplashHeaderDescription_1 = self.remoteConfig.configValue(forKey: "SplashHeaderDescription_1").stringValue
                    if !(SplashHeaderDescription_1 ?? "").isEmpty {
                        Definations.SplashHeaderDescription_1 = String(SplashHeaderDescription_1 ?? "and have fun!".localized())
                    } else {
                        Definations.SplashHeaderDescription_1 = "and have fun!".localized()
                    }
                    
                    let SplashHeaderDescription_2 = self.remoteConfig.configValue(forKey: "SplashHeaderDescription_2").stringValue
                    if !(SplashHeaderDescription_2 ?? "").isEmpty {
                        Definations.SplashHeaderDescription_2 = String(SplashHeaderDescription_2 ?? "into cartoon!".localized())
                    } else {
                        Definations.SplashHeaderDescription_2 = "into cartoon!".localized()
                    }
                    
                    let SplashHeaderDescription_3 = self.remoteConfig.configValue(forKey: "SplashHeaderDescription_3").stringValue
                    if !(SplashHeaderDescription_3 ?? "").isEmpty {
                        Definations.SplashHeaderDescription_3 = String(SplashHeaderDescription_3 ?? "hair & lip color".localized())
                    } else {
                        Definations.SplashHeaderDescription_3 = "hair & lip color".localized()
                    }
                    
                    let SplashButtonText_1 = self.remoteConfig.configValue(forKey: "SplashButtonText_1").stringValue
                    if !(SplashButtonText_1 ?? "").isEmpty {
                        Definations.SplashButtonText_1 = String(SplashButtonText_1 ?? "Try the masks".localized())
                    } else {
                        Definations.SplashButtonText_1 = "Try the masks".localized()
                    }
                    
                    let SplashButtonText_2 = self.remoteConfig.configValue(forKey: "SplashButtonText_2").stringValue
                    if !(SplashButtonText_2 ?? "").isEmpty {
                        Definations.SplashButtonText_2 = String(SplashButtonText_2 ?? "Try the filters".localized())
                    } else {
                        Definations.SplashButtonText_2 = "Try the filters".localized()
                    }
                    
                    let SplashButtonText_3 = self.remoteConfig.configValue(forKey: "SplashButtonText_3").stringValue
                    if !(SplashButtonText_3 ?? "").isEmpty {
                        Definations.SplashButtonText_3 = String(SplashButtonText_3 ?? "Change the color".localized())
                    } else {
                        Definations.SplashButtonText_3 = "Change the color".localized()
                    }
                    
                    let SplashButtonTextColor = self.remoteConfig.configValue(forKey: "SplashButtonTextColor").stringValue
                    if !(SplashButtonTextColor ?? "").isEmpty {
                        Definations.SplashButtonTextColor = String(SplashButtonTextColor ?? "F6F7FA")
                    } else {
                        Definations.SplashButtonTextColor = "F6F7FA"
                    }
                    
                    let SplashButtonColor = self.remoteConfig.configValue(forKey: "SplashButtonColor").stringValue
                    if !(SplashButtonColor ?? "").isEmpty {
                        Definations.SplashButtonColor = String(SplashButtonColor ?? "")
                    } else {
                        Definations.SplashButtonColor = ""
                    }
                    
                    let subscriptionShowPaywallFirst = self.remoteConfig.configValue(forKey: "subscriptionShowPaywallFirst").boolValue
                    Definations.subscriptionShowPaywallFirst = subscriptionShowPaywallFirst
                    
                    let subscriptionMainText_1 = self.remoteConfig.configValue(forKey: "subscriptionMainText_1").stringValue
                    if !(subscriptionMainText_1 ?? "").isEmpty {
                        Definations.subscriptionMainText_1 = String(subscriptionMainText_1 ?? "No ads".localized())
                    } else {
                        Definations.subscriptionMainText_1 = "No ads".localized()
                    }
                    
                    let subscriptionMainText_2 = self.remoteConfig.configValue(forKey: "subscriptionMainText_2").stringValue
                    if !(subscriptionMainText_2 ?? "").isEmpty {
                        Definations.subscriptionMainText_2 = String(subscriptionMainText_2 ?? "Access all the filters".localized())
                    } else {
                        Definations.subscriptionMainText_2 = "Access all the filters".localized()
                    }
                    
                    let subscriptionMainText_3 = self.remoteConfig.configValue(forKey: "subscriptionMainText_3").stringValue
                    if !(subscriptionMainText_3 ?? "").isEmpty {
                        Definations.subscriptionMainText_3 = String(subscriptionMainText_3 ?? "Contents update every week".localized())
                    } else {
                        Definations.subscriptionMainText_3 = "Contents update every week".localized()
                    }
                    
                    let subscriptionRewardedInterstitial = self.remoteConfig.configValue(forKey: "subscriptionRewardedInterstitial").boolValue
                    Definations.subscriptionRewardedInterstitial = subscriptionRewardedInterstitial
                    Definations.paywallConnectIsEnd = true
                    NotificationCenter.default.post(name: NSNotification.Name.init("paywallOnEnd"), object: nil, userInfo: nil)
                }
              } else {
                print("Config not fetched")
                print("Error: \(error?.localizedDescription ?? "No error available.")")
              }
            }
        }
    }
}

//
//  Extensions.swift
//  PixCam
//
//  Created by Orhan Erbas on 20.05.2021.
//

import Foundation
import UIKit
import Purchases

extension UIView {
    func addShadow(with color: UIColor) {
        
        layer.shadowColor = color.cgColor
        layer.shadowRadius = 5
        layer.shadowOpacity = 0.2
        layer.shadowOffset = CGSize(width: 2, height: 2)
        
    }
    
    func removeShadow() {
        layer.shadowOpacity = 0
    }
    
    func dropShadow(shadowColor: UIColor = UIColor.black,
                    fillColor: UIColor = UIColor.white,
                    opacity: Float            = 0.2,
                    offset: CGSize            = CGSize(width: 0.0, height: 1.0),
                    radius: CGFloat           = 10) -> CAShapeLayer {
        
        let shadowLayer           = CAShapeLayer()
        shadowLayer.path          = UIBezierPath(roundedRect: self.bounds, cornerRadius: radius).cgPath
        shadowLayer.fillColor     = fillColor.cgColor
        shadowLayer.shadowColor   = shadowColor.cgColor
        shadowLayer.shadowPath    = shadowLayer.path
        shadowLayer.shadowOffset  = offset
        shadowLayer.shadowOpacity = opacity
        shadowLayer.shadowRadius  = radius
        layer.insertSublayer(shadowLayer, at: 0)
        return shadowLayer
    }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
    @discardableResult
    func applyGradient(colours: [UIColor]) -> CAGradientLayer {
        return self.applyGradient(colours: colours, locations: nil)
    }

    @discardableResult
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) -> CAGradientLayer {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
        return gradient
    }

    //For animating
    func fadeIn() {
        UIView.animate(withDuration: 0.35) {
            self.alpha = 1
        } completion: { boolean in
            if self.isHidden {
                self.isHidden = false
            }
        }
    }
    
    func fadeOut() {
        UIView.animate(withDuration: 0.35) {
            self.alpha = 0
        } completion: { boolean in
            if !self.isHidden {
                self.isHidden = true
            }
        }
    }
    
    func fadeOutWithAlphaComponent(alpha: CGFloat) {
        UIView.animate(withDuration: 0.35) {
            self.alpha = alpha
        } completion: { boolean in
            if !self.isHidden {
                self.isHidden = true
            }
        }
    }
}

extension UIViewController { 
//  MARK: -Animate Popups-
    func animateIn(desiredView: UIView, mainView: UIView, popupRadius: Int?) {
        desiredView.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
        desiredView.alpha = 0
        desiredView.layer.cornerRadius = CGFloat(popupRadius ?? 0)
        mainView.backgroundColor = .black
        mainView.alpha = 0.50
        mainView.addSubview(desiredView)
        desiredView.center = mainView.center
        desiredView.centerXAnchor.constraint(equalTo: mainView.centerXAnchor).isActive = true
        desiredView.centerYAnchor.constraint(equalTo: mainView.centerYAnchor).isActive = true
       
        
        UIView.animate(withDuration: 0.1, animations: {
            desiredView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            desiredView.isHidden = false
            desiredView.alpha = 1
        })
    }
    
    func removePopup(desiredView: UIView) {
        UIView.animate(withDuration: 0.1, animations: {
            desiredView.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
            desiredView.alpha = 0
        }, completion: { _ in
            desiredView.removeFromSuperview()
        })
    }
    
    func getNewScreen(boardName: String, vcName: String) {
        guard let window = UIApplication.shared.windows.filter({$0.isKeyWindow}).first else {
            return
        }
        let storyboard = UIStoryboard(name: boardName, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: vcName)

        window.rootViewController = vc

        let options: UIView.AnimationOptions = .transitionCrossDissolve

        let duration: TimeInterval = 0.3

        UIView.transition(with: window, duration: duration, options: options, animations: {}, completion:
        { completed in
            // maybe do something on completion here
        })
    }
}


class DismissSegue: UIStoryboardSegue {
    override func perform() {
        self.source.presentingViewController?.dismiss(animated: true, completion: nil)
   }
}

extension String {
    func localized() -> String {
        return NSLocalizedString(self, tableName:"Localizable", bundle: .main, value: self, comment: self)
    }
}

//MARK: UIViewControllers

extension SubscriptionViewController { 
    //MARK: CreateViews
    func createSubscriptionButtons(subsModel: SubscriptionModel!)  {
        let thirtButton = UIView()//border'ın icindeki beyaz view
        let nameLabel = UILabel()//package name label
        let priceLabel = UILabel()//package price label
        let bgImageView = UIImageView()//package detail background image
        let descLabel = UILabel()//package detail desc label
        descLabel.widthAnchor.constraint(equalToConstant: 60).isActive = true
        descLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        let buttonGesture = UITapGestureRecognizer(target: self, action: #selector(self.changeFirstBorderState))
        let secondButtonGesture = UITapGestureRecognizer(target: self, action: #selector(self.changeSecondBorderState))
        let thirdBorderButtonGesture = UITapGestureRecognizer(target: self, action: #selector(self.changeSecondBorderState))
        
        button.addGestureRecognizer(buttonGesture)
        secondButton.addGestureRecognizer(secondButtonGesture)
        thirdBorderButton.addGestureRecognizer(thirdBorderButtonGesture)
        
        button.backgroundColor = .clear
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.white.cgColor
        button.layer.cornerRadius = 8
        button.heightAnchor.constraint(equalToConstant: self.mainStackView.frame.height).isActive = true
        button.widthAnchor.constraint(equalToConstant: self.view.frame.width / 2.0 - 28.1).isActive = true
       
        secondButton.backgroundColor = .clear
        secondButton.layer.borderWidth = 0
        secondButton.layer.borderColor = UIColor.white.cgColor
        secondButton.layer.cornerRadius = 8
        secondButton.heightAnchor.constraint(equalToConstant: self.mainStackView.frame.height).isActive = true
        secondButton.widthAnchor.constraint(equalToConstant: self.view.frame.width / 2.0 - 28.5).isActive = true
        
        thirdBorderButton.backgroundColor = .clear
        thirdBorderButton.layer.borderWidth = 0
        thirdBorderButton.layer.borderColor = UIColor.white.cgColor
        thirdBorderButton.layer.cornerRadius = 8
        thirdBorderButton.heightAnchor.constraint(equalToConstant: self.mainStackView.frame.height).isActive = true
        thirdBorderButton.widthAnchor.constraint(equalToConstant: self.view.frame.width / 2.0 - 28).isActive = true
        
       
        thirtButton.backgroundColor = .white
        thirtButton.layer.cornerRadius = 8
       
        nameLabel.text = subsModel.name
        nameLabel.font = UIFont(name: "Poppins-Bold", size: 14)
        nameLabel.textColor = .black
        
        priceLabel.text = subsModel.price
        priceLabel.font = UIFont(name: "Poppins-Bold", size: 18)
        priceLabel.textColor = .black
        
        bgImageView.contentMode = .scaleAspectFill
        bgImageView.image = subsModel.bgImage
        bgImageView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        bgImageView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
        descLabel.text = subsModel.detailText
        descLabel.font = UIFont(name: "Poppins-Regular", size: 10)
        descLabel.textColor = .white
        
        descLabel.numberOfLines = 0
        descLabel.adjustsFontSizeToFitWidth = true
        descLabel.lineBreakMode = .byWordWrapping
        descLabel.textAlignment = .center
        
        self.mainStackView.addArrangedSubview(button)
        button.leftAnchor.constraint(equalTo: self.mainStackView.leftAnchor, constant: 0).isActive = true
        button.bottomAnchor.constraint(equalTo: self.mainStackView.bottomAnchor, constant: 10).isActive = true
        button.topAnchor.constraint(equalTo: self.mainStackView.topAnchor, constant: 10).isActive = true
        
        /*
         self.mainStackView.addArrangedSubview(secondButton)
         secondButton.centerXAnchor.constraint(equalTo: self.mainStackView.centerXAnchor, constant: 0).isActive = true
         secondButton.bottomAnchor.constraint(equalTo: self.mainStackView.bottomAnchor, constant: 10).isActive = true
         secondButton.topAnchor.constraint(equalTo: self.mainStackView.topAnchor, constant: 10).isActive = true
         */
        
        self.mainStackView.addArrangedSubview(thirdBorderButton)
        thirdBorderButton.rightAnchor.constraint(equalTo: self.mainStackView.rightAnchor, constant: 10).isActive = true
        thirdBorderButton.bottomAnchor.constraint(equalTo: self.mainStackView.bottomAnchor, constant: 10).isActive = true
        thirdBorderButton.topAnchor.constraint(equalTo: self.mainStackView.topAnchor, constant: 10).isActive = true

        if subsModel.id == 0 {
            button.addSubview(thirtButton)
            thirtButton.heightAnchor.constraint(equalToConstant: button.frame.height - 10).isActive = true
            thirtButton.widthAnchor.constraint(equalToConstant: button.frame.width - 10).isActive = true
            thirtButton.leadingAnchor.constraint(equalTo: button.leadingAnchor, constant: 5).isActive = true
            thirtButton.trailingAnchor.constraint(equalTo: button.trailingAnchor, constant: -5).isActive = true
            thirtButton.topAnchor.constraint(equalTo: button.topAnchor, constant: 5).isActive = true
            thirtButton.bottomAnchor.constraint(equalTo: button.bottomAnchor, constant: -5).isActive = true
            thirtButton.setContentHuggingPriority(.defaultLow, for: .vertical)
            thirtButton.translatesAutoresizingMaskIntoConstraints = false
        }
        
        if subsModel.id == 1 {
            thirdBorderButton.addSubview(thirtButton)
            thirtButton.heightAnchor.constraint(equalToConstant: thirdBorderButton.frame.height - 10).isActive = true
            thirtButton.widthAnchor.constraint(equalToConstant: thirdBorderButton.frame.width - 10).isActive = true
            thirtButton.leadingAnchor.constraint(equalTo: thirdBorderButton.leadingAnchor, constant: 5).isActive = true
            thirtButton.trailingAnchor.constraint(equalTo: thirdBorderButton.trailingAnchor, constant: -5).isActive = true
            thirtButton.topAnchor.constraint(equalTo: thirdBorderButton.topAnchor, constant: 5).isActive = true
            thirtButton.bottomAnchor.constraint(equalTo: thirdBorderButton.bottomAnchor, constant: -5).isActive = true
            thirtButton.setContentHuggingPriority(.defaultLow, for: .vertical)
            thirtButton.translatesAutoresizingMaskIntoConstraints = false
        }
        
        /*
         if subsModel.id == 2 {
             secondButton.addSubview(thirtButton)
             thirtButton.heightAnchor.constraint(equalToConstant: secondButton.frame.height - 10).isActive = true
             thirtButton.widthAnchor.constraint(equalToConstant: secondButton.frame.width - 10).isActive = true
             thirtButton.leadingAnchor.constraint(equalTo: secondButton.leadingAnchor, constant: 5).isActive = true
             thirtButton.trailingAnchor.constraint(equalTo: secondButton.trailingAnchor, constant: -5).isActive = true
             thirtButton.topAnchor.constraint(equalTo: secondButton.topAnchor, constant: 5).isActive = true
             thirtButton.bottomAnchor.constraint(equalTo: secondButton.bottomAnchor, constant: -5).isActive = true
             thirtButton.setContentHuggingPriority(.defaultLow, for: .vertical)
             thirtButton.translatesAutoresizingMaskIntoConstraints = false
         }
         */
         
        thirtButton.addSubview(nameLabel)
        nameLabel.leadingAnchor.constraint(equalTo: thirtButton.leadingAnchor, constant: 16).isActive = true
        nameLabel.topAnchor.constraint(equalTo: thirtButton.topAnchor, constant: 16).isActive = true
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        
        thirtButton.addSubview(priceLabel)
        priceLabel.leadingAnchor.constraint(equalTo: thirtButton.leadingAnchor, constant: 16).isActive = true
        priceLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 3).isActive = true
        priceLabel.translatesAutoresizingMaskIntoConstraints = false
        
        thirtButton.addSubview(bgImageView)
        bgImageView.trailingAnchor.constraint(equalTo: thirtButton.trailingAnchor, constant: -20).isActive = true
        bgImageView.topAnchor.constraint(equalTo: thirtButton.topAnchor, constant: 8).isActive = true
        bgImageView.translatesAutoresizingMaskIntoConstraints = false
        
        thirtButton.addSubview(descLabel)
        descLabel.centerYAnchor.constraint(equalTo: bgImageView.centerYAnchor, constant: 0).isActive = true
        descLabel.centerXAnchor.constraint(equalTo: bgImageView.centerXAnchor, constant: 0).isActive = true
        descLabel.translatesAutoresizingMaskIntoConstraints = false
         
        
        if subsModel.isWeekly! {
            bgImageView.fadeOut()
            descLabel.fadeOut()
        }
        
        if Definations.subscriptionShowPrice {
            priceLabel.isHidden = false
        } else {
            priceLabel.isHidden = true
        }
    }
    
    //MARK: SetupPaymentItem
    func setupPaymentItems(completion: @escaping(Bool) -> ()) {
        let group = DispatchGroup()
        Purchases.shared.offerings { (offerings, error) in
            group.enter()
            if let offerings = offerings {
                let offer = offerings.current
                let packages = offer?.availablePackages
                
                guard packages != nil else {
                    completion(false)
                    return
                }
                group.leave()
                // Loop through packages

                group.notify(queue: .main) {
                    for i in 0...packages!.count - 1 {
                        // Get a reference to the package
                        let package = packages![i]
                        self.packagesAvailableForPurchase.append(package)
                        
                        // Get a reference to the product
                        let product = package.product
                        
                        // Product title
                        //let title = product.localizedTitle
                        
                        // Product Price
                        let price = product.getLocalizedPrice()
                        
                        func unitName(unitRawValue:UInt) -> String {
                            switch unitRawValue {
                                case 0: return "Days".localized()
                                case 1: return "Week".localized()
                                case 2: return "Month".localized()
                                case 3: return "Year".localized()
                                default: return ""
                            }
                        }
                        // Product duration
                        var duration = ""
                        var backgroundName = ""
                        var isWeekly = false
                        var detailText = ""
                        var units : String?
                        
                        if #available(iOS 11.2, *) {
                          if let period = product.introductoryPrice?.subscriptionPeriod {
                            units = "\(period.numberOfUnits) \(unitName(unitRawValue: period.unit.rawValue)) " + "Free".localized()
                            if period.numberOfUnits == 0 {
                                isWeekly = true
                            } else {
                                isWeekly = false
                            }
                          } else {
                            isWeekly = true
                          }
                        } else {
                          // Fallback on earlier versions
                          // Get it from your server via API
                        }
                        
                        
                        switch product.productIdentifier {
                            case Definations.weekly:
                                duration = "Week".localized()
                                backgroundName = "mounthlyBg"
                                detailText = "\(units ?? "")"
                            case Definations.monthly:
                                duration = "Month".localized()
                                backgroundName = "mounthlyBg"
                                detailText = "\(units ?? "")"
                            case Definations.yearly:
                                duration = "Year".localized()
                                backgroundName = "yearlyBg"
                                detailText = "Save".localized()+" %80"
                                isWeekly = false
                            default:
                                duration = ""
                        }
                        
                        
                        //Create Model
                        self.model.append(
                            SubscriptionModel(
                                id: i,
                                name: duration,
                                named: backgroundName,
                                isWeeklyFormat: isWeekly,
                                price: price,
                                detailText: detailText
                            )
                        )
                    }
                    completion(true)
                }
            }
        }
    }
    
    //SubscriptionDetailActions
    @objc func changeFirstBorderState() {
        button.layer.borderWidth = 1
        secondButton.layer.borderWidth = 0
        thirdBorderButton.layer.borderWidth = 0
        selectedPackage = 0
    }
    
    @objc func changeSecondBorderState() {
        button.layer.borderWidth = 0
        secondButton.layer.borderWidth = 0
        thirdBorderButton.layer.borderWidth = 1
        selectedPackage = 1
    }
    
    @objc func changeThirdBorderState() {
        button.layer.borderWidth = 0
        secondButton.layer.borderWidth = 0
        thirdBorderButton.layer.borderWidth = 1
        selectedPackage = 2
    }
}

//MARK: SubscriptionCViewController(triple view)

extension SubscriptionCViewController {

    //MARK: CreateViews
    func createSubscriptionButtons(subsModel: SubscriptionModel!)  {
        let thirtButton = UIView()//border'ın icindeki beyaz view
        let monthLabel = UILabel()//current packageMonth
        let nameLabel = UILabel()//package name label
        let priceLabel = UILabel()//package price label
        let bgImageView = UIImageView()//package detail background image
        let descLabel = UILabel()//package detail desc label
        
        descLabel.widthAnchor.constraint(equalToConstant: 60).isActive = true
        descLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        let buttonGesture = UITapGestureRecognizer(target: self, action: #selector(self.changeFirstBorderState))
        let secondButtonGesture = UITapGestureRecognizer(target: self, action: #selector(self.changeSecondBorderState))
        let thirdBorderButtonGesture = UITapGestureRecognizer(target: self, action: #selector(self.changeThirdBorderState))
        
        self.firstSubscriptionView.addGestureRecognizer(buttonGesture)
        self.secondSubscriptionView.addGestureRecognizer(secondButtonGesture)
        self.thirdSubscriptionView.addGestureRecognizer(thirdBorderButtonGesture)
        
        self.firstSubscriptionView.backgroundColor = .clear
        self.firstSubscriptionView.layer.borderWidth = 0
        self.firstSubscriptionView.layer.borderColor = UIColor.clear.cgColor
        self.firstSubscriptionView.layer.cornerRadius = 8
        
        self.secondSubscriptionView.backgroundColor = UIColor(named: "titleColor")
        self.secondSubscriptionView.layer.borderWidth = 4
        self.secondSubscriptionView.layer.borderColor = UIColor.clear.cgColor
        self.secondSubscriptionView.layer.cornerRadius = 8
        
        self.thirdSubscriptionView.backgroundColor = .clear
        self.thirdSubscriptionView.layer.borderWidth = 0
        self.thirdSubscriptionView.layer.borderColor = UIColor.clear.cgColor
        self.thirdSubscriptionView.layer.cornerRadius = 8
        
        switch subsModel.name {
            case "Week".localized():
                monthLabel.text = "1"
            case "Month".localized():
                monthLabel.text = "1"
            case "Year".localized():
                monthLabel.text = "1"
            default:
                monthLabel.text = ""
        }

        
        thirtButton.backgroundColor = .white
        thirtButton.layer.cornerRadius = 8
        
        
        monthLabel.font = UIFont(name: "Poppins-Bold", size: 24)
        monthLabel.textColor = .black
       
        nameLabel.text = subsModel.name
        nameLabel.font = UIFont(name: "Poppins-Regular", size: 14)
        nameLabel.textColor = .black
        
        priceLabel.text = subsModel.price
        priceLabel.font = UIFont(name: "Poppins-Regular", size: 18)
        priceLabel.textColor = .black
        
        bgImageView.image = subsModel.bgImage
        bgImageView.contentMode = .scaleAspectFill
        bgImageView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        bgImageView.widthAnchor.constraint(equalToConstant: 70).isActive = true
        bgImageView.layer.cornerRadius = 12
        
        descLabel.text = subsModel.detailText
        descLabel.font = UIFont(name: "Poppins-Regular", size: 8)
        descLabel.textColor = .white
        
        descLabel.numberOfLines = 0
        descLabel.adjustsFontSizeToFitWidth = true
        descLabel.lineBreakMode = .byWordWrapping
        descLabel.textAlignment = .center
        
        if subsModel.id == 0 {
            self.firstSubscriptionView.addSubview(thirtButton)
            thirtButton.heightAnchor.constraint(equalToConstant: self.firstSubscriptionView.frame.height - 10).isActive = true
            thirtButton.widthAnchor.constraint(equalToConstant: self.firstSubscriptionView.frame.width - 10).isActive = true
            thirtButton.leadingAnchor.constraint(equalTo: self.firstSubscriptionView.leadingAnchor, constant: 4).isActive = true
            thirtButton.trailingAnchor.constraint(equalTo: self.firstSubscriptionView.trailingAnchor, constant: -4).isActive = true
            thirtButton.topAnchor.constraint(equalTo: self.firstSubscriptionView.topAnchor, constant: 4).isActive = true
            thirtButton.bottomAnchor.constraint(equalTo: self.firstSubscriptionView.bottomAnchor, constant: -4).isActive = true
            thirtButton.setContentHuggingPriority(.defaultLow, for: .vertical)
            thirtButton.setContentHuggingPriority(.defaultLow, for: .horizontal)
            thirtButton.translatesAutoresizingMaskIntoConstraints = false
        }
        
        if subsModel.id == 1 {
            self.secondSubscriptionView.addSubview(thirtButton)
            thirtButton.heightAnchor.constraint(equalToConstant: self.secondSubscriptionView.frame.height - 10).isActive = true
            thirtButton.widthAnchor.constraint(equalToConstant: self.secondSubscriptionView.frame.width - 10).isActive = true
            thirtButton.leadingAnchor.constraint(equalTo: self.secondSubscriptionView.leadingAnchor, constant: 4).isActive = true
            thirtButton.trailingAnchor.constraint(equalTo: self.secondSubscriptionView.trailingAnchor, constant: -4).isActive = true
            thirtButton.topAnchor.constraint(equalTo: self.secondSubscriptionView.topAnchor, constant: 4).isActive = true
            thirtButton.bottomAnchor.constraint(equalTo: self.secondSubscriptionView.bottomAnchor, constant: -4).isActive = true
            thirtButton.setContentHuggingPriority(.defaultLow, for: .vertical)
            thirtButton.setContentHuggingPriority(.defaultLow, for: .horizontal)
            thirtButton.translatesAutoresizingMaskIntoConstraints = false
        }
        
        if subsModel.id == 2 {
            self.thirdSubscriptionView.addSubview(thirtButton)
            thirtButton.heightAnchor.constraint(equalToConstant: self.thirdSubscriptionView.frame.height - 10).isActive = true
            thirtButton.widthAnchor.constraint(equalToConstant: self.thirdSubscriptionView.frame.width - 10).isActive = true
            thirtButton.leadingAnchor.constraint(equalTo: self.thirdSubscriptionView.leadingAnchor, constant: 4).isActive = true
            thirtButton.trailingAnchor.constraint(equalTo: self.thirdSubscriptionView.trailingAnchor, constant: -4).isActive = true
            thirtButton.topAnchor.constraint(equalTo: self.thirdSubscriptionView.topAnchor, constant: 4).isActive = true
            thirtButton.bottomAnchor.constraint(equalTo: self.thirdSubscriptionView.bottomAnchor, constant: -4).isActive = true
            thirtButton.setContentHuggingPriority(.defaultLow, for: .vertical)
            thirtButton.setContentHuggingPriority(.defaultLow, for: .horizontal)
            thirtButton.translatesAutoresizingMaskIntoConstraints = false
        }
        
        thirtButton.addSubview(monthLabel)
        monthLabel.centerXAnchor.constraint(equalTo: thirtButton.centerXAnchor, constant: 0).isActive = true
        monthLabel.translatesAutoresizingMaskIntoConstraints = false
        
        thirtButton.addSubview(nameLabel)
        nameLabel.centerXAnchor.constraint(equalTo: monthLabel.centerXAnchor, constant: 0).isActive = true
        nameLabel.topAnchor.constraint(equalTo: monthLabel.bottomAnchor, constant: 6).isActive = true
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        
        thirtButton.addSubview(priceLabel)
        priceLabel.centerXAnchor.constraint(equalTo: monthLabel.centerXAnchor, constant: 0).isActive = true
        priceLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 3).isActive = true
        priceLabel.translatesAutoresizingMaskIntoConstraints = false
        
        thirtButton.addSubview(bgImageView)
        let imageCons = bgImageView.centerXAnchor.constraint(equalTo: thirtButton.centerXAnchor, constant: 0)
        imageCons.priority = UILayoutPriority(1)
        imageCons.isActive = true
        bgImageView.topAnchor.constraint(equalTo: thirtButton.topAnchor, constant: -15).isActive = true
        bgImageView.translatesAutoresizingMaskIntoConstraints = false
        
        thirtButton.addSubview(descLabel)
        bgImageView.layer.cornerRadius = 24
        descLabel.centerYAnchor.constraint(equalTo: bgImageView.centerYAnchor, constant: 0).isActive = true
        descLabel.centerXAnchor.constraint(equalTo: bgImageView.centerXAnchor, constant: 0).isActive = true
        descLabel.translatesAutoresizingMaskIntoConstraints = false
        
        if subsModel.isWeekly! {
            bgImageView.fadeOut()
            descLabel.fadeOut()
        }
        
        if Definations.subscriptionShowPrice {
            priceLabel.isHidden = false
            monthLabel.topAnchor.constraint(equalTo: thirtButton.topAnchor, constant: 18).isActive = true
        } else {
            priceLabel.isHidden = true
            monthLabel.centerYAnchor.constraint(equalTo: thirtButton.centerYAnchor, constant: -15).isActive = true
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            let viewee = self.secondSubscriptionView.subviews[0].subviews
            let label = viewee[2] as! UILabel
            let label3 = viewee[1] as! UILabel
            
            let unitLAbel = viewee[4] as! UILabel
            
            let unit = unitLAbel.text
            self.setRenewsText(months: "\(label3.text!)", price: label.text!, unit:"\(unit!)" )
        }
        
        self.selectedPackage = self.model[1].id!
    }
    
    //MARK: SetupPaymentItem
    func setupPaymentItems(completion: @escaping(Bool) -> ()) {
        let group = DispatchGroup()
        Purchases.shared.offerings { (offerings, error) in
            group.enter()
            if let offerings = offerings {
                let offer = offerings.current
                let packages = offer?.availablePackages
                
                guard packages != nil else {
                    completion(false)
                    return
                }
                group.leave()
                // Loop through packages

                group.notify(queue: .main) {
                    for i in 0...packages!.count - 1 {
                        // Get a reference to the package
                        let package = packages![i]
                        self.packagesAvailableForPurchase.append(package)
                        
                        // Get a reference to the product
                        let product = package.product
                        
                        // Product title
                        //let title = product.localizedTitle
                        
                        // Product Price
                        let price = product.getLocalizedPrice()

                        func unitName(unitRawValue:UInt) -> String {
                            switch unitRawValue {
                                case 0: return "days".localized()
                                case 1: return "week".localized()
                                case 2: return "month".localized()
                                case 3: return "year".localized()
                                default: return ""
                            }
                        }
                        // Product duration
                        var duration = ""
                        var backgroundName = ""
                        var isWeekly = false
                        var detailText = ""
                        var units : String?
                        
                        if #available(iOS 11.2, *) {
                          if let period = product.introductoryPrice?.subscriptionPeriod {
                            units = "\(period.numberOfUnits) \(unitName(unitRawValue: period.unit.rawValue)) " + "Free".localized().lowercased()
                            if period.numberOfUnits == 0 {
                                isWeekly = true
                            } else {
                                isWeekly = false
                            }
                          } else {
                            isWeekly = true
                          }
                        } else {
                          // Fallback on earlier versions
                          // Get it from your server via API
                        }
                    
                        
                        switch product.productIdentifier {
                            case Definations.weekly:
                                duration = "Week".localized()
                                backgroundName = "mounthlyBg"
                                detailText = "\(units ?? "")"
                            case Definations.monthly:
                                duration = "Month".localized()
                                backgroundName = "mounthlyBg"
                                detailText = "\(units ?? "")"
                            case Definations.yearly:
                                duration = "Year".localized()
                                backgroundName = "yearlyBg"
                                detailText = "Save".localized()+" %80"
                                isWeekly = false
                            default:
                                duration = ""
                        }
                        
                        //Create Model
                        self.model.append(
                            SubscriptionModel(
                                id: i,
                                name: duration,
                                named: backgroundName,
                                isWeeklyFormat: isWeekly,
                                price: price,
                                detailText: detailText
                            )
                        )
                     }
                    completion(true)
                }
            }
        }
    }
    
    //SubscriptionDetailActions
    @objc func changeFirstBorderState() {
        self.firstSubscriptionView.backgroundColor = UIColor(named: "titleColor")
        self.secondSubscriptionView.backgroundColor = .clear
        self.thirdSubscriptionView.backgroundColor = .clear
        let viewee = firstSubscriptionView.subviews[0].subviews
        let label = viewee[2] as! UILabel
        let label3 = viewee[1] as! UILabel
        
        let unitLAbel = viewee[4] as! UILabel
        let unit = unitLAbel.text

        self.setRenewsText(months: "\(label3.text!)", price: label.text!, unit:"\(unit!)" )
        selectedPackage = 0
    }
    
    @objc func changeSecondBorderState() {
        self.firstSubscriptionView.backgroundColor = .clear
        self.secondSubscriptionView.backgroundColor = UIColor(named: "titleColor")
        self.thirdSubscriptionView.backgroundColor = .clear
        let viewee = secondSubscriptionView.subviews[0].subviews
        let label = viewee[2] as! UILabel
        let label3 = viewee[1] as! UILabel
        let unitLAbel = viewee[4] as! UILabel
        let unit = unitLAbel.text
        self.setRenewsText(months: "\(label3.text!)", price: label.text!, unit:"\(unit!)" )
        selectedPackage = 1
    }
    
    @objc func changeThirdBorderState() {
        self.firstSubscriptionView.backgroundColor = .clear
        self.secondSubscriptionView.backgroundColor = .clear
        self.thirdSubscriptionView.backgroundColor = UIColor(named: "titleColor")
        let viewee = thirdSubscriptionView.subviews[0].subviews
        let label = viewee[2] as! UILabel
        let label3 = viewee[1] as! UILabel
        
        let unitLAbel = viewee[4] as! UILabel
        
        let unit = unitLAbel.text
        self.setRenewsText(months: "\(label3.text!)", price: label.text!, unit:"\(unit!)" )
        selectedPackage = 2
    }
    
    func setRenewsText(months: String, price: String, unit: String) {
        if unit == "" || unit.contains("Save".localized()){
            self.renewsTextOutlet.text = "\(price) / \(months.lowercased()). " + "Auto renews".localized()
        } else {
            if let countryCode = Locale.current.languageCode {
                if countryCode == "en" {
                    self.renewsTextOutlet.text = "Try".localized() + " \(unit.lowercased()), " + "then".localized() + " \(price) / \(months.lowercased()). "
                } else {
                    self.renewsTextOutlet.text = " \(unit.lowercased()), " + "Try".localized().lowercased() + " " + "then".localized() + " \(price) / \(months.lowercased()). "
                }
            }
        }
    }
}

//MARK: ProgressPhotoViewController extension
extension ProgressPhotoViewController { 
    func useToonify(completionHandler: @escaping(Bool) -> ()){
        let parameters = [
                "name": "image",
                "fileName": "IMG_8373.jpg",
                "contentType": "application/octet-stream",
                "file": "[object File]"
        ]

        let req = BaseRequest()
        let file = BaseFile()
              file.name = "image"
              file.type = .image
        if let data = self.imageView.image?.jpegData(compressionQuality: 1.0) {
                  file.data = data
                  req.files.append(file)
              }

        file.type = .image
        req.files.append(file)
        req.method = .post
        req.parameters = parameters
        req.url = GlobalConstants.BaseURL
        
        if effectId == GlobalConstants.init().caricature {
            CartoonEffectBuilder.request(request: req, success: { (json) in
                if let convertedString = json["b64_encoded_output"].rawValue as? String {
                    print(convertedString)
                    let imageData = Data.init(base64Encoded: convertedString, options: .init(rawValue: 0))
                    self.takenPhoto = UIImage(data: imageData!)
                    self.progressBar.progress = 0.99
                    self.isRequestEnded = true
                    completionHandler(true)
                } else {
                    completionHandler(false)
                }
            }) { (error) in
                print(error.localizedDescription)
                completionHandler(false)
            }
        } else if (effectId == GlobalConstants.init().zombify) {
            ZombifyEffectBuilder.request(request: req, success: { (json) in
                if let convertedString = json["b64_encoded_output"].rawValue as? String {
                    print(convertedString)
                    let imageData = Data.init(base64Encoded: convertedString, options: .init(rawValue: 0))
                    self.takenPhoto = UIImage(data: imageData!)
                    self.progressBar.progress = 0.99
                    self.isRequestEnded = true
                    completionHandler(true)
                } else {
                    completionHandler(false)
                }
            }) { (error) in
                print(error.localizedDescription)
                completionHandler(false)
            }
        } else if (effectId == GlobalConstants.init().toonifyPlus) {
            ToonifyplusEffectHandler.request(request: req, success: { (json) in
                if let convertedString = json["b64_encoded_output"].rawValue as? String {
                    print(convertedString)
                    let imageData = Data.init(base64Encoded: convertedString, options: .init(rawValue: 0))
                    self.takenPhoto = UIImage(data: imageData!)
                    self.progressBar.progress = 0.99
                    self.isRequestEnded = true
                    completionHandler(true)
                } else {
                    completionHandler(false)
                }
            }) { (error) in
                print(error.localizedDescription)
                completionHandler(false)
            }
        } else if (effectId == GlobalConstants.init().emojify) {
            EmojifyEffectHandler.request(request: req, success: { (json) in
                if let convertedString = json["b64_encoded_output"].rawValue as? String {
                    print(convertedString)
                    let imageData = Data.init(base64Encoded: convertedString, options: .init(rawValue: 0))
                    self.takenPhoto = UIImage(data: imageData!)
                    self.progressBar.progress = 0.99
                    self.isRequestEnded = true
                    completionHandler(true)
                } else {
                    completionHandler(false)
                }
            }) { (error) in
                print(error.localizedDescription)
                completionHandler(false)
            }
        } else if (effectId == GlobalConstants.init().toonyfyHd) {
            ToonifyHdEffectBuilder.request(request: req, success: { (json) in
                if let convertedString = json["b64_encoded_output"].rawValue as? String {
                    print(convertedString)
                    let imageData = Data.init(base64Encoded: convertedString, options: .init(rawValue: 0))
                    self.takenPhoto = UIImage(data: imageData!)
                    self.progressBar.progress = 0.99
                    self.isRequestEnded = true
                    completionHandler(true)
                } else {
                    completionHandler(false)
                }
            }) { (error) in
                print(error.localizedDescription)
                completionHandler(false)
            }
        } else if (effectId == GlobalConstants.init().comic) {
            ComicEffectHandler.request(request: req, success: { (json) in
                if let convertedString = json["b64_encoded_output"].rawValue as? String {
                    print(convertedString)
                    let imageData = Data.init(base64Encoded: convertedString, options: .init(rawValue: 0))
                    self.takenPhoto = UIImage(data: imageData!)
                    self.progressBar.progress = 0.99
                    self.isRequestEnded = true
                    completionHandler(true)
                } else {
                    completionHandler(false)
                }
            }) { (error) in
                print(error.localizedDescription)
                completionHandler(false)
            }
        }
    }
    
    @objc func setProgressBar() {
        if isRequestEnded {
            if !finishedProgress {
                finishedProgress = true
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "PhotoCompletedViewController") as! PhotoCompletedViewController
                vc.filteredImg = self.takenPhoto
                vc.effectTitle = self.effectTitle ?? ""
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            }
        } else {
            if indexProgressBar == poseDuration {
                // reset the progress counter
                indexProgressBar = 0
            }
        }
        // update the display
        // use poseDuration - 1 so that you display 20 steps of the the progress bar, from 0...19
        progressBar.progress = Float(indexProgressBar) / Float(poseDuration - 1)

        // increment the counter
        indexProgressBar += 1
    }
}


/*
 if (subsModel.id == 0 && subsModel.name!.contains("Yearly")) || (subsModel.id == 0 && subsModel.name!.contains("Monthly")) {
     if subsModel.id == 0 {
         button.addSubview(thirtButton)
         thirtButton.heightAnchor.constraint(equalToConstant: button.frame.height - 10).isActive = true
         thirtButton.widthAnchor.constraint(equalToConstant: button.frame.width - 10).isActive = true
         thirtButton.leadingAnchor.constraint(equalTo: button.leadingAnchor, constant: 5).isActive = true
         thirtButton.trailingAnchor.constraint(equalTo: button.trailingAnchor, constant: -5).isActive = true
         thirtButton.topAnchor.constraint(equalTo: button.topAnchor, constant: 5).isActive = true
         thirtButton.bottomAnchor.constraint(equalTo: button.bottomAnchor, constant: -5).isActive = true
         thirtButton.setContentHuggingPriority(.defaultLow, for: .vertical)
         thirtButton.translatesAutoresizingMaskIntoConstraints = false
     } else {
         secondButton.addSubview(thirtButton)
         thirtButton.heightAnchor.constraint(equalToConstant: secondButton.frame.height - 10).isActive = true
         thirtButton.widthAnchor.constraint(equalToConstant: secondButton.frame.width - 10).isActive = true
         thirtButton.leadingAnchor.constraint(equalTo: secondButton.leadingAnchor, constant: 5).isActive = true
         thirtButton.trailingAnchor.constraint(equalTo: secondButton.trailingAnchor, constant: -5).isActive = true
         thirtButton.topAnchor.constraint(equalTo: secondButton.topAnchor, constant: 5).isActive = true
         thirtButton.bottomAnchor.constraint(equalTo: secondButton.bottomAnchor, constant: -5).isActive = true
         thirtButton.setContentHuggingPriority(.defaultLow, for: .vertical)
         thirtButton.translatesAutoresizingMaskIntoConstraints = false
     }
 }
 if (subsModel.id == 1 && subsModel.name!.contains("Yearly")) || (subsModel.id == 1 && subsModel.name!.contains("Monthly")) {
     if subsModel.id == 1 {
         button.addSubview(thirtButton)
         thirtButton.heightAnchor.constraint(equalToConstant: button.frame.height - 10).isActive = true
         thirtButton.widthAnchor.constraint(equalToConstant: button.frame.width - 10).isActive = true
         thirtButton.leadingAnchor.constraint(equalTo: button.leadingAnchor, constant: 5).isActive = true
         thirtButton.trailingAnchor.constraint(equalTo: button.trailingAnchor, constant: -5).isActive = true
         thirtButton.topAnchor.constraint(equalTo: button.topAnchor, constant: 5).isActive = true
         thirtButton.bottomAnchor.constraint(equalTo: button.bottomAnchor, constant: -5).isActive = true
         thirtButton.setContentHuggingPriority(.defaultLow, for: .vertical)
         thirtButton.translatesAutoresizingMaskIntoConstraints = false
     } else {
         secondButton.addSubview(thirtButton)
         thirtButton.heightAnchor.constraint(equalToConstant: secondButton.frame.height - 10).isActive = true
         thirtButton.widthAnchor.constraint(equalToConstant: secondButton.frame.width - 10).isActive = true
         thirtButton.leadingAnchor.constraint(equalTo: secondButton.leadingAnchor, constant: 5).isActive = true
         thirtButton.trailingAnchor.constraint(equalTo: secondButton.trailingAnchor, constant: -5).isActive = true
         thirtButton.topAnchor.constraint(equalTo: secondButton.topAnchor, constant: 5).isActive = true
         thirtButton.bottomAnchor.constraint(equalTo: secondButton.bottomAnchor, constant: -5).isActive = true
         thirtButton.setContentHuggingPriority(.defaultLow, for: .vertical)
         thirtButton.translatesAutoresizingMaskIntoConstraints = false
     }
 } else {
     secondButton.addSubview(thirtButton)
     thirtButton.heightAnchor.constraint(equalToConstant: secondButton.frame.height - 10).isActive = true
     thirtButton.widthAnchor.constraint(equalToConstant: secondButton.frame.width - 10).isActive = true
     thirtButton.leadingAnchor.constraint(equalTo: secondButton.leadingAnchor, constant: 5).isActive = true
     thirtButton.trailingAnchor.constraint(equalTo: secondButton.trailingAnchor, constant: -5).isActive = true
     thirtButton.topAnchor.constraint(equalTo: secondButton.topAnchor, constant: 5).isActive = true
     thirtButton.bottomAnchor.constraint(equalTo: secondButton.bottomAnchor, constant: -5).isActive = true
     thirtButton.setContentHuggingPriority(.defaultLow, for: .vertical)
     thirtButton.translatesAutoresizingMaskIntoConstraints = false
 }
 
*/
extension SKProduct {
    func getLocalizedPrice(duration: Int = 1) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = self.priceLocale
        let durationPrice = NSNumber(value: (self.price.floatValue / Float(duration)) - (duration > 1 ? 0.01 : 0.00))
        return formatter.string(from: durationPrice) ?? "?"
    }
}

extension Array where Element: Equatable {
   // Remove first collection element that is equal to the given `object`:
   mutating func remove(object: Element) {
       guard let index = firstIndex(of: object) else {return}
       remove(at: index)
   }
}


/*
 for i in 0...packages!.count - 1{
     // Get a reference to the package
     let package = packages![i]
     self.packagesAvailableForPurchase.append(package)
     
     // Get a reference to the product
     let product = package.product
     
     // Product title
     //let title = product.localizedTitle
     
     // Product Price
     let price = product.getLocalizedPrice()
     // Product duration
     var duration = ""
     switch product.productIdentifier {
         case Definations.weekly:
             duration = "Weekly".localized()
         case Definations.monthly:
             duration = "Monthly".localized()
         case Definations.yearly:
             duration = "Yearly".localized()
         default:
             duration = ""
     }
     DispatchQueue.main.async { [self] in
         if i == 0 {
             if product.productIdentifier == Definations.yearly {
                 self.yearlyBg.isHidden = false
                 self.yearSaveText.isHidden = false
                 self.yearlyBg.image = UIImage(named: "yearlyBg")
                 self.yearSaveText.text = "Save".localized()+" %71"
                 self.isYearActive = true
                 
                 if !self.duplicateArray.contains(duration) {
                     self.model.append(
                         SubscriptionModel(
                             id: i,
                             name: duration,
                             named: "yearlyBg",
                             isWeeklyFormat: false,
                             price: price,
                             detailText: "Save".localized()+" %71"
                         )
                     )
                 }
             }

             if product.productIdentifier == Definations.monthly {
                 self.monthlyBg.isHidden = false
                 self.monthFreeText.isHidden = false
                 self.monthFreeText.text = "3 Days Free".localized()
                 self.isMonthActive = true

                 if !self.duplicateArray.contains(duration) {
                     self.model.append(
                         SubscriptionModel(
                             id: i,
                             name: duration,
                             named: "mounthlyBg",
                             isWeeklyFormat: false,
                             price: price,
                             detailText: "3 Days Free".localized()
                         )
                     )
                 }
             }

             if product.productIdentifier.contains("premium_weekly") {

                 if !self.duplicateArray.contains(duration) {
                     self.model.append(
                         SubscriptionModel(
                             id: i,
                             name: duration,
                             named: "monthlyBg",
                             isWeeklyFormat: true,
                             price: price,
                             detailText: "3 Days Free".localized()
                         )
                     )
                 }
             }

             self.yearText.text = duration
             self.yearSaveText.text = "Save".localized()+" %71"
             self.yearlyPrice.text = price

         }

         if i == 1 {
             if product.productIdentifier == Definations.yearly {
                 self.yearlyBg.isHidden = false
                 self.yearSaveText.isHidden = false
                 self.yearlyBg.image = UIImage(named: "yearlyBg")
                 self.yearSaveText.text = "Save".localized()+" %71"
                 self.isYearActive = true
                 if !self.duplicateArray.contains(duration) {
                     self.model.append(
                         SubscriptionModel(
                             id: i,
                             name: duration,
                             named: "yearlyBg",
                             isWeeklyFormat: false,
                             price: price,
                             detailText: "Save".localized()+" %71"
                         )
                     )
                 }
             }

             if product.productIdentifier == Definations.monthly {
                 self.monthlyBg.isHidden = false
                 self.monthFreeText.isHidden = false
                 self.isMonthActive = true
                 if !self.duplicateArray.contains(duration) {
                     self.model.append(
                         SubscriptionModel(
                             id: i,
                             name: duration,
                             named: "mounthlyBg",
                             isWeeklyFormat: false,
                             price: price,
                             detailText: "3 Days Free".localized()
                         )
                     )
                 }
             }

             if product.productIdentifier.contains("premium_weekly") {
                 if !self.duplicateArray.contains(duration) {
                     self.model.append(
                         SubscriptionModel(
                             id: i,
                             name: duration,
                             named: "monthlyBg",
                             isWeeklyFormat: true,
                             price: price,
                             detailText: "3 Days Free".localized()
                         )
                     )
                 }
             }

             self.monthText.text = duration
             self.monthFreeText.text = "3 Days Free".localized()
             print("orhan", "3 Days Free".localized())
             self.monthlyPrice.text = price
             completion(true)
         }
     }
 }
*/

//
//  Definations.swift
//  PixCam
//
//  Created by Orhan Erbas on 18.06.2021.
//

import Foundation
import BanubaSdk

var defaults = UserDefaults.standard
class Definations {
    //MARK: CommonVariables
    public static var banubaEffects = [BanubaEffects]()
    public static var toonifyEffects = [ToonifyEffects]()
    public static var maskEffects = [Mask]()
    public static var lipsEffects = [LipsEffects]()
    public static var makeupEffects = [LipsEffects]()
    public static var unzipedFiles = [String]()
    public static var unzipedLipsFiles = [String]()
    public static var completed = false
    public static var controllerArray : [Any] = []
    public static var uploadDescriptions : [String] = ["Keep your app open while your app loads effects.".localized(), "This will be done once during your use.".localized()]
    public static var uploadCounter : Int = 0
    public static var emailSended : Bool!
    public static var selectedEffectCode = -1
    public static var isBanubaLoaded : Bool = false
    public static var userFromBanuba = false
    public static var lastVCIdentifier = ""
    public static var selectedBanubaPhoto = ""
    public static var banubaIsInitialize = false
    public static var segueFromFacePercente = false
    public static var lastEffectName = ""
    
    // Remote Config
    public static var subscriptionShowPrice: Bool = false
    public static var subscriptionCTATitle : String = ""
    public static var subscriptionCloseAppearTime : Double = 0
    public static var subscriptionHeader : String = ""
    public static var subscriptionDescription : String = ""
    public static var subscriptionCTATextColor : String = ""
    public static var subscriptionCTAButtonColor : String = ""
    public static var subscriptionMainText_1 : String = ""
    public static var subscriptionMainText_2 : String = ""
    public static var subscriptionMainText_3 : String = ""
    public static var subscriptionShowSingleButton : Int = 0
    public static var subscriptionBGcolor : String = ""
    public static var subscriptionShowPaywallFirst : Bool = false
    public static var subscriptionRewardedInterstitial : Bool = false
    public static var SplashHeader_1 : String = ""
    public static var SplashHeader_2 : String = ""
    public static var SplashHeader_3 : String = ""
    public static var SplashButtonText_1 : String = ""
    public static var SplashButtonText_2 : String = ""
    public static var SplashButtonText_3 : String = ""
    public static var SplashButtonTextColor : String = ""
    public static var SplashButtonColor : String = ""
    public static var SplashHeaderDescription_1 : String = ""
    public static var SplashHeaderDescription_2 : String = ""
    public static var SplashHeaderDescription_3 : String = ""
    public static var paywallConnectIsEnd = false
    public static var isPremiumUser = false
    public static var PaywallFirst = false

    
    //MARK: Product
    public static var weekly = "pixcam_premium_weekly_02"
    public static var monthly = "pixcam_premium_monthly_02"
    public static var yearly = "pixcam_premium_annual_02"

    
    //MARK: ToonifyEffect
    public static var isPhotoSelected = false
    
    //MARK: Camera
    public static var isUserFromCamera = false


    
    public static var isBanubaEffectActive = Bool() {
        didSet {
            if isBanubaEffectActive {
                ApiManager.instance.getBanubaEffects(success: {result in
                    self.banubaEffects.append(result)
                })
            } 
        }
    }
    
    public static var isToonifyEffectActive = Bool() {
        didSet {
            if isToonifyEffectActive {
                ApiManager.instance.getToonifyEffects(success: {result in
                    self.toonifyEffects.append(result)
                })
            }
        }
    }
    
    public static var isMaskEffectsActive = Bool() {
        didSet {
            if isMaskEffectsActive {
                ApiManager.instance.getMasks { base in
                    switch base {
                    case .success(let success):
                        print(success)
                        DispatchQueue.main.async {
                            self.maskEffects.append(success)
                        }
                    case .failure(let failture):
                        print(failture.localizedDescription)
                    }
                }
            }
        }
    }
    
    public static var isLipsEffectsActive = Bool() {
         didSet {
            if isLipsEffectsActive {
               ApiManager.instance.getLips { result in
                     self.lipsEffects.append(result)
               }
            }
        }
    }
    
    public static var isMakeupEffectsActive = Bool() {
         didSet {
            if isMakeupEffectsActive {
               ApiManager.instance.getMakeup { result in
                     self.makeupEffects.append(result)
               }
            }
        }
    }
    
    //MARK: AllCountries
    
    public static var countries : [String] = [
        "Argentina-AR",
        "Armenia-AM",
        "Australia-AU",
        "Austria-AT",
        "Azerbaijan-AZ",
        "Bahrain-BH",
        "Barbados-BB",
        "Belarus-BY",
        "Belgium-BE",
        "Bolivia-BO",
        "Bosnia and Herzegovina-BA",
        "Brazil-BR",
        "Bulgaria-BG",
        "Cambodia-KH",
        "Cameroon-CM",
        "Canada-CA",
        "Chile-CL",
        "China-CN",
        "Colombia-CO",
        "Costa Rica-CR",
        "Côte d'Ivoire-CI",
        "Croatia-HR",
        "Cyprus-CY",
        "Czechia-CZ",
        "Congo-CD",
        "Denmark-DK",
        "Dominican Republic-DO",
        "Ecuador-EC",
        "Egypt-EG",
        "Estonia-EE",
        "Finland-FI",
        "France-FR",
        "Germany-DE",
        "Ghana-GH",
        "Greece-GR",
        "Honduras-HN",
        "Hungary-HU",
        "Iceland-IS",
        "India-IN",
        "Indonesia-ID",
        "Ireland-IE",
        "Israel-IL",
        "Italy-IT",
        "Jamaica-JM",
        "Japan-JP",
        "Kuwait-KW",
        "Latvia-LV",
        "Lebanon-LB",
        "Liechtenstein-LI",
        "Lithuania-LT",
        "Malaysia-MY",
        "Malta-MT",
        "Mexico-MX",
        "Moldova-MD",
        "Montenegro-ME",
        "Morocco-MA",
        "Netherlands-NL",
        "New Zealand-NZ",
        "Nigeria-NG",
        "Norway-NO",
        "Oman-OM",
        "Papua New Guinea-PG",
        "Paraguay-PY",
        "Peru-PE",
        "Philippines-PH",
        "Poland-PL",
        "Portugal-PT",
        "Qatar-QA",
        "Romania-RO",
        "Russia-RU",
        "Saudi Arabia-SA",
        "Senegal-SN",
        "Singapore-SG",
        "Slovakia-SK",
        "Slovenia-SI",
        "South Africa-ZA",
        "South Korea-KR",
        "Spain-ES",
        "Sri Lanka-LK",
        "Sweden-SE",
        "Switzerland-CH",
        "Thailand-TH",
        "Trinidad and Tobago-TT",
        "Tunisia-TN",
        "Turkey-TR",
        "Ukraine-UA",
        "United Arab Emirates-AE",
        "United Kingdom-GB",
        "United States-US",
        "Uruguay-UY",
        "Vietnam-VN"
    ]
}

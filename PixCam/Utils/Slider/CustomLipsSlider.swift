//
//  CustomSlider.swift
//  PixCam
//
//  Created by Orhan Erbas on 27.07.2021.
//

import Foundation
import UIKit

class CustomLipsSlider: UISlider {

    @IBInspectable var height: CGFloat = 2
    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(origin: bounds.origin, size: CGSize(width: bounds.width, height: height))
    }
}

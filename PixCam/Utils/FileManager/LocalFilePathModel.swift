//
//  LocalFilePathModel.swift
//  PixCam
//
//  Created by Orhan Erbas on 21.06.2021.
//

import Foundation
import UIKit

class LocalFilePathModel: NSObject, NSCoding {
 

    var fileName: String = ""
    var filePath: URL
    
    init(fileName: String, filePath: URL) {
        self.fileName = fileName
        self.filePath = filePath
    }
    
    // MARK: - NSCoding
     required init(coder aDecoder: NSCoder) {
        fileName = aDecoder.decodeObject(forKey: "fileName") as! String
        filePath = aDecoder.decodeObject(forKey: "filePath") as! URL
     }
    
    func encode(with coder: NSCoder) {
        coder.encode(fileName, forKey: "fileName")
        coder.encode(filePath, forKey: "filePath")
    }
}

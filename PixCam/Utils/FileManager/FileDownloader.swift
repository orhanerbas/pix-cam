//
//  FileDownloader.swift
//  PixCam
//
//  Created by Orhan Erbas on 21.06.2021.
//

import Foundation
import UIKit
import BanubaSdk

class FileDownloader {

    static func loadFileSync(url: URL, completion: @escaping (String?, Error?) -> Void)
    {
        let documentsUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!

        let destinationUrl = documentsUrl.appendingPathComponent(url.lastPathComponent)

        if FileManager().fileExists(atPath: destinationUrl.path)
        {
            completion(destinationUrl.path, nil)
        }
        else if let dataFromURL = NSData(contentsOf: url)
        {
            if dataFromURL.write(to: destinationUrl, atomically: true)
            {
                print("file saved [\(destinationUrl.path)]")
                completion(destinationUrl.path, nil)
            }
            else
            {
                print("error saving file")
                let error = NSError(domain:"Error saving file", code:1001, userInfo:nil)
                completion(destinationUrl.path, error)
            }
        }
        else
        {
            let error = NSError(domain:"Error downloading file", code:1002, userInfo:nil)
            completion(destinationUrl.path, error)
        }
    }

    static func loadFileAsync(url: URL, completion: @escaping (String?, Error?) -> Void)
    {
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!

        let destinationUrl = documentsUrl.appendingPathComponent(url.lastPathComponent)

        if FileManager().fileExists(atPath: destinationUrl.path)
        {
            completion(destinationUrl.path, nil)
        }
        else
        {
            let session = URLSession(configuration: URLSessionConfiguration.default, delegate: nil, delegateQueue: nil)
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            let task = session.dataTask(with: request, completionHandler:
            {
                data, response, error in
                if error == nil
                {
                    if let response = response as? HTTPURLResponse
                    {
                        if response.statusCode == 200
                        {
                            if let data = data
                            {
                                if let _ = try? data.write(to: destinationUrl, options: Data.WritingOptions.atomic)
                                {
                                    completion(destinationUrl.path, error)
                                }
                                else
                                {
                                    completion(destinationUrl.path, error)
                                }
                            }
                            else
                            {
                                completion(destinationUrl.path, error)
                            }
                        }
                    }
                }
                else
                {
                    completion(destinationUrl.path, error)
                }
            })
            task.resume()
        }
    }
    
//  MARK: -Download Mp3 Files-
    func downloadFiles(folderName: String, fileName: String, fileUrl: String, completionHandler: @escaping (Int, Error?) -> ()) {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        
        var documentsDirectory = paths[0]
        
        documentsDirectory += "/\(fileName)-.zip"
        let tmporaryDirectoryURL = FileManager.default.temporaryDirectory
     
        FileDownloader.loadFileAsync(url: URL(string: fileUrl)!) { (path, error) in
            self.unzipFile(zipPath: URL(string: "file://\(path!)")!,
                           unzipPath: self.tempUnzipPath(setName:
                                                         "\(folderName)",
                                                         filename: fileName)!)
        }
    }
    
//  MARK: -Unzip File-
    func unzipFile(zipPath: URL, unzipPath: URL){
        let fileManager = FileManager()
        do {
            try fileManager.unzipItem(at: zipPath, to: unzipPath)
            
        } catch let e {
            print("Error from unzip : ", e.localizedDescription)
        }
    }
    
//  MARK: -Temp Unzip Path-
    func tempUnzipPath(setName: String, filename: String) -> URL? {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        var documentsDirectory = paths[0]
        documentsDirectory += "/\(setName)/\(filename)"
        let url = URL(fileURLWithPath: documentsDirectory)
        
        do {
            print("url : ", url)
            Definations.controllerArray.append(url)
            print("url counter : ", Definations.controllerArray.count)
            if !UserDefaults.standard.bool(forKey: "newEffectData") {
                if Definations.controllerArray.count == 29 {
                    if !Definations.completed {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                            BanubaSdkManager.initialize (
                               resourcePath: [AppDelegate.documentsPath + "/MaskEffects", AppDelegate.documentsPath + "/MaskEffects"],
                               clientTokenString: banubaClientToken
                            )
                            
                            if FileDownloader.showFiles(effectDirectory: "/MaskEffects") != [] {
                                for item in FileDownloader.showFiles(effectDirectory: "/MaskEffects") {
                                    if !item.contains("Makeup") {
                                        Definations.unzipedFiles.append(item)
                                    } else {
                                        Definations.unzipedFiles.remove(object: item)
                                        print("contains = ", Definations.unzipedFiles)
                                    }
                                }
                            }
                            print("banuba sdk initialize succesfully")
                            defaults.setValue(true, forKey: "isBanubaLoaded")
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
                                NotificationCenter.default.post(name: NSNotification.Name.init("continueHomeScreen"), object: nil, userInfo: nil)
                            }
                        }
                        UserDefaults.standard.set(true, forKey: "newEffectData")
                        Definations.completed = true
                    }
                }
               
            } else {
                if !Definations.completed {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        BanubaSdkManager.initialize (
                           resourcePath: [AppDelegate.documentsPath + "/MaskEffects", AppDelegate.documentsPath + "/MaskEffects"],
                           clientTokenString: banubaClientToken
                        )
                        
                        if FileDownloader.showFiles(effectDirectory: "/MaskEffects") != [] {
                            for item in FileDownloader.showFiles(effectDirectory: "/MaskEffects") {
                                if !item.contains("Makeup") {
                                    Definations.unzipedFiles.append(item)
                                } else {
                                    Definations.unzipedFiles.remove(object: item)
                                    print("contains = ", Definations.unzipedFiles)
                                }
                            }
                        }
                         
                        print("banuba sdk initialize succesfully")
                        defaults.setValue(true, forKey: "isBanubaLoaded")
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
                            NotificationCenter.default.post(name: NSNotification.Name.init("continueHomeScreen"), object: nil, userInfo: nil)
                        }
                    }
                    Definations.completed = true
                }
            }
       
            try FileManager.default.createDirectory(at: url, withIntermediateDirectories: true, attributes: nil)
        } catch {
            print("temp unzip file error : " ,error.localizedDescription)
            return nil
        }
        return url
    }
    
//  MARK: -Save File Paths-
    public static func saveFilePathsofSets(fileName: String, filepath: URL){
        
        let category = LocalFilePathModel(fileName: fileName, filePath: filepath)
        
        do {
            let encodedData = try NSKeyedArchiver.archivedData(withRootObject: category, requiringSecureCoding: false)
            let userDefaults = UserDefaults.standard
            userDefaults.set(encodedData, forKey: fileName)
        } catch  {
            print(error.localizedDescription)
        }
    }
    
    public static func showFiles(effectDirectory: String) -> [String] {
        let fm = FileManager.default
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        
        var documentsDirectory = paths[0]
        documentsDirectory += effectDirectory
        
        do {
            let items1 = try fm.contentsOfDirectory(atPath: documentsDirectory)
           
            for item in items1 {
                do {
                    try! FileDownloader().unzipFile(zipPath: item.asURL(), unzipPath: item.asURL())
                    print("items print : ", try! item.asURL())
                }
                catch let e {
                    print("file unzip error : ", e.localizedDescription)
                }
            }
            return items1
        } catch {
            print("file error : ",error.localizedDescription)
            return []
        }
    }
}

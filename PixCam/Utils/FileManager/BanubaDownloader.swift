//
//  BanubaDownloader.swift
//  PixCam
//
//  Created by Orhan Erbas on 21.06.2021.
//

import FirebaseDatabase
import FirebaseStorage
import ZIPFoundation
import AVFoundation

class BanunbaDownloader {
    public static var ref: DatabaseReference!
    public static var storage = Storage.storage()
    
    public static func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    public static func downloadSetByID(file: String, setId: String, filename: String){
        let tmporaryDirectoryURL = FileManager.default.temporaryDirectory
        let localURL = tmporaryDirectoryURL.appendingPathComponent("\(setId)/\(filename)")
        localURL.appendingPathExtension("zip")
        let islandRef = storage.reference(forURL: file)


        let downloadTask = islandRef.write(toFile: localURL as URL) { url, error in
          if let error = error {
            print(error)
          } else {
            //unzipFile(zipPath: url!, unzipPath: tempUnzipPath(setName: "\(setId)", filename: filename)!)
            //saveFilePathsofSets(fileName: "\(setId)/\(filename)", filepath: tempUnzipPath(setName: "\(setId)", filename: filename)!)
            print(file + " File Downloaded !")
          }
        }
        
        let observer = downloadTask.observe(.success) { snapshot in
            print(snapshot)
        }
    }
}

//
//  UploadEffectsVC.swift
//  PixCam
//
//  Created by Orhan Erbas on 3.07.2021.
//

import UIKit
import NVActivityIndicatorView

class UploadEffectsVC: UIViewController {

    @IBOutlet weak var uploadTitle: UILabel!
    @IBOutlet weak var uploadDescription: UILabel!
    var activityIndicator : NVActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        uploadTitle.text = "Please wait, thanks for your patience".localized()
        uploadTitle.numberOfLines = 0
        uploadTitle.adjustsFontSizeToFitWidth = true
        uploadTitle.lineBreakMode = .byWordWrapping
        uploadTitle.textAlignment = .center
    }
    
    
    //MARK: Setup View
    func setupView() {
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: NSNotification.Name.init("selectCurrentScreen"), object: nil, userInfo: nil)
            
            NotificationCenter.default.addObserver(self, selector: #selector(self.notifObserver), name: NSNotification.Name.init("continueHomeScreen"), object: nil)
           
            UIView.animate(withDuration: 0.45) { [self] in
                let xAxis = self.view.bounds.size.width / 2;
                let yAxis = self.view.bounds.size.height / 3;

                let frame = CGRect(x: xAxis-20, y: yAxis, width: 45, height: 45)
                activityIndicator = NVActivityIndicatorView(frame: frame, type: .lineScale, color: .black)
                self.view.addSubview(activityIndicator)
                activityIndicator.startAnimating()
            }
            self.setDesc()
            _ = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(self.setDesc), userInfo: nil, repeats: true)
        }
    }

    //MARK: Timer Observer
    @objc func setDesc() {
        if Definations.uploadCounter == Definations.uploadDescriptions.count {
            Definations.uploadCounter = 0
        } else {
            Definations.uploadCounter += 1
            self.uploadDescription.alpha = 0
            UIView.animate(withDuration: 0.45) {
                self.uploadDescription.text = Definations.uploadDescriptions[Definations.uploadCounter - 1]
                self.uploadDescription.alpha = 1
            }
        }
    }
    
    //MARK: Notification Observer
    @objc func notifObserver(notification: Notification) {
        self.dismiss(animated: true, completion: nil)
        NotificationCenter.default.post(name: NSNotification.Name.init("contueBanubaVC"), object: nil, userInfo: nil)
    }
}

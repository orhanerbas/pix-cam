//
//  GlobalConstants.swift
//
//  Created by Orhan Erbas on 1.06.2021.
//

import Foundation

class GlobalConstants {
    static let BaseURL = "https://toonify.p.rapidapi.com/v0/"
    
    let toonyfyHd = 1
    let toonifyPlus = 2
    let emojify = 3
    let zombify = 4
    let caricature = 5
    let comic = 6
}

//
//  ToonyfysdEffectHandler.swift
//  PixCam
//
//  Created by Orhan Erbas on 1.06.2021.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

class ToonyfysdEffectHandler {
    static let shared = ToonyfysdEffectHandler()
    
    class func request(request: BaseRequest, success: @escaping ((_ json: JSON) -> Void), failure: @escaping ((_ error: Error) -> Void)) {
        guard let url = URL.init(string: "\(GlobalConstants.BaseURL)toonify") else { return }
       
        if request.files.count > 0 {
            AF.upload(multipartFormData: { (multi) in
                for item in request.files {
                    if let data = item.data {
                        multi.append(data, withName: item.name ?? "data", fileName: item.fileName, mimeType: "item.type.rawValue")
                    }
                }
                let par = ["Key":request.parameters]
                for (key, value) in par ["Key"] ?? [:] {
                    if let data = (value as AnyObject).data(using: String.Encoding.utf8.rawValue) {
                        multi.append(data, withName: key)
                    }
                }
            }, to: url, method: request.method, headers: BaseRequest.headers, interceptor: nil).uploadProgress { (progress) in
                
            }.responseData { response in
                ResponseHandler.responseHandler(response: response, request: request, url: url, success: success, failure: failure)
            }
        } else {
            AF.request(url, method: request.method, parameters: request.parameters, headers: BaseRequest.headers, interceptor: nil).validate().responseData { (response) in
                ResponseHandler.responseHandler(response: response, request: request, url: url, success: success, failure: failure)
            }
        }
    }
    
    class func requestWithSuccessfullRespnose(request: BaseRequest, showErrorMessage: Bool = true, success: @escaping ((_ json: JSON) -> Void)) {
        self.request(request: request, success: { (json) in
            success(json)
        }) { (error) in
            if showErrorMessage {
                print(error.localizedDescription)
            }
        }
    }
    
}

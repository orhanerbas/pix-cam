//
//  ResponseHandler.swift
//
//  Created by Orhan Erbas on 1.06.2021.
//

import Foundation
import Alamofire
import SwiftyJSON

class ResponseHandler {
    static let shared = ResponseHandler()
    
    static func responseHandler(response: AFDataResponse<Data>, showLoader: Bool = true, request: BaseRequest, url: URL ,success: @escaping ((_ json: JSON) -> Void), failure: @escaping ((_ error: Error) -> Void)) {
        debugPrint("************************* Request *************************")
        debugPrint("The url: \(url.absoluteString)")
        
        switch response.result {
            case .success(let data):
                do {
                    let json = try JSON.init(data: data)
                    debugPrint("Methods: \(request.method.rawValue)")
                    debugPrint("************************* Request *************************")
                        success(json)
                } catch(let error) {
                    debugPrint(error.localizedDescription)
                    failure(error)
                }
                break
            case .failure(let error):
                debugPrint(error.localizedDescription)
                failure(error)
                break
        }
    }
}

//
//  BaseRequest.swift
//
//  Created by Orhan Erbas on 1.06.2021.
//

import Foundation
import UIKit
import Alamofire

class BaseRequest {
    
    static var headers: HTTPHeaders {
         let dic: HTTPHeaders = [
             "X-RapidAPI-Key": "117aef1bb9mshe5b1f2bdf578b26p10d273jsna065853f4d5d",
             "x-rapidapi-host": "toonify.p.rapidapi.com"
         ]
     
        return dic
    }
    
    var url: String = ""
    var parameters: [String : Any] = [:]
    var method: HTTPMethod = .get
    var files: [BaseFile] = []
}


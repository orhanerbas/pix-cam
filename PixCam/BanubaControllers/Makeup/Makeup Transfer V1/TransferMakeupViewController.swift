import UIKit
import AssetsLibrary

import BanubaSdk
import BanubaEffectPlayer

enum SelectedButtonType {
    case none
    case sourceImage
    case destinationImage
}

class TransferMakeupViewController: UIViewController {
    @IBOutlet weak var sourceImageView: UIImageView!
    @IBOutlet weak var destinationImageView: UIImageView!
    @IBOutlet weak var resultImageView: UIImageView!
    @IBOutlet weak var selectSourceButton: UIButton!
    @IBOutlet weak var selectDestinationButton: UIButton!
    @IBOutlet weak var processButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    
    var sdkManager: BanubaSdkManager?
    
    var selectedButtonType: SelectedButtonType = .none
    
    var sourceImage: UIImage? {
        didSet {
            sourceImageView.image = sourceImage
            resultImage = nil
        }
    }
    
    var destinationImage: UIImage? {
        didSet {
            destinationImageView.image = destinationImage
            resultImage = nil
        }
    }
    
    var resultImage: UIImage? {
        didSet {
            resultImageView.image = resultImage
            updateViews()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        updateViews()
    }
    
    private func updateViews() {
        processButton.isEnabled = (sourceImageView.image != nil && destinationImageView.image != nil && resultImageView.image == nil) ? true : false
        saveButton.isEnabled = (resultImageView.image != nil)
        selectSourceButton.alpha = (sourceImageView.image != nil) ? 0 : 1
        selectDestinationButton.alpha = (destinationImageView.image != nil) ? 0 : 1
        processButton.alpha = (resultImageView.image != nil) ? 0 : 1
    }
    
    func showSelectMakeupFromImageVC() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SelectMakeupFromImageViewController") as! SelectMakeupFromImageViewController
        viewController.modalPresentationStyle = .fullScreen
        viewController.didSelectImageCallBack = { [weak self] image in
            guard let self = self else { return }
            switch self.selectedButtonType {
            case .destinationImage:
                self.destinationImage = image
            case .sourceImage:
                self.sourceImage = image
            default:
                fatalError("unexpected case")
            }
        }
        
        show(viewController, sender: nil)
    }
    
    private func showImagePickerVC(isSourceCamera: Bool) {
        let imagePickerVC = UIImagePickerController()
        if isSourceCamera {
            imagePickerVC.sourceType = .camera
        } else {
            imagePickerVC.sourceType = .photoLibrary
        }
        
        imagePickerVC.delegate = self
        imagePickerVC.modalPresentationStyle = .fullScreen
        show(imagePickerVC, sender: nil)
    }
    
    private func showAlertVC() {
        let alertVC = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        if selectedButtonType == .sourceImage {
            let actionSamplePhoto = UIAlertAction(title: "From sample photos", style: .default) { (_) in
                self.showSelectMakeupFromImageVC()
            }
            alertVC.addAction(actionSamplePhoto)
        }
        let actionCamera = UIAlertAction(title: "From Camera", style: .default) { (_) in
            self.showImagePickerVC(isSourceCamera: true)
        }
        let actionGallery = UIAlertAction(title: "From Gallery", style: .default) { (_) in
            self.showImagePickerVC(isSourceCamera: false)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertVC.addActions([actionCamera, actionGallery, cancelAction])
        
        show(alertVC, sender: nil)
    }
    
    // MARK: IBActions
    
    @IBAction func selectSourceButtonTapped(_ sender: Any) {
        selectedButtonType = .sourceImage
        showAlertVC()
    }
    
    @IBAction func selectDestinationButtonTapped(_ sender: Any) {
        selectedButtonType = .destinationImage
        showAlertVC()
    }
    
    @IBAction func saveButtonTapped(_ sender: Any) {
        UIImageWriteToSavedPhotosAlbum(self.resultImage!, nil, nil, nil)
        print("SAVED!")
    }
    
    
    @IBAction func processButtonTapped(_ sender: Any) {
        selectedButtonType = .none
        
        guard let srcPixelBuffer = self.sourceImage?.makeBgraPixelBuffer(),
            let targetPixelBuffer = self.destinationImage?.makeBgraPixelBuffer() else {
            print("ERROR - can't create pixel buffer")
            return
        }
        
        let srcImageData = BNBFullImageData(srcPixelBuffer,
                                            cameraOrientation: .deg0,
                                            requireMirroring: false,
                                            faceOrientation: 0,
                                            fieldOfView: 5)
        
        let targetImageData = BNBFullImageData(targetPixelBuffer,
                                               cameraOrientation: .deg0,
                                               requireMirroring: false,
                                               faceOrientation: 0,
                                               fieldOfView: 5)

        sdkManager!.stopEffectPlayer()
        sdkManager!.renderQueue.async { [weak self] in
            self!.sdkManager!.renderTarget?.activate()
            
            if let makeupTransferHandler = BNBMakeupTransfer.create() {
                if let output = makeupTransferHandler.processPhoto(srcImageData, targetImage: targetImageData, useToneTransfer: true) {
                    let resultImage = UIImage(rgbaDataNoCopy: output.image as NSData,
                                              width: Int(output.width),
                                              height: Int(output.height),
                                              channels: Int(output.channels))

                    DispatchQueue.main.async { [weak self] in
                      self?.resultImage = resultImage
                    }
                }
            }
        }
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}

extension UIImage
{
    /// This method doesn't use copying data, since it can heavily affect performance and memory usage
    /// (for high quality photos like 3024x4032 each copy of data has size ~30 Mb, and on slower devices making copy can take up to 0.2 sec).
    ///
    /// Base idea - we have NSData object which contains raw data, used for UIImage creation. Since we don't copy underlying data, we need to take control on
    /// lifetime of NSData object, otherwise internal content of UIImage will be destroyed.
    /// So, we manually transform NSData object into unmanaged pointer and increase its retain count by passRetained call, and in special callback of CGDataProvider,
    /// which will be called when UIImage is no longer needed, we release that unmanaged pointer by takeRetainedValue call, to prevent memory leaks.
    convenience init?(rgbaDataNoCopy: NSData, width: Int, height: Int, channels: Int) {
        let bufferLength = rgbaDataNoCopy.length
        let imageDataPointer: UnsafePointer<UInt8> = rgbaDataNoCopy.bytes.bindMemory(to: UInt8.self, capacity: bufferLength)
        
        let releaseImagePixelData: CGDataProviderReleaseDataCallback = { (info: UnsafeMutableRawPointer?, data: UnsafeRawPointer, size: Int) -> () in
            if let dataObjPointer = info {
                let _ = Unmanaged<NSData>.fromOpaque(dataObjPointer).takeRetainedValue()
            }
            return
        }
        
        let dataObjPointer = UnsafeMutableRawPointer(Unmanaged.passRetained(rgbaDataNoCopy).toOpaque())
        guard let dataProvider = CGDataProvider(dataInfo: dataObjPointer, data: imageDataPointer, size: bufferLength, releaseData: releaseImagePixelData) else {
            let _ = Unmanaged<NSData>.fromOpaque(dataObjPointer).takeRetainedValue()
            return nil
        }
        
        let colorSpaceRef = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = (channels == 4) ? CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue) :
            CGBitmapInfo(rawValue: CGImageAlphaInfo.none.rawValue)
        
        if let imageRef = CGImage(width: width,
                                  height: height,
                                  bitsPerComponent: 8,
                                  bitsPerPixel: 8 * channels,
                                  bytesPerRow: channels * width,
                                  space: colorSpaceRef,
                                  bitmapInfo: bitmapInfo,
                                  provider: dataProvider,
                                  decode: nil,
                                  shouldInterpolate: false,
                                  intent: CGColorRenderingIntent.defaultIntent) {
            self.init(cgImage: imageRef)
        } else {
            return nil
        }
    }
    
    func imageResizedToSize(_ size: CGSize) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        draw(in: CGRect(origin: .zero, size: size))
        let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return resizedImage
    }
    
    public convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        
        self.init(cgImage: cgImage)
    }
    
    func blurred(blurRadius: CGFloat) -> UIImage? {
        guard let blurEffect = CIFilter(name: "CIGaussianBlur"),
            let clampEffect = CIFilter(name: "CIAffineClamp"),
            let inputImage = CIImage(image: self) else { return nil }
        
        clampEffect.setDefaults()
        clampEffect.setValue(inputImage, forKey: kCIInputImageKey)
        
        blurEffect.setValue(clampEffect.outputImage, forKey: kCIInputImageKey)
        blurEffect.setValue(blurRadius, forKey: kCIInputRadiusKey)
        
        guard let ciImageResult = blurEffect.outputImage else { return nil }
        
        let ciContext = CIContext(options: nil)
        
        guard let cgImage = ciContext.createCGImage(ciImageResult, from: inputImage.extent) else { return nil }
        
        return UIImage(cgImage: cgImage, scale: scale, orientation: .up)
    }
}


extension TransferMakeupViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.originalImage] as? UIImage {
            switch selectedButtonType {
            case .destinationImage:
                destinationImage = image
            case .sourceImage:
                sourceImage = image
            default:
                fatalError("unexpected case")
            }
        } else {
            print("UIImagePickerControlle did not return any image")
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
}

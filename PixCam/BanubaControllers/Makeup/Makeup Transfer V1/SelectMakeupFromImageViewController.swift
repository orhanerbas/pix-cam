import UIKit
import BanubaEffectPlayer

class SelectMakeupFromImageViewController: UIViewController {
    let resources_dir = Bundle.init(for: BNBEffectPlayer.self).bundlePath + "/bnb-resources/"

    var images: [String] {
        get {
            return [
                "makeup_transfer/sources/makeuplook1.jpg",
                "makeup_transfer/sources/makeuplook2.png",
                "makeup_transfer/sources/makeuplook3.png",
                "makeup_transfer/sources/makeuplook4.png",
                "makeup_transfer/sources/makeuplook5.png",
                "makeup_transfer/sources/makeuplook6.png",
                "makeup_transfer/sources/makeuplook7.png",
                "makeup_transfer/sources/makeuplook8.png",
                "makeup_transfer/sources/makeuplook9.png",
                "makeup_transfer/sources/makeuplook10.png",
            ]
        }
    }
    @IBOutlet weak var applyButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var didSelectImageCallBack: ((UIImage?)->())?

    override func viewDidLoad() {
        super.viewDidLoad()

        let pageVC = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        
        if let pageView = pageVC.view {
            pageView.translatesAutoresizingMaskIntoConstraints = false
            view.insertSubview(pageView, at: 0)
            addChild(pageVC)
            pageVC.didMove(toParent: self)
            
            pageView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
            pageView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
            pageView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
            pageView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        }
        
        let nextIndex = 0
        pageVC.setViewControllers([viewControllerForImage(image: UIImage(named: resources_dir + images[nextIndex]), imageIndex: nextIndex)], direction: .forward, animated: false, completion: nil)
        pageVC.delegate = self
        pageVC.dataSource = self
        
        pageControl.numberOfPages = images.count
        pageControl.currentPage = nextIndex
    }
    
    func viewControllerForImage(image: UIImage?, imageIndex: Int) -> UIViewController {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "PreviewController") as! PreviewController
        viewController.image = image
        viewController.imageIndex = imageIndex
        viewController.needToHideBackButton = true
        
        return viewController
    }
    
    // MARK: IBActions
    
    @IBAction func cancelButtonDidTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func applyButtonDidTapped(_ sender: Any) {
        if let didSelectImageCallBack = didSelectImageCallBack {
            didSelectImageCallBack(UIImage(named: resources_dir + images[pageControl.currentPage]))
        }
        
        dismiss(animated: true, completion: nil)
    }
}

extension SelectMakeupFromImageViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if !completed { return }
        
        if let imageViewController = pageViewController.viewControllers?.first as? PreviewController {
            pageControl.currentPage = imageViewController.imageIndex!
        }
    }
}

extension SelectMakeupFromImageViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if let beforeVC = viewController as? PreviewController {
            if let index = beforeVC.imageIndex {
                if index > 0 {
                    let nextIndex = index - 1
                    return viewControllerForImage(image: UIImage(named: resources_dir + images[nextIndex]), imageIndex: nextIndex)
                }
            }
        }
        
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if let beforeVC = viewController as? PreviewController {
            if let index = beforeVC.imageIndex {
                if index < images.count - 1 {
                    let nextIndex = index + 1
                    return viewControllerForImage(image: UIImage(named: resources_dir + images[nextIndex]), imageIndex: nextIndex)
                }
            }
        }
        
        return nil
    }
}

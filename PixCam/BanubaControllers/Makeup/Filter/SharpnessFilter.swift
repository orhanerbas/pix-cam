import UIKit
import CoreImage

class SharpnessFilter {
    static func execute(image: UIImage, radius: Float, amount: Float) -> UIImage? {
        guard let filter = CIFilter(name: "CISharpenLuminance") else {
            fatalError("Cannot create CISharpenLuminance")
        }
        
        let inputImage = CIImage(image: image)
        filter.setValue(inputImage, forKey: kCIInputImageKey)
        filter.setValue(amount, forKey: kCIInputSharpnessKey)
        filter.setValue(radius, forKey: kCIInputRadiusKey)
        
        let context = CIContext(options: nil)
        var finalImage: UIImage?
        
        if let ciImage = filter.outputImage {
            let cgimg = context.createCGImage(ciImage, from: ciImage.extent)
            finalImage = UIImage(cgImage: cgimg!, scale: 1.0, orientation: image.imageOrientation)
        }
        
        return finalImage
    }
}

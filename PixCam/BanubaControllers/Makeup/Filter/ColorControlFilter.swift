import UIKit
import CoreImage

class ColorControlFilter {
    static private func buildColorFilter(for image: UIImage) -> CIFilter {
        guard let filter = CIFilter(name: "CIColorControls") else {
            fatalError("Cannot create CIColorControls")
        }
        
        let inputImage = CIImage(image: image)
        filter.setValue(inputImage, forKey: kCIInputImageKey)
        
        return filter
    }
    
    static private func draw(image: UIImage, filter: CIFilter) -> UIImage? {
        let context = CIContext(options: nil)
        var finalImage: UIImage?
        
        if let ciImage = filter.outputImage {
            let cgimg = context.createCGImage(ciImage, from: ciImage.extent)
            finalImage = UIImage(cgImage: cgimg!, scale: 1.0, orientation: image.imageOrientation)
        }
        
        return finalImage
    }
    
    static func execute(
        image: UIImage,
        brightness: Float,
        contrast: Float
    ) -> UIImage? {
        let filter = buildColorFilter(for: image)
        filter.setValue(brightness, forKey: kCIInputBrightnessKey)
        filter.setValue(contrast, forKey: kCIInputContrastKey)
        let finalImage = draw(image: image, filter: filter)

        return finalImage
    }
    
    static func execute(
        image: UIImage,
        saturation: Float
    ) -> UIImage? {
        let filter = buildColorFilter(for: image)
        filter.setValue(saturation, forKey: kCIInputSaturationKey)
        let finalImage = draw(image: image, filter: filter)
        
        return finalImage
    }
}

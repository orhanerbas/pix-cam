import UIKit
import Foundation

fileprivate struct ViewSettings {
    static let height: CGFloat = 44
}

fileprivate extension CGFloat {
    static let screenWidth = UIScreen.main.bounds.size.width
}

fileprivate extension UIFont {
    static let viewTextFont: UIFont = .systemFont(ofSize: 12)
}

fileprivate extension CGRect {
    static let defaultRect = CGRect(
        x: 0,
        y: 0,
        width: .screenWidth,
        height: ViewSettings.height
    )
}

class MakeupFilterParameter {
    let title: String
    
    init(title: String) {
        self.title = title
    }
    
    func buildView() -> UIView {
        let titleView = UILabel(frame: .defaultRect)
        titleView.text = title
        titleView.textColor = .white
        titleView.font = .viewTextFont
        titleView.textAlignment = .center
        titleView.sizeToFit()
        titleView.translatesAutoresizingMaskIntoConstraints = false
        
        return titleView
    }
    
    func currentValues() -> [String : Any] {
        return [:]
    }
    
    func update(with values: [String : Any]) {
        // do nothing because it is text
    }
}

class MakeupFilterParameterBool: MakeupFilterParameter {
    let defaultValue: Bool
    private(set) var currentValue: Bool
    
    init(title: String,
         defaulfValue: Bool
    ) {
        self.defaultValue = defaulfValue
        currentValue = defaultValue
        
        super.init(title: title)
    }
    
    override func buildView() -> UIView {
        let titleView = super.buildView()
        let placeHolderView = UIView(frame: .defaultRect)
        placeHolderView.translatesAutoresizingMaskIntoConstraints = false
        let switchView = UISwitch()
        switchView.translatesAutoresizingMaskIntoConstraints = false
        switchView.addTarget(self, action: #selector(switchValueChanged(sender:)), for: .valueChanged)
        
        placeHolderView.addSubview(titleView)
        placeHolderView.addSubview(switchView)
        
        NSLayoutConstraint.activate([
            titleView.leadingAnchor.constraint(equalTo: placeHolderView.leadingAnchor, constant: 0),
            titleView.centerYAnchor.constraint(equalTo: placeHolderView.centerYAnchor, constant: 0),
            switchView.trailingAnchor.constraint(equalTo: placeHolderView.trailingAnchor, constant: 0),
            switchView.centerYAnchor.constraint(equalTo: placeHolderView.centerYAnchor, constant: 0),
            placeHolderView.heightAnchor.constraint(equalToConstant: ViewSettings.height)
        ])
        
        return placeHolderView
    }
    
    @objc func switchValueChanged(sender: UISwitch) {
        currentValue = sender.isOn
    }
    
    override func currentValues() -> [String : Any] {
        let values = [
            title : currentValue
        ]
        let parentValues = super.currentValues()
        let all = parentValues.merging(values, uniquingKeysWith: { k1, k2 in return k2 })
        
        return all
    }
}

class MakeupFilterParameterFloat: MakeupFilterParameter {
    let defaultValue: Float
    let range: ClosedRange<Float>
    private(set) var currentValue: Float
    private var lastCurrentLabelView: UILabel?
    private let delimeter: Float
    private var sliderView: UISlider?
    
    init(
        title: String,
        defaulfValue: Float,
        range: ClosedRange<Float>,
        fractionDigitCount: Int = 0
    ) {
        self.defaultValue = defaulfValue
        currentValue = defaultValue
        self.range = range
        delimeter = powf(10.0, Float(fractionDigitCount))
        
        super.init(title: title)
    }
    
    override func buildView() -> UIView {
        let titleView = super.buildView()
        let placeHolderView = UIView(frame: .defaultRect)
        placeHolderView.translatesAutoresizingMaskIntoConstraints = false
        
        let sliderView = UISlider(frame: .defaultRect)
        sliderView.minimumValue = range.lowerBound
        sliderView.maximumValue = range.upperBound
        sliderView.value = defaultValue
        sliderView.translatesAutoresizingMaskIntoConstraints = false
        sliderView.addTarget(self, action: #selector(onSliderValueChanged(sender:)), for: .valueChanged)
        self.sliderView = sliderView
        
        let currentLabelView = UILabel(frame: .defaultRect)
        currentLabelView.text = "\(currentValue)"
        currentLabelView.textColor = .green
        currentLabelView.font = .viewTextFont
        currentLabelView.textAlignment = .center
        currentLabelView.sizeToFit()
        currentLabelView.translatesAutoresizingMaskIntoConstraints = false
        lastCurrentLabelView = currentLabelView
        
        placeHolderView.addSubview(titleView)
        placeHolderView.addSubview(sliderView)
        placeHolderView.addSubview(currentLabelView)
        
        NSLayoutConstraint.activate([
            titleView.leadingAnchor.constraint(equalTo: placeHolderView.leadingAnchor),
            titleView.centerYAnchor.constraint(equalTo: placeHolderView.centerYAnchor),
            sliderView.trailingAnchor.constraint(equalTo: placeHolderView.trailingAnchor),
            sliderView.widthAnchor.constraint(equalTo: placeHolderView.widthAnchor, multiplier: 3 / 5),
            sliderView.centerYAnchor.constraint(equalTo: placeHolderView.centerYAnchor),
            placeHolderView.heightAnchor.constraint(equalToConstant: ViewSettings.height),
            currentLabelView.centerYAnchor.constraint(equalTo: placeHolderView.centerYAnchor),
            currentLabelView.trailingAnchor.constraint(equalTo: sliderView.leadingAnchor, constant: -5)
        ])
        
        return placeHolderView
    }
    
    @objc func onSliderValueChanged(sender: UISlider) {
        changeCurrentValue()
    }
    
    private func changeCurrentValue() {
        guard let sliderView = sliderView else {
            return
        }
        
        currentValue = (Float(Int(sliderView.value * delimeter)) / delimeter)
        lastCurrentLabelView?.text = "\(currentValue)"
    }
    
    override func currentValues() -> [String : Any] {
        let values = [
            title : currentValue
        ]
        let parentValues = super.currentValues()
        let all = parentValues.merging(values, uniquingKeysWith: { k1, k2 in return k2 })
        
        return all
    }
    
    override func update(with values: [String : Any]) {
        guard let floatValue = values[title] as? Float else {
            return
        }
        
        sliderView?.value = floatValue
        changeCurrentValue()
    }
}

class MakeupFilterParameterChoice: MakeupFilterParameter {
    let values: [String]
    private var lastSegmentedControl: UISegmentedControl?
    private(set) var currentValue: String
    
    init(
        title: String,
        values: [String]
    ) {
        self.values = values
        currentValue = values.first ?? ""
        
        super.init(title: title)
    }
    
    override func buildView() -> UIView {
        let titleView = super.buildView()
        let placeHolderView = UIView(frame: .defaultRect)
        placeHolderView.translatesAutoresizingMaskIntoConstraints = false
        
        let segmentView = UISegmentedControl(items: values)
        segmentView.backgroundColor = .darkGray
        segmentView.translatesAutoresizingMaskIntoConstraints = false
        segmentView.addTarget(self, action: #selector(onSegmentValueChanged(sender:)), for: .valueChanged)
        lastSegmentedControl = segmentView
        
        if values.count > 0 {
            segmentView.selectedSegmentIndex = 0
        }
        
        placeHolderView.addSubview(titleView)
        placeHolderView.addSubview(segmentView)
        
        NSLayoutConstraint.activate([
            titleView.leadingAnchor.constraint(equalTo: placeHolderView.leadingAnchor, constant: 0),
            titleView.centerYAnchor.constraint(equalTo: placeHolderView.centerYAnchor, constant: 0),
            segmentView.trailingAnchor.constraint(equalTo: placeHolderView.trailingAnchor, constant: 0),
            segmentView.leadingAnchor.constraint(equalTo: placeHolderView.centerXAnchor, constant: 0),
            segmentView.centerYAnchor.constraint(equalTo: placeHolderView.centerYAnchor, constant: 0),
            placeHolderView.heightAnchor.constraint(equalToConstant: ViewSettings.height)
        ])
        
        return placeHolderView
    }
    
    @objc func onSegmentValueChanged(sender: UISegmentedControl) {
        currentValue = values[sender.selectedSegmentIndex]
    }
    
    override func currentValues() -> [String : Any] {
        let values = [
            title : currentValue
        ]
        let parentValues = super.currentValues()
        let all = parentValues.merging(values, uniquingKeysWith: { k1, k2 in return k2 })
        
        return all
    }
}

class MakeupFilter {
    let parameters: [MakeupFilterParameter]
    let view: UIView
    let id: UInt
    
    init(
        id: UInt,
        parameters: [MakeupFilterParameter]
    ) {
        self.parameters = parameters
        self.id = id
        view = {
            let placeholderView = UIStackView(frame: .defaultRect)
            placeholderView.axis = .vertical
            placeholderView.translatesAutoresizingMaskIntoConstraints = false
            parameters.forEach { placeholderView.addArrangedSubview($0.buildView()) }
            
            return placeholderView
        }()
    }
    
    func currentValues() -> [String : Any] {
        return parameters.map { $0.currentValues() }
            .flatMap { $0 }
            .reduce([String : Any]()) { (dict, tuple) in
                var nextDict = dict
                nextDict.updateValue(tuple.1, forKey: tuple.0)
                
                return nextDict
            }
    }
  
    func update(with values: [String : Any]) {
        parameters.forEach { $0.update(with: values) }
    }
}

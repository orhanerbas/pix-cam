import UIKit
import BanubaSdk
import BanubaEffectPlayer

class MakeupFiltersViewController: MakeupTransferBaseViewController {
    @IBOutlet weak var currentFilterPlaceholderView: UIView!
    @IBOutlet weak var filtersSegmentedControl: UISegmentedControl!
    
    let filters: [MakeupFilter] = MakeupFiltersDataSource().filters
    let neuroBeautyHandler = BNBNeuroBeauty.create()

    private var currentFilter: MakeupFilter? {
        didSet {
            changeFilterParametersView()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        selectCurrentFilter()
        perfButton.isHidden = true
        
        navigationItem.rightBarButtonItem =
            UIBarButtonItem(
                title: "Reset",
                style: .plain,
                target: self,
                action: #selector(reset)
            )
        
        self.sdkManager!.loadEffect("blur_bg", synchronous: true);
    }
    
    @objc func reset() {
        resultImageView.image = sourceImageView.image
    }
    
    override func canBeProcessed() -> Bool {
        resultImageView.image != nil
    }
    
    override func didPickSourceImage(_ image: UIImage) {
        super.didPickSourceImage(image)
        
        sourceImageView.image = image
        resultImageView.image = image
        updateViews()
    }
    
    private func executeF0InCoreImage(_ values: [String : Any]) {
        showDim(true)
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else {
                return
            }
            
            let startTime = CFAbsoluteTimeGetCurrent()
            
            let amount = values[F0.Value.amount] as? Float ?? 0
            let radius = values[F0.Value.radius] as? Float ?? 0
            
            let finalImage = SharpnessFilter.execute(
                image: self.resultImageView.image!,
                radius: radius,
                amount: amount
            )
            
            let endTime = CFAbsoluteTimeGetCurrent()
            let diff = endTime - startTime;
            print("-------------> Filters --------------> @", diff * 1000, " ms" )
            
            self.complete(with: finalImage)
        }
    }
    
    private func executeF1InCoreImage(_ values: [String : Any]) {
        showDim(true)
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else {
                return
            }
            
            let startTime = CFAbsoluteTimeGetCurrent()
            
            let brightless = values[F1.Value.brightless] as? Float ?? 0
            let contrast = values[F1.Value.contrast] as? Float ?? 0
            
            let finalImage = ColorControlFilter.execute(
                image: self.resultImageView.image!,
                brightness: brightless,
                contrast: contrast
            )
            
            let endTime = CFAbsoluteTimeGetCurrent()
            let diff = endTime - startTime;
            print("-------------> Filters --------------> @", diff * 1000, " ms" )
            
            self.complete(with: finalImage)
        }
    }
    
    private func executeF4InCoreImage(_ values: [String : Any]) {
        showDim(true)
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else {
                return
            }
            
            let startTime = CFAbsoluteTimeGetCurrent()
            let saturation = values[F4.Value.slider] as? Float ?? 0
            
            let finalImage = ColorControlFilter.execute(
                image: self.resultImageView.image!,
                saturation: saturation
            )
            
            let endTime = CFAbsoluteTimeGetCurrent()
            let diff = endTime - startTime;
            print("-------------> Filters --------------> @", diff * 1000, " ms" )
            
            self.complete(with: finalImage)
        }
    }
    
    override func proccess() {
        guard let currentFilter = currentFilter else {
            return
        }
        
        if currentFilter.id == F0.id {
            executeF0InCoreImage(currentFilter.currentValues())
            return
        }
//        else if currentFilter.id == F1.id {
//            executeF1InCoreImage(currentFilter.currentValues())
//            return
//        } else if currentFilter.id == F4.id {
//            executeF4InCoreImage(currentFilter.currentValues())
//            return
//        }
        
        self.showDim(true)

        DispatchQueue.main.async { [weak self] in
            guard let self = self else {
                return
            }
            
            var originalImage = self.resultImageView.image            
            guard let srcPixelBuffer = self.resultImageView.image?.makeBgraPixelBuffer() else {
                print("ERROR - can't create pixel buffer")
                return
            }
            
            let srcImageData = BNBFullImageData(
                srcPixelBuffer,
                cameraOrientation: .deg0,
                requireMirroring: false,
                faceOrientation: 0,
                fieldOfView: 5
            )

            self.sdkManager!.stopEffectPlayer()
            self.sdkManager!.renderQueue.async { [weak self] in
                self!.sdkManager!.renderTarget?.activate()
            
                var output : BNBNeuroBeautyOutput?
                var values = currentFilter.currentValues()
                switch currentFilter.id {
                case F0.id:
                    let amount = values[F0.Value.amount] as? Float ?? 0
                    let radius = values[F0.Value.radius] as? Float ?? 0
                    
                    output = self?.neuroBeautyHandler?.executeF0(
                        srcImageData,
                        amount: amount,
                        radius: radius)
                    
                case F1.id:
                    let autoTune = values[F1.Value.autoTune] as? Bool ?? false
                    if ( autoTune )
                    {
                        let tuneOutput = self?.neuroBeautyHandler?.executeAutoTune(srcImageData)
                        values[F1.Value.brightless] = tuneOutput!.brightness
                        values[F1.Value.contrast] = tuneOutput!.contrast
                        DispatchQueue.main.sync {
                            currentFilter.update(with: values)
                        }
                        
                        print("------> Auto-tune brightness = ", tuneOutput!.brightness)
                        print("------> Auto-tune contrast = ", tuneOutput!.contrast)
                    }
                    
                    let brightless = values[F1.Value.brightless] as? Float ?? 0
                    let contrast = values[F1.Value.contrast] as? Float ?? 0
                    let useLegacy = values[F1.Value.useLegacy] as? Bool ?? false
                    
                    output = self?.neuroBeautyHandler?.executeF1(
                        srcImageData,
                        brightness: brightless,
                        contrast: contrast,
                        useLegacy: useLegacy)
                    

                case F2.id:
                    let strength = values[F2.Value.strength] as? Float ?? 0
                    let noiseAlgorithm = F2.NoiseAlgorithm(rawValue: values[F2.Value.noiseAlgorithm] as! String) ?? .gaussian
                    let monochromatic = values[F2.Value.monochromatic] as? Bool ?? false
                    
                    switch noiseAlgorithm {
                    case .gaussian:
                        output = self?.neuroBeautyHandler?.executeF2(
                            srcImageData,
                            strength: strength,
                            noiseAlgorithm: 0,
                            monochromatic: monochromatic)
                        
                    case .uniform:
                        output = self?.neuroBeautyHandler?.executeF2(
                            srcImageData,
                            strength: strength,
                            noiseAlgorithm: 1,
                            monochromatic: monochromatic)
                    }
                    
                case F3.id:
                    self?.sdkManager!.processImageData(originalImage!, completion: { [weak self] (result) in
                        self?.complete(with: result)
                    })
                    
                case F4.id:
                    let slider = values[F4.Value.slider] as? Float ?? 0
                    
                    output = self?.neuroBeautyHandler?.executeF3(
                        srcImageData,
                        saturation: slider)

                default:
                    fatalError("Unknown filter id")
                }
                
                if ( output != nil )
                {
                    let resultImage = UIImage(
                        rgbaDataNoCopy: output!.image as NSData,
                        width: Int(output!.width),
                        height: Int(output!.height),
                        channels: Int(output!.channels)
                    )
                    
                    self?.complete(with: resultImage)
                }
            }
        }
    }
    
    private func complete(with image: UIImage?) {
        DispatchQueue.main.async { [weak self] in
            self?.showDim(false)
            self?.resultImage = image
        }
    }
    
    private func selectCurrentFilter() {
        currentFilter = filters[filtersSegmentedControl.selectedSegmentIndex]
    }
    
    private func changeFilterParametersView() {
        currentFilterPlaceholderView.subviews.forEach { $0.removeFromSuperview() }
        
        guard let filter = currentFilter else {
            return
        }
        
        let filterView = filter.view
        currentFilterPlaceholderView.addSubview(filterView)
        currentFilterPlaceholderView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            currentFilterPlaceholderView.topAnchor.constraint(equalTo: filterView.topAnchor, constant: 0),
            currentFilterPlaceholderView.leadingAnchor.constraint(equalTo: filterView.leadingAnchor, constant: 0),
            currentFilterPlaceholderView.trailingAnchor.constraint(equalTo: filterView.trailingAnchor, constant: 0),
        ])
    }
}

// MARK: - Actions

extension MakeupFiltersViewController {
    @IBAction func onFilterChanged(_ sender: Any) {
        selectCurrentFilter()
    }
    
    @IBAction func resetButtonAction(_ sender: Any) {
        reset()
    }
}

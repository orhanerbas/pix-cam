import Foundation

class MakeupFiltersDataSource {
    let filters: [MakeupFilter] = [
        MakeupFilter(
            id: F0.id,
            parameters:
                [
                    MakeupFilterParameterFloat(title: F0.Value.amount, defaulfValue: 1.5, range: 0...3, fractionDigitCount: 1),
                    MakeupFilterParameterFloat(title: F0.Value.radius, defaulfValue: 1, range: 0...10)
                ]
        ),
        
        MakeupFilter(
            id: F1.id,
            parameters:
                [
                    MakeupFilterParameterFloat(title: F1.Value.brightless, defaulfValue: 0, range: -150...150, fractionDigitCount: 1),
                    MakeupFilterParameterFloat(title: F1.Value.contrast, defaulfValue: 0, range: -100...100, fractionDigitCount: 1),
                    MakeupFilterParameterBool(title: F1.Value.autoTune, defaulfValue: false),
                    MakeupFilterParameterBool(title: F1.Value.useLegacy, defaulfValue: false)
                ]
        ),
        
        MakeupFilter(
            id: F2.id,
            parameters:
                [
                    MakeupFilterParameterFloat(title: F2.Value.strength, defaulfValue: 0.2, range: 0...1, fractionDigitCount: 1),
                    MakeupFilterParameterChoice(title: F2.Value.noiseAlgorithm, values: F2.NoiseAlgorithm.allCases.map { $0.rawValue }),
                    MakeupFilterParameterBool(title: F2.Value.monochromatic, defaulfValue: false)
                ]
        ),
        
        MakeupFilter(
            id: F3.id,
            parameters:
                [
                    MakeupFilterParameter(title: "Empty")
                ]
        ),
        
        MakeupFilter(
            id: F4.id,
            parameters:
                [
                    MakeupFilterParameterFloat(title: F4.Value.slider, defaulfValue: 0, range: -70...70, fractionDigitCount: 1)
                ]
        )
    ]
}

enum F0 {
    static let id: UInt = 0
    
    enum Value {
        static let amount = "Amount"
        static let radius = "Radius"
    }
}

enum F1 {
    static let id: UInt = 1
    
    enum Value {
        static let brightless = "Brightness"
        static let contrast = "Contrast"
        static let autoTune = "Auto-tune Brightness & Contrast"
        static let useLegacy = "Use legacy"
    }
}

enum F2 {
    static let id: UInt = 2
    
    enum Value {
        static let strength = "Strength"
        static let noiseAlgorithm = "Noise Algorithm"
        static let monochromatic = "Monochromatic"
    }
    
    enum NoiseAlgorithm: String, CaseIterable {
        case gaussian = "Gaussian"
        case uniform = "Uniform"
    }
}

enum F3 {
    static let id: UInt = 3
}

enum F4 {
    static let id: UInt = 4
    
    enum Value {
        static let slider = "Slider"
    }
}

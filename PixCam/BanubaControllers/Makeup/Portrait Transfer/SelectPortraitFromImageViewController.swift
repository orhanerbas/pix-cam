import UIKit
import BanubaEffectPlayer

class SelectPortraitFromImageViewController: SelectMakeupFromImageViewController {
    override var images: [String] {
        get {
            return [
                "test/portrait_match/pictures/000000.jpeg",
                "test/portrait_match/pictures/000001.jpeg",
                "test/portrait_match/pictures/000002.jpeg",
                "test/portrait_match/pictures/000003.jpeg",
                "test/portrait_match/pictures/000004.jpeg",
                "test/portrait_match/pictures/000005.jpeg",
                "test/portrait_match/pictures/000006.jpeg",
                "test/portrait_match/pictures/000007.jpeg",
                "test/portrait_match/pictures/000008.jpeg",
                "test/portrait_match/pictures/000009.jpeg",
                "test/portrait_match/pictures/000010.jpeg",
                "test/portrait_match/pictures/000011.jpeg",
                "test/portrait_match/pictures/000012.jpeg",
                "test/portrait_match/pictures/000013.jpeg",
                "test/portrait_match/pictures/000014.jpeg",
                "test/portrait_match/pictures/000015.jpeg",
                "test/portrait_match/pictures/000016.jpeg",
                "test/portrait_match/pictures/000017.jpeg",
                "test/portrait_match/pictures/000018.jpeg",
                "test/portrait_match/pictures/000019.jpeg",
                "test/portrait_match/pictures/000020.jpeg",
            ]
        }
    }
}

import Foundation
import UIKit
import AssetsLibrary

import BanubaSdk
import BanubaEffectPlayer

class PortraitTransferViewController: TransferMakeupViewController {
    @IBAction override func processButtonTapped(_ sender: Any) {
        selectedButtonType = .none
        
        guard let srcPixelBuffer = self.sourceImage?.makeBgraPixelBuffer(),
            let targetPixelBuffer = self.destinationImage?.makeBgraPixelBuffer() else {
            print("ERROR - can't create pixel buffer")
            return
        }
        
        let fov = Float(55)
        let portraitImageData = BNBFullImageData(srcPixelBuffer,
                                            cameraOrientation: .deg0,
                                            requireMirroring: false,
                                            faceOrientation: 0,
                                            fieldOfView: fov)
        let userImageData = BNBFullImageData(targetPixelBuffer,
                                               cameraOrientation: .deg0,
                                               requireMirroring: false,
                                               faceOrientation: 0,
                                               fieldOfView: fov)
        
        sdkManager!.stopEffectPlayer()
        sdkManager!.renderQueue.async { [weak self] in
            self!.sdkManager!.renderTarget?.activate()
            
            if let PortraitMatchHandler = BNBPortraitMatch.create("/test/portrait_match/data.json") {
                if let output = PortraitMatchHandler.processPortrait(userImageData, portraitImage: portraitImageData, fov: fov) {
                    let resultImage = UIImage(rgbaDataNoCopy: output.image as NSData,
                                              width: Int(output.width),
                                              height: Int(output.height),
                                              channels: Int(output.channels))

                    DispatchQueue.main.async { [weak self] in
                      self?.resultImage = resultImage
                    }
                }
            }
        }
    }
    
    override func showSelectMakeupFromImageVC() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SelectPortraitFromImageViewController") as! SelectPortraitFromImageViewController
        viewController.modalPresentationStyle = .fullScreen
        viewController.didSelectImageCallBack = { [weak self] image in
            guard let self = self else { return }
            switch self.selectedButtonType {
            case .destinationImage:
                print("destinationImage")
                self.destinationImage = image
            case .sourceImage:
                print("sourceImage")
                self.sourceImage = image
            default:
                fatalError("unexpected case")
            }
        }
        
        show(viewController, sender: nil)
    }
}

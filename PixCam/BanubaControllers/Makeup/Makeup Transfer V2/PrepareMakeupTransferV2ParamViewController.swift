import UIKit
import AssetsLibrary

import BanubaSdk
import BanubaEffectPlayer

extension String {
    static let parentFolder = "bnb-resources/neuro_beauty"
    static let lutsFolder = "luts"
    static let toneFolder = "tone_texts"
    static let eyeFolder = "eye_texts"
    static let lashesFolder = "lashes_texts"
    static let blushFolder = "blush_texts"
    
    static let noneText = "None"
}

class MakeupParameter {
    var fileItem: FileItem?
    var isEnabled: Bool = true
    var isValid: Bool {
        isEnabled && fileItem != nil
    }
    
    var pathForProccessing: String? {
        isEnabled ? fileItem?.url.path : nil
    }
}

class LutMakeupParameter: MakeupParameter {
    var useSkinOnly: Bool = false
}

class BlushMakeupParameter: MakeupParameter {
    var useAutocorrection: Bool = true
}

struct MakeupParams {
    var image: UIImage?
    var lut = LutMakeupParameter()
    var tone = MakeupParameter()
    var eye = MakeupParameter()
    var lashes = MakeupParameter()
    var blush = BlushMakeupParameter()
    var useEyesCorrection = true
    var useEyeBeautification = true
    var color: UIColor?
    var useColor = false
    
    var canBeProccessed: Bool {
        return (image != nil) &&
            (
                useEyesCorrection ||
                    useEyeBeautification ||
                    lut.isValid ||
                    tone.isValid ||
                    eye.isValid ||
                    lashes.isValid ||
                    blush.isValid
            )
    }
}

class PrepareMakeupTransferV2ParamViewController: MakeupTransferBaseViewController {
    @IBOutlet weak var chooseLutPathButton: UIButton!
    @IBOutlet weak var skinOnlySwitch: UISwitch!
    @IBOutlet weak var chooseToneButton: UIButton!
    @IBOutlet weak var chooseEyeButton: UIButton!
    @IBOutlet weak var useLutSwitch: UISwitch!
    @IBOutlet weak var useToneSwitch: UISwitch!
    @IBOutlet weak var useEyeSwitch: UISwitch!
    @IBOutlet weak var useEyesCorrectionSwitch: UISwitch!
    @IBOutlet weak var useLashesSwitch: UISwitch!
    @IBOutlet weak var chooseLashesButton: UIButton!
    @IBOutlet weak var useEyeBeautificationSwitch: UISwitch!
    @IBOutlet weak var useBlushSwitch: UISwitch!
    @IBOutlet weak var chooseBlushButton: UIButton!
    @IBOutlet weak var useBlushAutocorrectionSwitch: UISwitch!
    
    @IBOutlet weak var colorPickerSwitch: UISwitch!
    private var makeupParams: MakeupParams = MakeupParams()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func canBeProcessed() -> Bool {
        makeupParams.canBeProccessed
    }
    
    override func didPickSourceImage(_ image: UIImage) {
        makeupParams.image = image
        sourceImageView.image = image
        super.didPickSourceImage(image)
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else {
                return
            }
            let time_00 = DispatchTime.now()
            
            self.uploadImage()
            
            let time_01 = DispatchTime.now()
            let nanoTime_00 = time_01.uptimeNanoseconds - time_00.uptimeNanoseconds
            let timeInterval_00 = Double(nanoTime_00) / 1_000_000
            print("Time to uploadImage: \(timeInterval_00) milliseconds")
        }
    }
    
    override func updateViews() {
        super.updateViews()
        
        chooseLutPathButton.setTitle(
            makeupParams.lut.fileItem?.name ?? .noneText,
            for: .normal
        )
        skinOnlySwitch.isOn = makeupParams.lut.useSkinOnly
        
        chooseToneButton.setTitle(
            makeupParams.tone.fileItem?.name ?? .noneText,
            for: .normal
        )
        
        chooseEyeButton.setTitle(
            makeupParams.eye.fileItem?.name ?? .noneText,
            for: .normal
        )
        
        chooseLashesButton.setTitle(
            makeupParams.lashes.fileItem?.name ?? .noneText,
            for: .normal
        )
        
        chooseBlushButton.setTitle(
            makeupParams.blush.fileItem?.name ?? .noneText,
            for: .normal
        )
        useBlushAutocorrectionSwitch.isOn = makeupParams.blush.useAutocorrection
        
        useEyesCorrectionSwitch.isOn = makeupParams.useEyesCorrection
        useEyeBeautificationSwitch.isOn = makeupParams.useEyeBeautification
        
        colorPickerSwitch.isOn = makeupParams.useColor
        colorPickerSwitch.onTintColor = makeupParams.color
    }
    
    private func showFolderContentSelectionController(
        _ initialDirertory: URL,
        mode: FolderContentDisplayMode,
        completion: ((FileItem) -> Void)?
    ) {
        let storyboard = UIStoryboard.init(name: "FolderContent", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "FolderContentSelectionViewController") as! FolderContentSelectionViewController
        viewController.modalPresentationStyle = .fullScreen
        viewController.displayMode = mode
        viewController.directoryToShow = initialDirertory
        viewController.completion = completion
        show(viewController, sender: nil)
    }
    
    private func folderUrl(for folder: String) -> URL {
        guard let parentFolder =
                Bundle.init(for: BNBEffectPlayer.self).url(
                    forResource: .parentFolder,
                    withExtension: nil
                )
        else {
            fatalError("Parent directory path is not found for makeup tech V2")
        }
        
        let requiredFolder = parentFolder.appendingPathComponent(folder, isDirectory: true)
        
        return requiredFolder
    }
    
    override func proccess() {
        guard makeupParams.canBeProccessed else {
            fatalError("Parameters for makeup proccessing are not valid")
        }
        
        showDim(true)
        DispatchQueue.main.async { [weak self] in
            guard let self = self else {
                return
            }
            
            if let neuroBeautyHandler = BNBNeuroBeauty.create() {
                self.applyTextures(
                    lutPath: self.makeupParams.lut.pathForProccessing,
                    useOnlySkin: self.makeupParams.lut.useSkinOnly,
                    tonePath: self.makeupParams.tone.pathForProccessing,
                    blushPath: self.makeupParams.blush.pathForProccessing,
                    useBlushAutocorrection: self.makeupParams.blush.useAutocorrection,
                    eyePath: self.makeupParams.eye.pathForProccessing,
                    lashesPath: self.makeupParams.lashes.pathForProccessing,
                    applyScleraMod: self.makeupParams.useEyesCorrection,
                    applyEyesBeauty: self.makeupParams.useEyeBeautification,
                    neuroBeautyHandler: neuroBeautyHandler,
                    useColor: self.makeupParams.useColor,
                    color: self.makeupParams.color
                )
            } else {
                self.showDim(false)
            }
        }
    }
    
    private func measurePerf() {
        DispatchQueue.main.async {
            if let neuroBeautyHandler = BNBNeuroBeauty.create() {
                neuroBeautyHandler.runPerfTest()
            }
        }
    }
    
    private func saveImageToGallery() {
        UIImageWriteToSavedPhotosAlbum(resultImage!, nil, nil, nil)
    }
    
    private func uploadImage() {
        sdkManager!.stopEffectPlayer()
        _ = sdkManager!.loadEffect("test_Neuro_beauty", synchronous: true)
        if let image = self.sourceImageView.image {
            sdkManager!.startEditingImage(image, fieldOfView: 5)
        }
    }
    
    private func applyTextures(
        lutPath: String?,
        useOnlySkin: Bool,
        tonePath: String?,
        blushPath: String?,
        useBlushAutocorrection: Bool,
        eyePath: String?,
        lashesPath: String?,
        applyScleraMod: Bool,
        applyEyesBeauty: Bool,
        neuroBeautyHandler : BNBNeuroBeauty,
        useColor: Bool,
        color: UIColor?
    ) {
        sdkManager!.stopEffectPlayer()
        sdkManager!.renderQueue.async { [weak self] in
            let time_00 = DispatchTime.now()
            self!.sdkManager!.renderTarget?.activate()
            
            let textureData = BNBNeuroBeautyInput(
                lutPath: lutPath ?? "",
                lutOnlySkin: useOnlySkin,
                tonePath: tonePath ?? "",
                blushPath: blushPath ?? "",
                blushAutoCorrection: useBlushAutocorrection,
                eyePath: eyePath ?? "",
                applyScleraMod: applyScleraMod,
                applyEyeFiltration: applyEyesBeauty,
                eyelashPath: lashesPath ?? "",
                lipsPainting: useColor,
                lipsColorR:Float(color?.redValue ?? 0),
                lipsColorG:Float(color?.greenValue ?? 0),
                lipsColorB:Float(color?.blueValue ?? 0)
            )
            
            if let fd = self!.sdkManager!.editingImageFrameData, let output = neuroBeautyHandler.applyTextures(fd, beautyParams: textureData) {
                let resultImage = UIImage(
                    rgbaDataNoCopy: output.image as NSData,
                    width: Int(output.width),
                    height: Int(output.height),
                    channels: Int(output.channels)
                )
                
                DispatchQueue.main.async { [weak self] in
                    self?.resultImage = resultImage
                    self?.showDim(false)
                }
            }
            let time_01 = DispatchTime.now()
            let nanoTime_01 = time_01.uptimeNanoseconds - time_00.uptimeNanoseconds
            let timeInterval_01 = Double(nanoTime_01) / 1_000_000
            print("Time to applyTextures: \(timeInterval_01) milliseconds")
        }
    }
    
    private func showColorPicker() {
        if #available(iOS 14.0, *) {
            let colorPickerVC = UIColorPickerViewController()
            colorPickerVC.delegate = self
            colorPickerVC.selectedColor = makeupParams.color ?? .black
            present(colorPickerVC, animated: true, completion: nil)
        } else {
            // Fallback on earlier versions
        }
        
    }
}

// MARK: - Actions

extension PrepareMakeupTransferV2ParamViewController {
    @IBAction func selectLutAction(_ sender: Any) {
        let folder = folderUrl(for: .lutsFolder)
        showFolderContentSelectionController(folder, mode: .filesOnly) { (fileItem) in
            self.makeupParams.lut.fileItem = fileItem
            self.updateViews()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func selectToneAction(_ sender: Any) {
        let folder = folderUrl(for: .toneFolder)
        showFolderContentSelectionController(folder, mode: .foldersOnly) { (fileItem) in
            self.makeupParams.tone.fileItem = fileItem
            self.updateViews()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func selectEyeAction(_ sender: Any) {
        let folder = folderUrl(for: .eyeFolder)
        showFolderContentSelectionController(folder, mode: .foldersOnly) { (fileItem) in
            self.makeupParams.eye.fileItem = fileItem
            self.updateViews()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func skinOnlyChanged(_ sender: Any) {
        makeupParams.lut.useSkinOnly = skinOnlySwitch.isOn
        updateViews()
    }
    
    @IBAction func useLutChanged(_ sender: Any) {
        makeupParams.lut.isEnabled = useLutSwitch.isOn
        updateViews()
    }
    
    @IBAction func useToneChanged(_ sender: Any) {
        makeupParams.tone.isEnabled = useToneSwitch.isOn
        updateViews()
    }
    
    @IBAction func useEyeChanged(_ sender: Any) {
        makeupParams.eye.isEnabled = useEyeSwitch.isOn
        updateViews()
    }
    
    @IBAction func useEyeCorrectionChanged(_ sender: Any) {
        makeupParams.useEyesCorrection = useEyesCorrectionSwitch.isOn
        updateViews()
    }
    
    @IBAction func useLashesChanged(_ sender: Any) {
        makeupParams.lashes.isEnabled = useLashesSwitch.isOn
        updateViews()
    }
    
    @IBAction func selectLashesAction(_ sender: Any) {
        let folder = folderUrl(for: .lashesFolder)
        showFolderContentSelectionController(folder, mode: .filesOnly) { (fileItem) in
            self.makeupParams.lashes.fileItem = fileItem
            self.updateViews()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func useEyeBeautificationChanged(_ sender: Any) {
        makeupParams.useEyeBeautification = useEyeBeautificationSwitch.isOn
        updateViews()
    }
    
    @IBAction func useBlushChanged(_ sender: Any) {
        makeupParams.blush.isEnabled = useBlushSwitch.isOn
        updateViews()
    }
    
    @IBAction func selectBlushAction(_ sender: Any) {
        let folder = folderUrl(for: .blushFolder)
        showFolderContentSelectionController(folder, mode: .filesOnly) { (fileItem) in
            self.makeupParams.blush.fileItem = fileItem
            self.updateViews()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func useBlushAutocorrectionChanged(_ sender: Any) {
        makeupParams.blush.useAutocorrection = useBlushAutocorrectionSwitch.isOn
        updateViews()
    }
    
    @IBAction func closeMakeupButtonAction(_ sender: Any) {
        super.closeView()
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else {
                return
            }
            
            _ = self.sdkManager!.loadEffect("", synchronous: true)
            self.sdkManager!.stopEditingImage(startCameraInput: true)
        }
    }
    
    @IBAction func useColorSwitchChanged(_ sender: Any) {
        if colorPickerSwitch.isOn {
            makeupParams.useColor = true
            showColorPicker()
        } else {
            makeupParams.useColor = false
            updateViews()
        }
    }
}

extension PrepareMakeupTransferV2ParamViewController: UIColorPickerViewControllerDelegate {
    @available(iOS 14.0, *)
    func colorPickerViewControllerDidSelectColor(_ viewController: UIColorPickerViewController) {
        makeupParams.color = viewController.selectedColor
        updateViews()
    }
}

import UIKit

class ShowImagesPageControllerViewController: UIViewController {
    @IBOutlet weak var pageControl: UIPageControl!
    var images: [UIImage]?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let pageVC = UIPageViewController(
            transitionStyle: .scroll,
            navigationOrientation: .horizontal,
            options: nil
        )
        
        if let pageView = pageVC.view {
            pageView.translatesAutoresizingMaskIntoConstraints = false
            view.insertSubview(pageView, at: 0)
            addChild(pageVC)
            pageVC.didMove(toParent: self)
            pageView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
            pageView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
            pageView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
            pageView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        }
        
        if images?.count ?? 0 > 0 {
            let nextIndex = 0
            pageVC.setViewControllers(
                [viewControllerForImage(image: images?[nextIndex], imageIndex: nextIndex)],
                direction: .forward,
                animated: false,
                completion: nil
            )
            pageVC.delegate = self
            pageVC.dataSource = self
            pageControl.numberOfPages = images?.count ?? 0
            pageControl.currentPage = nextIndex
        }
    }
    
    private func viewControllerForImage(image: UIImage?, imageIndex: Int) -> UIViewController {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "PreviewController") as! PreviewController
        viewController.image = image
        viewController.imageIndex = imageIndex
        viewController.needToHideBackButton = true
        
        return viewController
    }
}

extension ShowImagesPageControllerViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if !completed {
            return
        }
        
        if let imageViewController = pageViewController.viewControllers?.first as? PreviewController {
            pageControl.currentPage = imageViewController.imageIndex!
        }
    }
}

extension ShowImagesPageControllerViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if let beforeVC = viewController as? PreviewController {
            if let index = beforeVC.imageIndex {
                if index > 0 {
                    let nextIndex = index - 1
                    return viewControllerForImage(image: images?[nextIndex], imageIndex: nextIndex)
                }
            }
        }
        
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if let beforeVC = viewController as? PreviewController {
            if let index = beforeVC.imageIndex {
                if index < (images?.count ?? 0) - 1 {
                    let nextIndex = index + 1
                    return viewControllerForImage(image: images?[nextIndex], imageIndex: nextIndex)
                }
            }
        }
        
        return nil
    }
}

import UIKit

protocol PickerViewControllerDelegate: AnyObject {
    func didPick(_ value: String)
}

class PickerViewController: UIViewController {
    @IBOutlet weak var pickerView: UIPickerView!
    
    weak var delegate: PickerViewControllerDelegate?
    var dataSource: [String] = []
    var selectedItem: String?

    override func viewDidLoad() {
        super.viewDidLoad()

        guard let selectedItem = selectedItem else {
            return
        }
        
        guard let index = dataSource.firstIndex(where: { $0 == selectedItem }) else {
            return
        }
        
        pickerView.selectRow(index, inComponent: 0, animated: false)
    }
}

// MARK: - Actions

extension PickerViewController: UIPickerViewDelegate {
    @IBAction func done(_ sender: Any) {
        let value = dataSource[pickerView.selectedRow(inComponent: 0)]
        delegate?.didPick(value)
        dismiss(animated: true, completion: nil)
    }
}

extension PickerViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        dataSource.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        dataSource[row]
    }

    
    
}

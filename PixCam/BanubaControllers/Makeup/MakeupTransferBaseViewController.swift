import UIKit
import BanubaSdk
import BanubaEffectPlayer

class MakeupTransferBaseViewController: UIViewController {
    @IBOutlet weak var sourceImageView: UIImageView!
    @IBOutlet weak var selectSourceButton: UIButton!
    @IBOutlet weak var proccessButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var resultImageView: UIImageView!
    @IBOutlet weak var showImagesButton: UIButton!
    @IBOutlet weak var dimView: UIView!
    @IBOutlet weak var perfButton: UIButton!
    
    var sdkManager: BanubaSdkManager?
    var mainViewController: ViewController?
    
    var resultImage: UIImage? {
        didSet {
            resultImageView.image = resultImage
            updateViews()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        updateViews()
    }
    
    @objc
    public func closeView() {
        dismiss(animated: true, completion: nil)
    }
    
    func updateViews() {
        proccessButton.isEnabled = canBeProcessed()
        let isResultExist = resultImage != nil
        saveButton.isHidden = !isResultExist
        showImagesButton.isHidden = !isResultExist
        perfButton.isEnabled = true
        
        if sourceImageView.image != nil {
            selectSourceButton.setTitle(nil, for: .normal)
        }
    }
    
    func canBeProcessed() -> Bool {
        return false
    }
    
    func didPickSourceImage(_ image: UIImage) {
        resultImage = nil
        updateViews()
    }
    
    func proccess() {
        fatalError("Override and process images into the child class")
    }
    
    func showDim(_ show: Bool) {
        dimView.isHidden = !show
    }
    
    private func showAlertVC() {
        let cameraAction = UIAlertAction(
            title: "From Camera",
            style: .default
        ) { [unowned self](_) in
            self.showImagePickerVC(isSourceCamera: true)
        }
        let galleryAction = UIAlertAction(
            title: "From Gallery",
            style: .default
        ) { [unowned self](_) in
            self.showImagePickerVC(isSourceCamera: false)
        }
        let cancelAction = UIAlertAction(
            title: "Cancel",
            style: .cancel,
            handler: nil
        )
        let actionSheet = UIAlertController(
            title: nil,
            message: nil,
            preferredStyle: .actionSheet
        )
        actionSheet.addActions([cameraAction, galleryAction, cancelAction])
        present(
            actionSheet,
            animated: true,
            completion: nil
        )
    }
    
    private func showImagePickerVC(isSourceCamera: Bool) {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType =  isSourceCamera ? .camera : .photoLibrary
        imagePicker.delegate = self
        imagePicker.modalPresentationStyle = .fullScreen
        show(
            imagePicker,
            sender: nil
        )
    }
    
    private func measurePerf() {
        DispatchQueue.main.async {
            if let neuroBeautyHandler = BNBNeuroBeauty.create() {
                neuroBeautyHandler.runPerfTest()
            }
        }
    }
    
    private func saveImageToGallery() {
        UIImageWriteToSavedPhotosAlbum(resultImage!, nil, nil, nil)
    }
    
    private func showImagesPageController() {
        let storyboard = UIStoryboard.init(name: "TransferMakeupV2", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ShowImagesPageControllerViewController") as! ShowImagesPageControllerViewController
        viewController.modalPresentationStyle = .fullScreen
        viewController.images = [sourceImageView.image!, resultImage!]
        show(viewController, sender: nil)
    }
}

// MARK: - Actions

extension MakeupTransferBaseViewController {
    @IBAction func selectSourceImageAction(_ sender: Any) {
        showAlertVC()
    }
    
    @IBAction func proccessButtonAction(_ sender: Any) {
        proccess()
    }
    
    @IBAction func perfButtonAction(_ sender: Any) {
        measurePerf()
    }
    
    @IBAction func closeButtonAction(_ sender: Any) {
        closeView()
    }
    
    @IBAction func saveButtonAction(_ sender: Any) {
        saveImageToGallery()
    }
    
    @IBAction func showImagesAction(_ sender: Any) {
        showImagesPageController()
    }
}

// MARK: - UIImagePickerControllerDelegate, UINavigationControllerDelegate

extension MakeupTransferBaseViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(
        _ picker: UIImagePickerController,
        didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]
    ) {
        picker.dismiss(animated: true, completion: nil)
        
        guard let image = info[.originalImage] as? UIImage else {
            print("UIImagePickerControlle did not return any image")
            
            return
        }
        
        didPickSourceImage(image)
    }
}

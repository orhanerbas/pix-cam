//
//  PreviewController.swift
//  PixCam
//
//  Created by Orhan Erbas on 8.06.2021.
//

import UIKit

class PreviewController: UIViewController {
    
    var image: UIImage?
    var imageIndex: Int?
    var needToHideBackButton = false
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imageView.image = self.image
        
        backButton.isHidden = needToHideBackButton
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
}


//
//  BanubaViewController.swift
//  PixCam
//
//  Created by Orhan Erbas on 8.06.2021.
//

import UIKit
import AVKit
import VideoToolbox
import MobileCoreServices
import BanubaSdk
import BanubaEffectPlayer

class BanubaViewController: UIViewController {
    
    struct Defaults {
        static let renderSize: CGSize = CGSize(width: 720, height: 1280)
        static let PhotoCameraModeAspectRatio: CGFloat = 3.0 / 4.0
        static let VideoCameraModeAspectRatio: CGFloat = 9.0 / 16.0
        static let previewInteractiveIdentifier = "toPreviewInteractive"
        static let previewIdentifier = "toPreview"
        static let colors: [UIColor] = [
            #colorLiteral(red: 0.5607843137254902, green: 0.03529411764705882, blue: 0.09019607843137255, alpha: 1),
            #colorLiteral(red: 0.788235294117647, green: 0.6666666666666666, blue: 0.5450980392156862, alpha: 1),
            #colorLiteral(red: 0.12156862745098039, green: 0.12156862745098039, blue: 0.12156862745098039, alpha: 1),
            #colorLiteral(red: 0.5215686274509804, green: 0.7803921568627451, blue: 0.7176470588235294, alpha: 1),
            #colorLiteral(red: 0.35294117647058826, green: 0.596078431372549, blue: 0.11764705882352941, alpha: 1)
        ]
    }
    
    private var glView: EffectPlayerView!
    private let playerController = AVPlayerViewController()
    private let sdkManager = BanubaSdkManager()
    private var previewImage: UIImage?
    private var previewImageSrc: UIImage?
    private var binState = false
    private var isFrontCamera = true
    private var renderMode: EffectPlayerRenderMode = .photo
    private var frameDurationLogger: FrameDurationLogger! = nil
    private var rulerListener: RulerListener! = nil
    private var fakePhoto = false
    private var currentFeatureColor: UIColor = Defaults.colors.first!
    private var cameraSessionType: CameraSessionType {
        if isFrontCamera {
            return renderMode == .photo ? .FrontCameraPhotoSession : .FrontCameraVideoSession
        } else {
            return renderMode == .photo ? .BackCameraPhotoSession : .BackCameraVideoSession
        }
    }
    var isSelectedF = false
    var isSelectedS = false
    var isSelectedT = false
    var isSelectedFourth = false
    
    @IBOutlet weak var glViewContainer: UIView!
    @IBOutlet weak var photoButton: UIButton!
    @IBOutlet weak var videoButton: UIButton!
    @IBOutlet weak var effectsList: UICollectionView!
    @IBOutlet weak var sdkVersion: UILabel!
    @IBOutlet weak var renderLabel: UILabel!
    @IBOutlet weak var recognizerLabel: UILabel!
    @IBOutlet weak var cameraLabel: UILabel!
    @IBOutlet weak var switchCameraNote: UILabel!
    @IBOutlet weak var frameLoggerNote: UILabel!
    @IBOutlet weak var saveVideoMode: UISwitch!
    @IBOutlet weak var saveModeLabel: UILabel!
    @IBOutlet weak var buttonsEffectView: UIVisualEffectView!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var openGallery: UIButton!
    
    //Effect Buttons
    @IBOutlet weak var visualButton: UIVisualEffectView!
    @IBOutlet weak var firstButtonCons: NSLayoutConstraint!
    
    @IBOutlet weak var visualSecondButton: UIVisualEffectView!
    @IBOutlet weak var secondButtonCons: NSLayoutConstraint!
    
    @IBOutlet weak var visualThirtButton: UIVisualEffectView!
    @IBOutlet weak var thirtButtonCons: NSLayoutConstraint!
    
    @IBOutlet weak var visualFourthButton: UIVisualEffectView!
    @IBOutlet weak var fourthButtonCons: NSLayoutConstraint!
    
    @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var secondLabel: UILabel!
    @IBOutlet weak var thirtLabel: UILabel!
    @IBOutlet weak var fourthLabel: UILabel!
    
    @IBOutlet weak var rotateLabel: UILabel!
    @IBOutlet weak var backView: UIView!
    
    
    @IBOutlet weak var rotateView: UIView!
    @IBOutlet weak var showGalleryView: UIView!
    
    
    var demoLevels = [String]()
    var documentPath = ""
    var visibleItem = 0
    var currentMakeUpName = ""
    private var indexOfCellBeforeDragging = 0
    
    //MARK: ViewDidLoad
    override func viewDidLoad() {
        rotateLabel.text = "Rotate".localized()
        super.viewDidLoad()
        if Definations.unzipedFiles.count == 0 {
            DispatchQueue.main.async { [self] in
                if FileDownloader.showFiles(effectDirectory: "/MaskEffects") != [] {
                    for item in FileDownloader.showFiles(effectDirectory: "/MaskEffects") {
                        if !item.contains("Makeup") {
                            Definations.unzipedFiles.append(item)
                            self.demoLevels.append(item)
                        }
                    }
                }

                Definations.unzipedFiles.remove(object: "Makeup") 
                setupPlayer()
                setupBanubaSelectionWithLocal()
            }
        } else {
            Definations.unzipedFiles.remove(object: "Makeup")
            setupPlayer()
            setupBanubaSelectionWithLocal()
        }
        Definations.lastVCIdentifier = "BanubaViewController"
        loadEffect((self.demoLevels[0]))
    }
    
    deinit {
        sdkManager.destroyEffectPlayer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        sdkVersion.text = UIApplication.banubaVersion
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(reloadData),
            name: UIApplication.didBecomeActiveNotification,
            object: nil
        )
        effectsList.reloadData()
        configureCameraModeUI()
        addListeners()
        setFeatureColor(currentFeatureColor)
        sdkManager.input.startCamera()
        sdkManager.startEffectPlayer()
        navigationController?.setNavigationBarHidden(true, animated: animated)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
        removeListeners()
        sdkManager.input.stopCamera()
        navigationController?.setNavigationBarHidden(false, animated: animated)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    //MARK: ReloadData
    @objc func reloadData() {
        effectsList.reloadData()
    }
    
    //MARK: TakeFakePhoto
    func takeFakePhoto() {
        DispatchQueue.main.async { [weak self] in
            self!.fakePhoto = true
            self!.makePhoto(0)
        }
    }
    //MARK: SetupPlayer
    func setupPlayer() {
        let configuration = EffectPlayerConfiguration(renderMode: renderMode)
        sdkManager.setup(configuration: configuration)
        self.prepareRenderTargetLayer(renderMode)
        guard let layer = glView.layer as? CAEAGLLayer else {return}
        sdkManager.setRenderTarget(layer: layer, playerConfiguration: nil)
        sdkManager.setMaxFaces(2)
        TestHandler.setupTesting(sdkManager: sdkManager, view: self)

        //MARK: Watermark
        //guard let watermark = UIImage(named: "watermark") else {return}
        //let offset = CGPoint(x: 20.0, y: 10.0)
        //let watermarkInfo = WatermarkInfo(image: watermark, corner: .bottomLeft, offset: offset, targetNormalizedWidth: 0.7)
        //sdkManager.configureWatermark(watermarkInfo)
        
        documentPath = "/MaskEffects/"
        demoLevels = Definations.unzipedFiles
    }
    
    private func addListeners() {
        sdkManager.effectPlayer?.add(self as BNBFrameDurationListener)
        sdkManager.effectPlayer?.add(self as BNBEffectInfoListener)
        sdkManager.effectManager()?.add(self as BNBEffectEventListener)
    }
    
    private func removeListeners() {
        sdkManager.effectManager()?.remove(self as BNBEffectEventListener)
        sdkManager.effectPlayer?.remove(self as BNBFrameDurationListener)
        sdkManager.effectPlayer?.remove(self as BNBEffectInfoListener)
        removeRulerListener()
        removeFrameDurationLogger()
    }

    //MARK: FileURL
    func fileURL() -> URL {
        let fileUrl = EffectsService.shared.fm.temporaryDirectory.appendingPathComponent("video.mp4")
        return fileUrl
    }
    
    // MARK: - Makeup pipelines
    private func showTransferV1Pipeline() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "TransferMakeupViewController") as! TransferMakeupViewController
        viewController.modalPresentationStyle = .fullScreen
        viewController.sdkManager = sdkManager
        show(viewController, sender: nil)
    }
    
    private func showTransferV2Pipeline() {
        let storyboard = UIStoryboard.init(name: "TransferMakeupV2", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "PrepareMakeupTransferV2ParamViewController") as! PrepareMakeupTransferV2ParamViewController
        viewController.modalPresentationStyle = .fullScreen
        viewController.sdkManager = sdkManager
        show(viewController, sender: nil)
    }
    
    private func showMakeupFilterPipeline() {
        let storyboard = UIStoryboard.init(name: "FilterMakeup", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "MakeupFiltersViewController") as! MakeupFiltersViewController
        viewController.modalPresentationStyle = .fullScreen
        viewController.sdkManager = sdkManager
        show(viewController, sender: nil)
    }
    
    func setupBanubaSelectionWithLocal() {
        
        if Definations.selectedEffectCode != -1 {
            if Definations.selectedEffectCode == 1 {
                UIView.animate(withDuration: 0.35) {
                    self.firstButtonCons.constant = 145
                    self.visualButton.backgroundColor = .white
                    self.visualSecondButton.backgroundColor = .clear
                    self.visualThirtButton.backgroundColor = .clear
                    self.visualFourthButton.backgroundColor = .clear
                    self.secondButtonCons.constant = 44
                    self.thirtButtonCons.constant = 44
                    self.fourthButtonCons.constant = 44
                    self.firstLabel.alpha = 1
                    self.secondLabel.alpha = 0
                    self.thirtLabel.alpha = 0
                    self.fourthLabel.alpha = 0
                }
                isSelectedF = true
                isSelectedS = false
                isSelectedT = false
                isSelectedFourth = false
            }
            
            if Definations.selectedEffectCode == 2 {
                UIView.animate(withDuration: 0.35) {
                    self.secondButtonCons.constant = 145
                    self.visualSecondButton.backgroundColor = .white
                    self.visualButton.backgroundColor = .clear
                    self.visualThirtButton.backgroundColor = .clear
                    self.visualFourthButton.backgroundColor = .clear
                    self.firstButtonCons.constant = 44
                    self.thirtButtonCons.constant = 44
                    self.fourthButtonCons.constant = 44
                    self.secondLabel.alpha = 1
                    self.firstLabel.alpha = 0
                    self.thirtLabel.alpha = 0
                    self.fourthLabel.alpha = 0
                }
                isSelectedF = false
                isSelectedS = true
                isSelectedT = false
                isSelectedFourth = false
            }
            
            if Definations.selectedEffectCode == 3 {
                UIView.animate(withDuration: 0.55) {
                    self.thirtButtonCons.constant = 145
                    self.visualThirtButton.backgroundColor = .white
                    self.visualSecondButton.backgroundColor = .clear
                    self.visualButton.backgroundColor = .clear
                    self.visualFourthButton.backgroundColor = .clear
                    self.firstButtonCons.constant = 44
                    self.secondButtonCons.constant = 44
                    self.fourthButtonCons.constant = 44
                    self.thirtLabel.alpha = 1
                    self.firstLabel.alpha = 0
                    self.secondLabel.alpha = 0
                    self.fourthLabel.alpha = 0
                    
                }
                isSelectedF = false
                isSelectedS = false
                isSelectedT = true
                isSelectedFourth = false
            }
            
            if Definations.selectedEffectCode == 4 {
                UIView.animate(withDuration: 0.35) {
                    self.fourthButtonCons.constant = 145
                    self.visualFourthButton.backgroundColor = .white
                    self.visualSecondButton.backgroundColor = .clear
                    self.visualThirtButton.backgroundColor = .clear
                    self.visualButton.backgroundColor = .clear
                    self.thirtButtonCons.constant = 44
                    self.secondButtonCons.constant = 44
                    self.firstButtonCons.constant = 44
                    self.fourthLabel.alpha = 1
                    self.firstLabel.alpha = 0
                    self.secondLabel.alpha = 0
                    self.thirtLabel.alpha = 0
                    self.effectsList.alpha = 1
                }
                isSelectedF = false
                isSelectedS = false
                isSelectedT = false
                isSelectedFourth = true
                
                self.configureButtons()
                loadEffect((Definations.unzipedFiles[0]))
            }
        } else {
            self.configureButtons()
            loadEffect((Definations.unzipedFiles[0]))
        }
        
        let gestureFirst = UITapGestureRecognizer(target: self, action: #selector(self.gestureFirst))
        self.visualButton.addGestureRecognizer(gestureFirst)
        
        let gestureSecond = UITapGestureRecognizer(target: self, action: #selector(self.gestureSecond))
        self.visualSecondButton.addGestureRecognizer(gestureSecond)
        
        let gestureThirt = UITapGestureRecognizer(target: self, action: #selector(self.gestureThirt))
        self.visualThirtButton.addGestureRecognizer(gestureThirt)
        
        let gestureFourth = UITapGestureRecognizer(target: self, action: #selector(self.gestureFourth))
        self.visualFourthButton.addGestureRecognizer(gestureFourth)
        
        //Buttons UI
        self.visualButton.clipsToBounds = true
        self.visualButton.layer.cornerRadius = 12
        
        self.visualSecondButton.clipsToBounds = true
        self.visualSecondButton.layer.cornerRadius = 12
        
        self.visualThirtButton.clipsToBounds = true
        self.visualThirtButton.layer.cornerRadius = 12
        
        self.visualFourthButton.clipsToBounds = true
        self.visualFourthButton.layer.cornerRadius = 12
        
        let backGesture = UITapGestureRecognizer(target: self, action: #selector(self.backGestureObserver))
        self.backView.addGestureRecognizer(backGesture)
        
        let switchCameraGesture = UITapGestureRecognizer(target: self, action: #selector(self.switchCameraViewObserver))
        self.rotateView.addGestureRecognizer(switchCameraGesture)
        
        let showGalleryGesture = UITapGestureRecognizer(target: self, action: #selector(self.showGalleryViewObserver))
        self.showGalleryView.addGestureRecognizer(showGalleryGesture)
    }
    
    //MARK: Configure Blur Buttons
    func configureButtons() {
        //Gestures
        let gestureFirst = UITapGestureRecognizer(target: self, action: #selector(self.gestureFirst))
        self.visualButton.addGestureRecognizer(gestureFirst)
        
        let gestureSecond = UITapGestureRecognizer(target: self, action: #selector(self.gestureSecond))
        self.visualSecondButton.addGestureRecognizer(gestureSecond)
        
        let gestureThirt = UITapGestureRecognizer(target: self, action: #selector(self.gestureThirt))
        self.visualThirtButton.addGestureRecognizer(gestureThirt)
        
        let gestureFourth = UITapGestureRecognizer(target: self, action: #selector(self.gestureFourth))
        self.visualFourthButton.addGestureRecognizer(gestureFourth)
        
        //Buttons UI
        self.visualFourthButton.backgroundColor = .white
        self.isSelectedFourth = true
        
        self.visualButton.clipsToBounds = true
        self.visualButton.layer.cornerRadius = 12
        
        self.visualSecondButton.clipsToBounds = true
        self.visualSecondButton.layer.cornerRadius = 12
        
        self.visualThirtButton.clipsToBounds = true
        self.visualThirtButton.layer.cornerRadius = 12
        
        self.visualFourthButton.clipsToBounds = true
        self.visualFourthButton.layer.cornerRadius = 12
        
        self.firstLabel.alpha = 0
        self.secondLabel.alpha = 0
        self.thirtLabel.alpha = 0
        self.fourthLabel.alpha = 1
        
        //CollectionViewVisibility
        self.effectsList.alpha = 1
    }
    
    
    //MARK: Gestures
    @objc func gestureFirst() {
        
        if !isSelectedF {
            UIView.animate(withDuration: 0.35) {
                self.firstButtonCons.constant = 145
                self.visualButton.backgroundColor = .white
                self.visualSecondButton.backgroundColor = .clear
                self.visualThirtButton.backgroundColor = .clear
                self.visualFourthButton.backgroundColor = .clear
                self.secondButtonCons.constant = 44
                self.thirtButtonCons.constant = 44
                self.fourthButtonCons.constant = 44
                self.firstLabel.alpha = 1
                self.secondLabel.alpha = 0
                self.thirtLabel.alpha = 0
                self.fourthLabel.alpha = 0
            }
            isSelectedF = true
            isSelectedS = false
            isSelectedT = false
            isSelectedFourth = false
        } else {
            UIView.animate(withDuration: 0.35) {
                self.firstButtonCons.constant = 44
                self.visualButton.backgroundColor = .clear
                self.firstLabel.alpha = 0
            }
            isSelectedF = false
        }
    }
    
    
    @objc func gestureSecond() {
        
        if !isSelectedS {
            UIView.animate(withDuration: 0.35) {
                self.secondButtonCons.constant = 145
                self.visualSecondButton.backgroundColor = .white
                self.visualButton.backgroundColor = .clear
                self.visualThirtButton.backgroundColor = .clear
                self.visualFourthButton.backgroundColor = .clear
                self.firstButtonCons.constant = 44
                self.thirtButtonCons.constant = 44
                self.fourthButtonCons.constant = 44
                self.secondLabel.alpha = 1
                self.firstLabel.alpha = 0
                self.thirtLabel.alpha = 0
                self.fourthLabel.alpha = 0
            }
            isSelectedF = false
            isSelectedS = true
            isSelectedT = false
            isSelectedFourth = false
        } else {
            UIView.animate(withDuration: 0.35) {
                self.secondButtonCons.constant = 44
                self.visualSecondButton.backgroundColor = .clear
                self.secondLabel.alpha = 0
            }
            isSelectedS = false
        }
    }
    
    
    @objc func gestureThirt() {
//        self.demoLevels = Definations.unzipedLipsFiles
        self.effectsList.reloadData()
        
        if !isSelectedT {
            UIView.animate(withDuration: 0.55) {
                self.thirtButtonCons.constant = 145
                self.visualThirtButton.backgroundColor = .white
                self.visualSecondButton.backgroundColor = .clear
                self.visualButton.backgroundColor = .clear
                self.visualFourthButton.backgroundColor = .clear
                self.firstButtonCons.constant = 44
                self.secondButtonCons.constant = 44
                self.fourthButtonCons.constant = 44
                self.thirtLabel.alpha = 1
                self.firstLabel.alpha = 0
                self.secondLabel.alpha = 0
                self.fourthLabel.alpha = 0
                self.effectsList.alpha = 1
            }
            isSelectedF = false
            isSelectedS = false
            isSelectedT = true
            isSelectedFourth = false
        } else {
            UIView.animate(withDuration: 0.35) {
                self.thirtButtonCons.constant = 44
                self.visualThirtButton.backgroundColor = .clear
                self.thirtLabel.alpha = 0
            }
            isSelectedT = false
        }
    }
  
    @objc func switchCameraViewObserver() {
        isFrontCamera = !isFrontCamera
        if isFrontCamera {
            switchCameraNote.text = "Front camera"
        } else {
            switchCameraNote.text = "Back camera"
        }
        sdkManager.input.switchCamera(to: cameraSessionType) {
            print("RotateCamera")
        }
    }
  
    @objc func gestureFourth() {
        self.documentPath = "/MaskEffects/" 
//        self.demoLevels = Definations.unzipedFiles
        
        self.effectsList.reloadData()
        if !isSelectedFourth {
            UIView.animate(withDuration: 0.35) {
                self.fourthButtonCons.constant = 145
                self.visualFourthButton.backgroundColor = .white
                self.visualSecondButton.backgroundColor = .clear
                self.visualThirtButton.backgroundColor = .clear
                self.visualButton.backgroundColor = .clear
                self.thirtButtonCons.constant = 44
                self.secondButtonCons.constant = 44
                self.firstButtonCons.constant = 44
                self.fourthLabel.alpha = 1
                self.firstLabel.alpha = 0
                self.secondLabel.alpha = 0
                self.thirtLabel.alpha = 0
                self.effectsList.alpha = 1
            }
            isSelectedF = false
            isSelectedS = false
            isSelectedT = false
            isSelectedFourth = true
        } else {
            UIView.animate(withDuration: 0.35) {
                self.fourthButtonCons.constant = 44
                self.visualFourthButton.backgroundColor = .clear
                self.fourthLabel.alpha = 0
            }
            isSelectedFourth = false
        }
    }
    
    @objc func backGestureObserver() {
        DispatchQueue.main.async {
            self.getNewScreen(boardName: "Main", vcName: "HomeViewController")
        }
    }
    
    @objc func showGalleryViewObserver() {
        let picker = UIImagePickerController()
        picker.allowsEditing = false
        picker.delegate = self
        picker.modalPresentationStyle = .fullScreen
        present(picker, animated: true, completion: nil)
    }
    
    //MARK: OnStartBinButton
    @IBAction func onStartBinButton(_ sender: Any) {
        let button = sender as! UIButton
        binState = !binState
        sdkManager.setFrameDataRecord(binState)
        let title = binState ? "[Stop]" : "[Record BIN]"
        button.setTitle(title, for: .normal)
    }
    //MARK: OnDurationButtonClicked
    @IBAction func onDurationButtonClicked(_ sender: UIButton) {
        if frameDurationLogger == nil {
            frameDurationLogger = FrameDurationLogger()
            sdkManager.effectPlayer?.add(frameDurationLogger as BNBFrameDurationListener)
            frameLoggerNote.text = "Disable frame logger"
        } else {
            removeFrameDurationLogger()
        }
    }
      
    //MARK: MakePhoto
    @IBAction func makePhoto(_ sender: Any) {
        let makeStart = Date()
        sdkManager.stopEffectPlayer()
        let useInteractivePreview = true
        //flashMode parametr allows you to turn on, off, auto flashlight of your device.
        //But if your device use Arkit like iPhone X and above, flash won't work
        let settings = CameraPhotoSettings(useStabilization: true, flashMode: .off)
        sdkManager.makeCameraPhoto(cameraSettings: settings, flipFrontCamera: true, srcImageHandler: {
            [weak self] (srcCVPixelBuffer) in
            var cgImage: CGImage?
            VTCreateCGImageFromCVPixelBuffer(srcCVPixelBuffer, options: nil, imageOut: &cgImage)
            var orient: UIImage.Orientation
            switch (self?.sdkManager.imageOrientationForCameraPhoto ?? .deg0) {
            case .deg0:
                orient = UIImage.Orientation.up
            case .deg90:
                orient = UIImage.Orientation.right
            case .deg180:
                orient = UIImage.Orientation.down
            case .deg270:
                orient = UIImage.Orientation.left
            default:
                orient = UIImage.Orientation.up
            }
            let image = UIImage.init(cgImage: cgImage!, scale:1, orientation: orient)
            self?.previewImageSrc = image
        }) { [weak self] (image) in
            DispatchQueue.main.async {
                if let image = image {
                    DispatchQueue.main.async {
                        let vc = self?.storyboard?.instantiateViewController(withIdentifier: "ShareViewController") as! ShareViewController
                        vc.modalPresentationStyle = .fullScreen
                        vc.takenPhoto = image
                        let imageName = self?.currentMakeUpName.replacingOccurrences(of: "2F", with: "")
                        vc.effectTitle = imageName ?? ""
                        self!.present(vc, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    //MARK: SwitchCamera
    @IBAction func switchCamera(_ sender: Any) {
        isFrontCamera = !isFrontCamera
        if isFrontCamera {
            switchCameraNote.text = "Front camera"
        } else {
            switchCameraNote.text = "Back camera"
        }
        sdkManager.input.switchCamera(to: cameraSessionType) {
            print("RotateCamera")
        }
    }
    //MARK: SwitchRecordMode
    @IBAction func switchRecordMode(_ sender: Any) {
        sdkManager.stopEffectPlayer()
        sdkManager.removeRenderTarget();
        renderMode = renderMode == .photo ? .video : .photo
        sdkManager.input.setCameraSessionType(cameraSessionType)
        configureCameraModeUI()
        prepareRenderTargetLayer(renderMode)
        guard let layer = glView.layer as? CAEAGLLayer else {return}
        sdkManager.setRenderTarget(layer: layer, playerConfiguration: nil)
        sdkManager.startEffectPlayer()
    }
    //MARK: OpenGallery
    @IBAction func openGallery(_ sender: Any) {
        let picker = UIImagePickerController()
        picker.allowsEditing = false
        picker.delegate = self
        picker.modalPresentationStyle = .fullScreen
        present(picker, animated: true, completion: nil)
    }
    //MARK: RestartPlayer
    @IBAction func restartPlayer(_ sender: Any?) {
        removeFrameDurationLogger()
        removeRulerListener()
        sdkManager.destroyEffectPlayer()
        setupPlayer()
        sdkManager.startEffectPlayer()
    }
    //MARK: Transfer Makeup
    @IBAction func onTransferMakeupButton(_ sender: Any?) {
        let transferMakeupV1Action = UIAlertAction(
            title: "Transfer V1",
            style: .default)
        { [unowned self](_) in
            self.showTransferV1Pipeline()
        }
        let transferMakeupV2Action = UIAlertAction(
            title: "Transfer V2",
            style: .default)
        { [unowned self](_) in
            self.showTransferV2Pipeline()
        }
        let filterMakeupAction = UIAlertAction(
            title: "Filter",
            style: .default
        ) { [unowned self](_) in
            self.showMakeupFilterPipeline()
        }
        let cancelAction = UIAlertAction(
            title: "Cancel",
            style: .cancel,
            handler: nil
        )
        let ac = UIAlertController(
            title: "Makeup",
            message: nil,
            preferredStyle: .actionSheet
        )
        ac.addActions(
            [
                transferMakeupV1Action,
                transferMakeupV2Action,
                filterMakeupAction,
                cancelAction
            ]
        )
        
        present(ac, animated: true, completion: nil)
    }
    
    //MARK: ConfigureCameraModeUI
    private func configureCameraModeUI() {
        saveModeLabel.isHidden = renderMode == .photo
        saveVideoMode.isHidden = renderMode == .photo
        photoButton.isHidden = renderMode == .video
        videoButton.isHidden = renderMode == .photo
        
        photoButton.backgroundColor = .clear
        photoButton.layer.borderWidth = 4
        photoButton.layer.borderColor = UIColor.white.withAlphaComponent(0.6).cgColor
        photoButton.frame = CGRect(x: 160, y: 100, width: 50, height: 50)
        photoButton.layer.cornerRadius = 0.8 * photoButton.bounds.size.width + 2
        photoButton.clipsToBounds = true
    }
    
    private func saveVideoToGallery(fileURL: String) {
        if UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(fileURL) {
            UISaveVideoAtPathToSavedPhotosAlbum(fileURL, nil, nil, nil)
        }
    }
    //MARK: ToggleVideo
    @IBAction func toggleVideo(_ sender: Any) {
        let shouldRecord = !(sdkManager.output?.isRecording ?? false)
        let hasSpace =  sdkManager.output?.hasDiskCapacityForRecording() ?? true
        if shouldRecord && hasSpace {
            let fileURL = self.fileURL()
            sdkManager.input.startAudioCapturing()
            sdkManager.output?.startVideoCapturing(fileURL:fileURL) { (success, error) in
                print("Done Writing: \(success)")
                if let _error = error {
                    print(_error)
                }
                self.sdkManager.input.stopAudioCapturing()
                print("voiceChanger.isConfigured:\(self.sdkManager.voiceChanger?.isConfigured ?? false)")
                guard self.sdkManager.voiceChanger?.isConfigured ?? false else {
                    self.presentVideoController(fileURL: fileURL)
                    if self.saveVideoMode.isOn {
                        self.saveVideoToGallery(fileURL: fileURL.relativePath)
                    }
                    return
                }
                self.sdkManager.effectPlayer?.setEffectVolume(0.0)
                self.sdkManager.voiceChanger?.process(file: fileURL, completion: { (success, error) in
                    self.sdkManager.effectPlayer?.setEffectVolume(1.0)
                    print("--- Voice Changer:[Success:\(success)][Error:\(String(describing: error))]")
                    if success {
                        DispatchQueue.main.async {
                            self.presentVideoController(fileURL: fileURL)
                            if self.saveVideoMode.isOn {
                                self.saveVideoToGallery(fileURL: fileURL.relativePath)
                            }
                        }
                    }
                })
            }
            self.videoButton.setImage(UIImage(named: "stop_video"), for: .normal)
        } else {
            sdkManager.output?.stopVideoCapturing(cancel: false)
            self.videoButton.setImage(UIImage(named: "shutter_video"), for: .normal)
        }
    }
    
    //MARK: CloseButtonAction
    @IBAction func closeViewAction(_ sender: Any) {
        DispatchQueue.main.async {
            self.getNewScreen(boardName: "Main", vcName: "HomeViewController")
        }
    }
    
    //MARK: PrepareRenderTargetLayer
    func prepareRenderTargetLayer(_ renderMode: EffectPlayerRenderMode) {
        if glView != nil {
            glView.removeFromSuperview()
            glView = nil
        }
        let cameraSessionAspectRatio: CGFloat = (renderMode == .photo) ? Defaults.PhotoCameraModeAspectRatio : Defaults.VideoCameraModeAspectRatio
        let frame = calculateRenderLayerFrame(layerAspectRatio: cameraSessionAspectRatio)
        guard let effectPlayer = sdkManager.effectPlayer else { return }
        glView = EffectPlayerView(frame: frame)
        glView.effectPlayer = effectPlayer
        glView.isMultipleTouchEnabled = true
        glView.layer.contentsScale = UIScreen.main.scale
        glViewContainer.addSubview(glView)
        glViewContainer.addSubview(self.buttonsEffectView)
        glViewContainer.addSubview(self.closeButton)
        glViewContainer.addSubview(self.saveButton)
        glViewContainer.addSubview(self.rotateLabel)
        glViewContainer.addSubview(self.effectsList)
        glViewContainer.addSubview(self.photoButton)
        glViewContainer.addSubview(self.backView)
        glViewContainer.addSubview(self.rotateView)
    }
    //MARK: CalculateRenderLayerFrame
    private func calculateRenderLayerFrame(layerAspectRatio: CGFloat) -> CGRect {
        let screenSize = UIScreen.main.bounds.size
        let size = CGSize(width: self.view.frame.width, height: self.view.frame.height)
        return CGRect(origin: CGPoint(x: self.view.frame.origin.x, y: self.view.frame.origin.y), size: size)
    }
    //MARK:  Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Defaults.previewIdentifier {
            guard let previewController = segue.destination as? PreviewController else {return}
            previewController.image = self.previewImage
            self.previewImage = nil
            self.previewImageSrc = nil
        }
        if segue.identifier == Defaults.previewInteractiveIdentifier {
            guard let previewController = segue.destination as? InteractivePreviewController else {return}
            previewController.image = self.previewImage
            previewController.srcImage = self.previewImageSrc
            previewController.sdkManager = sdkManager
            self.previewImage = nil
            self.previewImageSrc = nil
        }
    }
    
    func presentVideoController(fileURL:URL) {
        let player = AVPlayer(url: fileURL)
        self.playerController.player = player
        self.present(self.playerController, animated: true, completion: nil)
    }
}
//MARK: EffectCell Custom Class
class EffectCell: UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView!
    
    func loadImageFromPath(imgPath: String) {
        var imageTemplate = UIImage(contentsOfFile: imgPath)
        if imageTemplate == nil {
            imageTemplate = UIImage(named: "eyes_prod")
        }
        image.image = imageTemplate
    }
}

//MARK: addNewEffectCell Custom Class
class addNewEffectCell: UICollectionViewCell {
    
    @IBOutlet weak var addView: UIView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        addView.layer.cornerRadius = addView.frame.width / 2
    }

}

// MARK: Collection  View Extension
extension BanubaViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return 0
        }
        else {
            return self.demoLevels.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "addCell", for: indexPath)
            return cell
        } else {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "effectCell", for: indexPath)
                    as? EffectCell else {return EffectCell()}
            
            let imgPath = AppDelegate.documentsPath + documentPath +  (self.demoLevels[indexPath.row])  + "/preview.png"
            cell.loadImageFromPath(imgPath: imgPath)
            cell.backgroundColor = .white
            cell.layer.cornerRadius = 0.5 * cell.bounds.size.width
            
            if indexPath.row == 1 {
                if let item = effectsList.indexPathsForVisibleItems.first {
                    effectsList.scrollToItem(at: item, at: .centeredHorizontally, animated: true)
                }
            }
            return cell
        }
    }
   
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            let documentPicker = UIDocumentPickerViewController(documentTypes: [kUTTypeArchive as String], in: .import)
            documentPicker.delegate = self
            if #available(iOS 11.0, *) {
                documentPicker.allowsMultipleSelection = false
            }
            present(documentPicker, animated: true, completion: nil)
        } else if indexPath.section == 1 {
            let effect_info = BNBEffectManager.getEffectInfo((self.demoLevels[indexPath.row]))
            print("Effect info before loading effect:")
            print("Url: " + effect_info.url)
            print("Uses audio: " + String(effect_info.usesAudio))
            print("Uses video: " + String(effect_info.usesVideo))
            print("Uses touches: " + String(effect_info.usesTouches))
            loadEffect((self.demoLevels[indexPath.row]))
            let generator = UINotificationFeedbackGenerator()
            generator.notificationOccurred(.success)
            effectsList.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 3) { [self] in
                self.photoButton.isUserInteractionEnabled = true
            }
        } else {
            //let effect_info = BNBEffectManager.getEffectInfo((externalEffects[indexPath.row]))
            //print("Url: " + effect_info.url)
            //print("Uses audio: " + String(effect_info.usesAudio))
            //print("Uses video: " + String(effect_info.usesVideo))
            //print("Uses touches: " + String(effect_info.usesTouches))
            //loadEffect((externalEffects[indexPath.row]))
        }
    }
     
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        for index in effectsList.indexPathsForVisibleItems {
            let visibleRect = CGRect(origin: effectsList.contentOffset, size: effectsList.bounds.size)
            let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
            let visibleIndexPath = effectsList.indexPathForItem(at: visiblePoint)
            let generator = UINotificationFeedbackGenerator()
            generator.notificationOccurred(.success)
           
            
            DispatchQueue.main.async { [self] in
                if visibleIndexPath != nil {
                    effectsList.scrollToItem(at: visibleIndexPath ?? index, at: .centeredHorizontally, animated: true)
                    loadEffect((self.demoLevels[Int(visibleIndexPath?[1] ?? 6)]))
                    self.currentMakeUpName = self.demoLevels[Int(visibleIndexPath?[1] ?? 6)]
                }
                self.photoButton.isUserInteractionEnabled = true
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 3) { [self] in
                self.photoButton.isUserInteractionEnabled = true
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        for index in effectsList.indexPathsForVisibleItems {
            let visibleRect = CGRect(origin: effectsList.contentOffset, size: effectsList.bounds.size)
            let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
            let visibleIndexPath = effectsList.indexPathForItem(at: visiblePoint)
            let generator = UINotificationFeedbackGenerator()
            generator.notificationOccurred(.success)
           
            
            DispatchQueue.main.async { [self] in
                if visibleIndexPath != nil {
                    effectsList.scrollToItem(at: visibleIndexPath ?? index, at: .centeredHorizontally, animated: true)
                    loadEffect((self.demoLevels[Int(visibleIndexPath?[1] ?? 6)]))
                }
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now()) { [self] in
                self.photoButton.isUserInteractionEnabled = true
            }
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.photoButton.isUserInteractionEnabled = false
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.photoButton.isUserInteractionEnabled = false
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 70, height: 70)
    }
    
    
    private func removeFrameDurationLogger() {
        if frameDurationLogger != nil {
            sdkManager.effectPlayer?.remove(frameDurationLogger as BNBFrameDurationListener)
            frameDurationLogger.printResult()
            frameDurationLogger = nil
            frameLoggerNote.text = "Enable frame logger"
        }
    }
    
    private func removeRulerListener() {
        if rulerListener != nil {
            sdkManager.effectPlayer?.remove(rulerListener as BNBFrameDataListener)
            rulerListener = nil
        }
    }

    func loadEffect(_ effect: String) {
        if (effect == "test_Nails") {
            showColorSelector()
        }
        if (effect == "test_Ruler" || effect == "FaceRuler") {
            if rulerListener == nil {
                rulerListener = RulerListener(sdkManager: sdkManager, setFaceData: effect == "FaceRuler")
                sdkManager.effectPlayer?.add(rulerListener as BNBFrameDataListener)
            }
            rulerListener.updateSetFaceDataFlag(setFaceData: effect == "FaceRuler")
        }
        else {
            removeRulerListener()
        }
        _ = sdkManager.loadEffect(effect)
    }
    
}

// MARK: Select Feature Color Extension
extension BanubaViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return Defaults.colors.count
    }

    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let view = UIImageView()
        view.backgroundColor = Defaults.colors[row]
        return view
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        currentFeatureColor = Defaults.colors[row]
        setFeatureColor(currentFeatureColor)
    }
    
    func setFeatureColor(_ color: UIColor) {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        color.getRed(&r, green: &g, blue: &b, alpha: &a);
        sdkManager.featureParameters =
            [BNBFeatureParameter(x: Float(r), y: Float(g), z: Float(b), w: Float(a))]
    }

    func showColorSelector() {
        let alert = UIAlertController(title: "Select color", message: "\n\n\n\n\n\n", preferredStyle: .alert)
        alert.isModalInPopover = true
        let pickerFrame = UIPickerView(frame: CGRect(x: 5, y: 20, width: 250, height: 140))
        alert.view.addSubview(pickerFrame)
        pickerFrame.dataSource = self
        pickerFrame.delegate = self
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {(UIAlertAction) in}))
        self.present(alert, animated: true, completion: nil)
    }
}

// MARK: Image Picker Extension
extension BanubaViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: false) {
            guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {return}
            let useInteractivePreview = true
            self.previewImageSrc = image
            self.sdkManager.processImageData(image) { procImage in
                DispatchQueue.main.async {
                    self.previewImage = procImage
                    if useInteractivePreview {
                        self.performSegue(withIdentifier: Defaults.previewInteractiveIdentifier, sender: self)
                    } else {
                        self.performSegue(withIdentifier: Defaults.previewIdentifier, sender: self)
                    }
                }
            }
        }
    }
}
//MARK: Document Picker Extenstion
extension BanubaViewController: UIDocumentPickerDelegate {
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let selectedFileURL = urls.first else {return}
        guard let dir = EffectsService.shared.fm.urls(for: .documentDirectory, in: .userDomainMask).first else {return}
        let sandboxFileURL = dir.appendingPathComponent(selectedFileURL.lastPathComponent)
        if EffectsService.shared.fm.fileExists(atPath: sandboxFileURL.path) {
            let alertController = UIAlertController(title: "Already exists", message: "File not copied", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            present(alertController, animated: true, completion: nil)
        }
        else {
            do {
                try EffectsService.shared.fm.copyItem(at: selectedFileURL, to: sandboxFileURL)
                let alertController = UIAlertController(title: "Archive is imported!", message: "", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                present(alertController, animated: true, completion: nil)
            }
            catch {
                print("Error: \(error)")
            }
        }
    }
}
//MARK: BNBFrameDurationListener Extenstion
extension BanubaViewController: BNBFrameDurationListener {
    func onRecognizerFrameDurationChanged(_ instant: Float, averaged: Float) {
        DispatchQueue.main.async {
            self.recognizerLabel.text = (NSString(format:"%.2f", 1/averaged)) as String
        }
    }
    
    func onCameraFrameDurationChanged(_ instant: Float, averaged: Float) {
        DispatchQueue.main.async {
            self.cameraLabel.text = (NSString(format:"%.2f", 1/averaged)) as String
        }
    }
    
    func onRenderFrameDurationChanged(_ instant: Float, averaged: Float) {
        DispatchQueue.main.async {
            self.renderLabel.text = (NSString(format:"%.2f", 1/averaged)) as String
        }
    }
}
//MARK: BNBEffectInfoListener Extenstion
extension BanubaViewController: BNBEffectInfoListener {
    func onEffectInfoUpdated(_ info: BNBEffectInfo) {
        print("Effect info after loading effect:")
        print("Effect name: " + info.url)
        print("Effect rendered: " + String(info.renderType.rawValue))
        print("Effect features: " + info.recognizerFeatures.description)
        print("Effect uses audio: " + String(info.usesAudio))
        print("Effect uses video: " + String(info.usesVideo))
        print("Effect uses touches: " + String(info.usesTouches))
    }
}
//MARK: UIApplication Extenstion
extension UIApplication {
    static var banubaVersion: String? {
        let frameworkBundle = Bundle(identifier: "banuba.sdk.effect-player")
        return frameworkBundle?.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
    }
}
//MARK: BNBEffectEventListener Extenstion
extension BanubaViewController: BNBEffectEventListener {
    func onEffectEvent(_ name: String, params: [String : String]) {
        if name == "setFeatureColor" {
            // This case is deprecated. Rely on the next only
            guard
                let r = Float(params["r"]!),
                let g = Float(params["g"]!),
                let b = Float(params["b"]!),
                let a = Float(params["a"]!)
            else { return }
            sdkManager.featureParameters = [BNBFeatureParameter(x: r, y: g, z: b, w: a)]
        }
        if name == "setFeatureParameters" {
            var parameters: [BNBFeatureParameter] = []
            let floats = params.first!.value.split(separator: ",")
            for i in 0 ..< floats.count / 4 {
                let x = Float(floats[4 * i + 0])
                let y = Float(floats[4 * i + 1])
                let z = Float(floats[4 * i + 2])
                let w = Float(floats[4 * i + 3])
                parameters.append(BNBFeatureParameter(x: x!, y: y!, z: z!, w: w!))
            }
            sdkManager.featureParameters = parameters
        }
    }
}


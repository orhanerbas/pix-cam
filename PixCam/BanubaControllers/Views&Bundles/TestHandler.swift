//
//  TestHandler.swift
//  PixCam
//
//  Created by Orhan Erbas on 8.06.2021.
//

import UIKit
import BanubaSdk
import BanubaEffectPlayer

class TestHandler
{
    class func setupTesting(sdkManager: BanubaSdkManager, view: BanubaViewController)
    {
        if CommandLine.arguments.count != 2 {
            return
        }
        
        print(CommandLine.arguments[1])
        
        class TestEventHandler: BNBEffectEventListener
        {
            weak var view: BanubaViewController?;
            func onEffectEvent(_ name:String, params: Dictionary<String, String>)
            {
                if name == "autotest_result" {
                    print("autotest_result " + params["result"]!)
                } else if name == "autotest_take_photo_please" {
                    print(name)
                    view!.takeFakePhoto()
                } else {
                    print(name)
                    print(params)
                }
            }
        }
        let h = TestEventHandler();
        h.view = view;
        sdkManager.effectManager()?.add(h);
        
        sdkManager.effectPlayer?.debugInterface()?.setAutotestConfig(CommandLine.arguments[1])
    }
}


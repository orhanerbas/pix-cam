//
//  UIAlertController+Utils.swift
//  PixCam
//
//  Created by Orhan Erbas on 8.06.2021.
//

import UIKit

extension UIAlertController {
    func addActions(_ actions: [UIAlertAction]) {
        actions.forEach { addAction($0) }
    }
}

//
//  FirstVC.swift
//  PhotoFilter
//
//  Created by Orhan Erbas on 18.05.2021.
//

import UIKit
import FirebaseStorage
import Photos
import BanubaSdk


class FirstVC: UIViewController {

    @IBOutlet weak var tryBtn: UIButton!
    @IBOutlet weak var SplashHeader_2: UILabel!
    @IBOutlet weak var SplashHeaderDescription_2: UILabel!
    
    //Color Variables
    var ColorOne = ""
    var ColorTwo = ""
    var ColorTree = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let filePath = AppDelegate.documentsPath + "/MaskEffects"
        var fileSize : UInt64

        do {
            //return [FileAttributeKey : Any]
            let attr = try FileManager.default.attributesOfItem(atPath: filePath)
            fileSize = attr[FileAttributeKey.size] as! UInt64

            //if you convert to NSDictionary, you can get file size old way as well.
            let dict = attr as NSDictionary
            fileSize = dict.fileSize()
            
        } catch {
            print("Error: \(error)")
        }
        
        SplashHeader_2.text = Definations.SplashHeader_2
        SplashHeaderDescription_2.text = Definations.SplashHeaderDescription_2
        
        tryBtn.layer.cornerRadius = 32
        tryBtn.setTitle(Definations.SplashButtonText_2, for: .normal)
        tryBtn.setTitleColor(hexStringToUIColor(hex: Definations.SplashButtonTextColor), for: .normal)
        
        if !Definations.SplashButtonColor.contains(",") {
            if Definations.SplashButtonColor.count > 0 {
                tryBtn.backgroundColor = hexStringToUIColor(hex: Definations.SplashButtonColor)
            }
        } else {
            DispatchQueue.main.async { [self] in
                let yourString = Definations.SplashButtonColor
                 
                let splitedCodes = yourString.split(separator: ",")

                if splitedCodes.count > 3 || splitedCodes.count == 3 {
                    ColorOne = String(splitedCodes[0])
                    ColorTwo = String(splitedCodes[1])
                    ColorTree =  String(splitedCodes[2])
                    
                    tryBtn.applyGradient(colours: [
                                                    hexStringToUIColor(hex: ColorOne),
                                                    hexStringToUIColor(hex: ColorTwo),
                                                    hexStringToUIColor(hex: ColorTree)
                    ])
                } else if splitedCodes.count > 2 || splitedCodes.count == 2 {
                    ColorOne = String(splitedCodes[0])
                    ColorTwo = String(splitedCodes[1])
                    
                    tryBtn.applyGradient(colours: [
                                                    hexStringToUIColor(hex: ColorOne),
                                                    hexStringToUIColor(hex: ColorTwo),
                    ])
                }
            }
        }
    }
    
    @IBAction func nextBtnIcn(_ sender: Any) {
        nextPage()
    }
    @IBAction func nextButtonAction(_ sender: Any) {
        nextPage()
    }
    
    func nextPage() {
        guard let window = UIApplication.shared.windows.filter({$0.isKeyWindow}).first else {
            return
        }
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "thirdVc")

        window.rootViewController = vc

        let options: UIView.AnimationOptions = .transitionCrossDissolve

        let duration: TimeInterval = 0.3

        UIView.transition(with: window, duration: duration, options: options, animations: {}, completion:
        { completed in
            // maybe do something on completion here
        })
    }
    
    func fileSize(fromPath path: String) -> String? {
        guard let size = try? FileManager.default.attributesOfItem(atPath: path)[FileAttributeKey.size],
            let fileSize = size as? UInt64 else {
            return nil
        }

        // bytes
        if fileSize < 1023 {
            return String(format: "%lu bytes", CUnsignedLong(fileSize))
        }
        // KB
        var floatSize = Float(fileSize / 1024)
        if floatSize < 1023 {
            return String(format: "%.1f KB", floatSize)
        }
        // MB
        floatSize = floatSize / 1024
        if floatSize < 1023 {
            return String(format: "%.1f MB", floatSize)
        }
        // GB
        floatSize = floatSize / 1024
        return String(format: "%.1f GB", floatSize)
    }
}

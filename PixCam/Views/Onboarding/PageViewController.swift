//
//  PageViewController.swift
//  PhotoFilter
//
//  Created by Orhan Erbas on 18.05.2021.
//

import UIKit


class PageViewController: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    lazy var orderedViewControllers: [UIViewController] = {
        return [self.newVc(viewController: "firstVc"),
                self.newVc(viewController: "secondVc"),
                self.newVc(viewController: "thirdVc")]
    }()
    
    public static var pageControl = UIPageControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.dataSource = self
        
        if let firstViewController = orderedViewControllers.first {
            setViewControllers([firstViewController],
                               direction: .forward,
                               animated: true, completion: nil)
        }
        
        self.delegate = self
        configurePageControll()
        
    }
    
    

    
    func configurePageControll() {
        switch UIDevice().type {
        case .iPhoneSE, .iPhoneSE2, .iPhone8, .iPhone8Plus, .iPhone7Plus, .iPhone7:
            PageViewController.pageControl = UIPageControl(frame: CGRect(x: 0, y: UIScreen.main.bounds.maxY - 90, width: UIScreen.main.bounds.width, height: 50))
        default:
            PageViewController.pageControl = UIPageControl(frame: CGRect(x: 0, y: UIScreen.main.bounds.maxY - 130, width: UIScreen.main.bounds.width, height: 50))
        }
        
        PageViewController.pageControl.numberOfPages = orderedViewControllers.count
        PageViewController.pageControl.currentPage = 0
        PageViewController.pageControl.tintColor = UIColor.black
        PageViewController.pageControl.pageIndicatorTintColor = UIColor(named: "indicatorColors")
        PageViewController.pageControl.currentPageIndicatorTintColor = UIColor.black
        self.view.addSubview(PageViewController.pageControl)
    }
    
    func newVc(viewController: String) -> UIViewController {
        return UIStoryboard(name: "Main" , bundle: nil).instantiateViewController(withIdentifier: viewController)
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.firstIndex(of: viewController) else { return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return orderedViewControllers.last
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.firstIndex(of: viewController) else { return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        
        guard orderedViewControllers.count != nextIndex else {
            return orderedViewControllers.first
        }
        
        guard orderedViewControllers.count > nextIndex else {
            return nil
        }
        return orderedViewControllers[nextIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let pageContentViewController = pageViewController.viewControllers![0]
        
        if PageViewController.pageControl.currentPage == 2 {
            UIView.animate(withDuration: 0.02) {
                         let mainStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
                         let viewController = mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! UITabBarController
                         UIApplication.shared.windows.first?.rootViewController = viewController
                         UIApplication.shared.windows.first?.makeKeyAndVisible()
                     }

            
        } else {
            PageViewController.pageControl.currentPage = orderedViewControllers.firstIndex(of: pageContentViewController)!
        }
    }
}

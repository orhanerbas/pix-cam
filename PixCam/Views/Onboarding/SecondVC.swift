//
//  SecondVC.swift
//  PhotoFilter
//
//  Created by Orhan Erbas on 18.05.2021.
//

import UIKit

class SecondVC: UIViewController {

    @IBOutlet weak var tryBtn: UIButton!
    @IBOutlet weak var SplashHeader_3: UILabel!
    @IBOutlet weak var SplashHeaderDescription_3: UILabel!
    
    //Color Variables
    var ColorOne = ""
    var ColorTwo = ""
    var ColorTree = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        SplashHeader_3.text = Definations.SplashHeader_3
        SplashHeaderDescription_3.text = Definations.SplashHeaderDescription_3
        tryBtn.layer.cornerRadius = 32
        tryBtn.contentEdgeInsets = UIEdgeInsets(top: 20,left: 21,bottom: 20,right: 21)
        tryBtn.setTitle(Definations.SplashButtonText_3, for: .normal)
        tryBtn.setTitleColor(hexStringToUIColor(hex: Definations.SplashButtonTextColor), for: .normal)
        
        if !Definations.SplashButtonColor.contains(",") {
            if Definations.SplashButtonColor.count > 0 {
                tryBtn.backgroundColor = hexStringToUIColor(hex: Definations.SplashButtonColor)
            }
        } else {
            DispatchQueue.main.async { [self] in
                let yourString = Definations.SplashButtonColor
                 
                let splitedCodes = yourString.split(separator: ",")

                if splitedCodes.count > 3 || splitedCodes.count == 3 {
                    ColorOne = String(splitedCodes[0])
                    ColorTwo = String(splitedCodes[1])
                    ColorTree =  String(splitedCodes[2])
                    
                    tryBtn.applyGradient(colours: [
                                                    hexStringToUIColor(hex: ColorOne),
                                                    hexStringToUIColor(hex: ColorTwo),
                                                    hexStringToUIColor(hex: ColorTree)
                    ])
                } else if splitedCodes.count > 2 || splitedCodes.count == 2 {
                    ColorOne = String(splitedCodes[0])
                    ColorTwo = String(splitedCodes[1])
                    
                    tryBtn.applyGradient(colours: [
                                                    hexStringToUIColor(hex: ColorOne),
                                                    hexStringToUIColor(hex: ColorTwo),
                    ])
                }
            }
        }
    }
    
    @IBAction func nextBtnIcn(_ sender: Any) {
        performSegue(withIdentifier: "showLastPage", sender: nil)
    }
    @IBAction func nextButtonAction(_ sender: Any) {
       performSegue(withIdentifier: "showLastPage", sender: nil)
    }
    
}

//
//  LoadingVC.swift
//  PixCam
//
//  Created by Orhan Erbas on 28.06.2021.
//

import UIKit
import BanubaSdk
import NVActivityIndicatorView
import AVKit
import AVFoundation

class LoadingVC: UIViewController {

    var player : AVPlayer!
    var avPlayerLayer : AVPlayerLayer!
    var isVideoEnd = false
    var counter = 0
    var timer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let alert = UIAlertController(title: "Check your connection", message: "Please check your connection and try again.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Try Again", style: .default, handler: { action in
            switch action.style {
                case .default:
                    self.restartApplication()
                
                default:
                    break
            }
        }))
        
        if Reachability.isConnectedToNetwork(){
            print("Internet Connection Available!")
        } else {
            DispatchQueue.main.async {
                self.present(alert, animated: true, completion: nil)
            }
        }
        Definations.isMaskEffectsActive = true
        Definations.isMakeupEffectsActive = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.update(notification:)), name: NSNotification.Name.init("paywallOnEnd"), object: nil)
        playVideo()
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.checkCounterAndRefresh), userInfo: nil, repeats: true)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.timer.invalidate()
    }
    
    func restartApplication() {
        DispatchQueue.main.async {
            self.getNewScreen(boardName: "Main", vcName: "LoadingVC")
        }
    }
    
    //MARK: Make Video
    private func playVideo() {
        let videoURL = Bundle.main.url(forResource: "PIX_Splash_1", withExtension: "mp4") // Get video url

        player = AVPlayer(url: videoURL!)
        avPlayerLayer = AVPlayerLayer(player: player)
        avPlayerLayer.videoGravity = AVLayerVideoGravity.resize
        avPlayerLayer.frame = view.layer.bounds
        view.layer.addSublayer(avPlayerLayer)
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil, queue: nil) { notification in
            self.counter += 1
            self.player.seek(to: CMTime.zero)
            self.player.play()
            if !self.isVideoEnd {
                PaywallConfig.instance.fetchPaywallUIValue()
                self.isVideoEnd = true
            } 
        }
        player.play()
     }
    
    @objc func update(notification: Notification) {
        if Definations.paywallConnectIsEnd {
            if defaults.bool(forKey: "onboarded") {
                DispatchQueue.main.async {
                    self.getNewScreen(boardName: "Main", vcName: "HomeViewController")
                }
            } else {
                DispatchQueue.main.async {
                    self.getNewScreen(boardName: "Main", vcName: "firstVc")
                }
            }
        }
    }
    
    @objc func checkCounterAndRefresh() {
        if counter == 6 {
            counter = 0
            timer.invalidate()
            let alert = UIAlertController(title: "Check your connection", message: "Please check your connection and try again.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Try Again", style: .default, handler: { action in
                switch action.style {
                    case .default:
                        self.restartApplication()
                    
                    default:
                        break
                }
            }))
            
            if Reachability.isConnectedToNetwork(){
                self.restartApplication()
            } else {
                DispatchQueue.main.async {
                    self.present(alert, animated: true, completion: nil)
                    self.timer.fire()
                }
            }
        }
    }
}

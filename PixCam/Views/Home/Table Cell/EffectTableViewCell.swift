//
//  EffectTableViewCell.swift
//  PixCam
//
//  Created by Orhan Erbas on 20.05.2021.
//

import UIKit
import Firebase

class EffectTableViewCell: UITableViewCell {
    static let identifier = "effectsCell"

    @IBOutlet weak var effectImage: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var TryBtn: UIButton!
    
    var post: ToonifyLocal! {
        didSet {
            updateUI()
        }
    }
    
    func updateUI(){
        DispatchQueue.main.async { [self] in
            self.effectImage.image = self.post.img
            effectImage.layer.cornerRadius = 16
            mainView.layer.cornerRadius = 16
            self.title.text = self.post.title
            TryBtn.isUserInteractionEnabled = false
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutMargins = UIEdgeInsets.zero //or UIEdgeInsetsMake(top, left, bottom, right)
        self.separatorInset = UIEdgeInsets.zero
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

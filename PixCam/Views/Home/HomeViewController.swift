//
//  HomeViewController.swift
//  PhotoFilter
//
//  Created by Orhan Erbas on 19.05.2021.
//

import UIKit
import Firebase
import SwiftyJSON
import BottomPopup
import Purchases

class HomeViewController: UIViewController, BottomPopupDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var scrollView: IKScrollView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var premiumBar: UIView!
    @IBOutlet weak var premiumBarConsTop: NSLayoutConstraint!
    @IBOutlet weak var premiumBarConsBottom: NSLayoutConstraint!
    @IBOutlet weak var premiumViewHeight: NSLayoutConstraint!
    @IBOutlet weak var headerConstant: NSLayoutConstraint!
    
    private var previousOffset: CGFloat = 0
    private var currentPageSecond: Int = 0
    var currentPage = 0
    
    @IBOutlet weak var homePremiumText: UILabel!
    let banubaSlider = [
        BanubaLocal(id: 1, title: "MASKS".localized(), img: UIImage(named: "banuba1")),
        BanubaLocal(id: 2, title: "BEAUTIFY".localized(), img: UIImage(named: "banuba2")),
    ]
    
    let toonifyList = [
        ToonifyLocal(id: 1, title: "2D CARTOON".localized(), img: UIImage(named: "3dCartoon")),
        ToonifyLocal(id: 2, title: "3D CARTOON".localized(), img: UIImage(named: "2dCartoon")),
        ToonifyLocal(id: 3, title: "BARBIE WORLD".localized(), img: UIImage(named: "Barbie")),
        ToonifyLocal(id: 6, title: "COMIC AI".localized(), img: UIImage(named: "Comics")),
        ToonifyLocal(id: 4, title: "ZOMBIE LAND".localized(), img: UIImage(named: "Zombie")),
        ToonifyLocal(id: 5, title: "CARICATURE".localized(), img: UIImage(named: "Caricature")),
        ToonifyLocal(id: 7, title: "Ethnicity Estimate".localized(), img: UIImage(named: "Ethnicity")),
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(isPremium), name: NSNotification.Name("User is Premium"), object: nil)
        defaults.set(true, forKey: "onboarded")
        tableView.delegate = self
        tableView.dataSource = self
        collectionView.delegate = self
        collectionView.dataSource = self

        pageControl.hidesForSinglePage = true
        prepareUI()
        tableView.reloadData()
        collectionView.reloadData()
        showPaywall()
        switch UIDevice().type {
            case .iPhoneSE, .iPhoneSE2, .iPhone8, .iPhone8Plus, .iPhone7Plus, .iPhone7:
                headerConstant.constant = 40
            default:
                headerConstant.constant = 60
        }
        
        if self.tabBarController?.selectedIndex == 1 {
            performSegue(withIdentifier: "", sender: nil)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.tabbarObserver), name: NSNotification.Name.init("selectCurrentScreen"), object: nil)
//        UserDefaults.standard.set(true, forKey: "newEffectData")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.observeDownload), name: NSNotification.Name.init("contueBanubaVC"), object: nil)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if Definations.isPremiumUser {
            hiddenPremium()
        }
        
        Purchases.shared.purchaserInfo { (purchaserInfo, error) in
            if purchaserInfo?.entitlements.all["PixCam_Premium"]?.isActive == true {
                // User is "premium"
                self.isPremium()
            } else {
                print("not premium")
            }
        }
    }
    
    @objc func isPremium() {
        Definations.isPremiumUser = UserDefaults.standard.bool(forKey: "isPremium")
    }
    
    func hiddenPremium() {
        self.premiumBar.isHidden = true
        self.premiumBarConsTop.constant = 0
        self.premiumViewHeight.constant = 0
    }
    
    func showPopUpPage(vc: String){
        guard let popupVC = UIStoryboard(name: "Worker", bundle: nil).instantiateViewController(withIdentifier: vc) as? MainPopUp else { return }
        popupVC.height = popupVC.view.frame.height * 1.3 / 3
        popupVC.topCornerRadius = 35
        popupVC.presentDuration = 0.5
        popupVC.dismissDuration = 0.5
        popupVC.popupDelegate = self
        present(popupVC, animated: true, completion: nil)
    }
    
    func showPaywall(){
        if !Definations.isPremiumUser {
            if Definations.subscriptionShowPaywallFirst {
                if !Definations.PaywallFirst {
                    Definations.PaywallFirst = true
                    if Definations.subscriptionShowSingleButton == 1 {
                        OperationQueue.main.addOperation {
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SubscriptionFirstViewController") as! SubscriptionFirstViewController
                            vc.modalPresentationStyle = .fullScreen
                            vc.type = 12
                            self.present(vc, animated: true, completion: nil)
                        }
                    } else if Definations.subscriptionShowSingleButton == 2 {
                        OperationQueue.main.addOperation {
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SubscriptionViewController") as! SubscriptionViewController
                            vc.modalPresentationStyle = .fullScreen
                            vc.type = 12
                            self.present(vc, animated: true, completion: nil)
                        }
                    } else if Definations.subscriptionShowSingleButton == 3 {
                        OperationQueue.main.addOperation {
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SubscriptionCViewController") as! SubscriptionCViewController
                            vc.modalPresentationStyle = .fullScreen
                            vc.type = 12
                            self.present(vc, animated: true, completion: nil)
                        }
                    }
                }
            }
        }
    }
    
    @objc func observeDownload(notification: Notification) {
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            self.getNewScreen(boardName: "Main", vcName: "BanubaViewController")
        }
    }
    
    @objc func tabbarObserver(notification: Notification) {
        self.tabBarController?.selectedIndex = 0
    }
    
    @IBAction func didTapBanuba(_ sender: Any) {
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollView.sizeMatching = .Dynamic(width: { self.view.frame.width },
                                           height: { self.calculateScrollHeight() }
        )
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.async {
            if FileDownloader.showFiles(effectDirectory: "/MaskEffects") != [] {
                 Definations.unzipedFiles = FileDownloader.showFiles(effectDirectory: "/MaskEffects")
            }
             
        }
    }
    
    func calculateScrollHeight() -> CGFloat {
      var height:CGFloat = 50
        
      height = height + tableViewHeight
        
      return height
    }
    
    var tableViewHeight: CGFloat {
        tableView.layoutIfNeeded()
        return tableView.contentSize.height + 430
    }
    
    func prepareUI(){
        homePremiumText.text = "PixCam Premium".localized()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.premiumBar.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openSubscription)))
    }
    
    @objc fileprivate func openSubscription(){
        if Definations.subscriptionShowSingleButton == 1 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SubscriptionFirstViewController") as! SubscriptionFirstViewController
            vc.modalPresentationStyle = .fullScreen
            vc.type = 12
            self.present(vc, animated: true, completion: nil)
        } else if Definations.subscriptionShowSingleButton == 2 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SubscriptionViewController") as! SubscriptionViewController
            vc.modalPresentationStyle = .fullScreen
            vc.type = 12
            self.present(vc, animated: true, completion: nil)
        } else if Definations.subscriptionShowSingleButton == 3 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SubscriptionCViewController") as! SubscriptionCViewController
            vc.modalPresentationStyle = .fullScreen
            vc.type = 12
            self.present(vc, animated: true, completion: nil)
        }
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return toonifyList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "effectsCell", for: indexPath) as! EffectTableViewCell
        cell.post = toonifyList[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         let vc = self.storyboard?.instantiateViewController(withIdentifier: "CameraViewController") as! CameraViewController
        Definations.lastEffectName = toonifyList[indexPath.row].title
         if toonifyList[indexPath.row].id != 7 {
            Definations.segueFromFacePercente = false
             vc.effectId = toonifyList[indexPath.row].id
             vc.effectTitle = toonifyList[indexPath.row].title
             vc.modalPresentationStyle = .fullScreen
             self.present(vc, animated: false, completion: nil)
         } else {
             Definations.segueFromFacePercente = true
             vc.effectId = toonifyList[indexPath.row].id
             vc.effectTitle = toonifyList[indexPath.row].title
             vc.modalPresentationStyle = .fullScreen
             self.present(vc, animated: false, completion: nil)
         }
    }
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate, UICollectionViewDelegateFlowLayout {
    
    // MARK: - ScrollView Delegate Method
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageWidth = scrollView.frame.width
        self.currentPage = Int((scrollView.contentOffset.x + pageWidth / 2) / pageWidth)
        self.pageControl.currentPage = self.currentPage
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.pageControl.numberOfPages = banubaSlider.count
        return banubaSlider.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BanubaCollectionViewCell.identifier, for: indexPath) as! BanubaCollectionViewCell
        cell.post = banubaSlider[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        switch UIDevice().type {
        case .iPhone8:
           return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        case .iPhone6, .iPhone6S:
            return UIEdgeInsets(top: 5, left: 5, bottom: 20, right: 5)
        default:
            return UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
        }
    }
    
    /*
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         switch UIDevice().type {
         case .iPhone12:
             return CGSize(width: 20, height: self.view.frame.height)
         case .iPhone12Pro:
             return CGSize(width: 20, height: self.view.frame.height)
         default:
             return CGSize(width: 20, height: self.view.frame.height)
         }
     }
     */

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        Definations.selectedEffectCode = indexPath.row + 1
        Definations.segueFromFacePercente = false
        if indexPath.row == 1 {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                if !defaults.bool(forKey: "isBanubaLoaded") {
                    self.showPopUpPage(vc: "uploadEffectVC")
                } else {
                    self.getNewScreen(boardName: "Main", vcName: "NewCameraViewController")
                }
            }
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                if !defaults.bool(forKey: "isBanubaLoaded") {
                    self.showPopUpPage(vc: "uploadEffectVC")
                } else {
                    self.getNewScreen(boardName: "Main", vcName: "BanubaViewController")
                }
            }
        }
    }
} 

//
//  BanubaCollectionViewCell.swift
//  PixCam
//
//  Created by Orhan Erbas on 17.06.2021.
//

import UIKit

class BanubaCollectionViewCell: UICollectionViewCell {
    
    static let identifier = "BanubaCollectionViewCell"
    
    @IBOutlet weak var effectImage: UIImageView!
    @IBOutlet weak var effectTitle: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var tryNowBtn: UIButton!
    
    var post: BanubaLocal! {
        didSet {
            updateUI()
        }
    }
    
    func updateUI(){
        DispatchQueue.main.async { [self] in
            tryNowBtn.titleLabel?.numberOfLines = 0
            tryNowBtn.titleLabel?.adjustsFontSizeToFitWidth = true
            tryNowBtn.titleLabel?.lineBreakMode = .byWordWrapping
            tryNowBtn.titleLabel?.textAlignment = .center
            //            if let imageDownloadURL = self.post.imageURL {
            //                let url = URL(string: imageDownloadURL)
            //                self.effectImage.kf.setImage(with: url)
            //                //self.effectImage?.sd_setImage(with: URL(string: imageDownloadURL), placeholderImage: UIImage(named: "1.png"))
            //            }
            if let countryCode = Locale.current.languageCode {
//                if countryCode == "id" || countryCode == "ja" || countryCode == "es" {
//                    tryNowBtn.titleLabel?.font = UIFont(name: "Poppins-SemiBold", size: 11)
//                } else if countryCode == "pt" {
//                    tryNowBtn.titleLabel?.font = UIFont(name: "Poppins-SemiBold", size: 14)
//                } else if countryCode == "fr" {
//                    tryNowBtn.titleLabel?.font = UIFont(name: "Poppins-SemiBold", size: 14)
//                }
            }
            tryNowBtn.setTitle("Try Now".localized(), for: .normal)
            self.effectImage.image = self.post.img
            effectImage.layer.cornerRadius = 16
            mainView.layer.cornerRadius = 16
            effectTitle.text = post.title
            tryNowBtn.isUserInteractionEnabled = false
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutMargins = UIEdgeInsets.zero //or UIEdgeInsetsMake(top, left, bottom, right)
    }
}

//
//  BanunaNavigationController.swift
//  PixCam
//
//  Created by Orhan Erbas on 29.06.2021.
//

import UIKit
import BottomPopup

class BanunaNavigationController: UINavigationController, BottomPopupDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        //performSegue(withIdentifier: "showBanubaController", sender: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.observeDownload), name: NSNotification.Name.init("contueBanubaVC"), object: nil)
        if !UserDefaults.standard.bool(forKey: "onboarded") {
            self.showPopUpPage(vc: "uploadEffectVC")
        } else {
            guard let window = UIApplication.shared.windows.filter({$0.isKeyWindow}).first else {
                return
            }
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "BanubaViewController")

            window.rootViewController = vc

            let options: UIView.AnimationOptions = .transitionCrossDissolve

            let duration: TimeInterval = 0.3

            UIView.transition(with: window, duration: duration, options: options, animations: {}, completion:
            { completed in
                // maybe do something on completion here
            })
        } 
    }
    
    func showPopUpPage(vc: String){
        guard let popupVC = UIStoryboard(name: "Worker", bundle: nil).instantiateViewController(withIdentifier: vc) as? MainPopUp else { return }
        popupVC.height = popupVC.view.frame.height * 1.3 / 3
        popupVC.topCornerRadius = 35
        popupVC.presentDuration = 0.5
        popupVC.dismissDuration = 0.5
        popupVC.popupDelegate = self
        present(popupVC, animated: true, completion: nil)
    }
    
    @objc func observeDownload(notification: Notification) {
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            guard let window = UIApplication.shared.windows.filter({$0.isKeyWindow}).first else {
                return
            }
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "BanubaViewController")

            window.rootViewController = vc

            let options: UIView.AnimationOptions = .transitionCrossDissolve

            let duration: TimeInterval = 0.3

            UIView.transition(with: window, duration: duration, options: options, animations: {}, completion:
            { completed in
                // maybe do something on completion here
            })
        }
    }
}

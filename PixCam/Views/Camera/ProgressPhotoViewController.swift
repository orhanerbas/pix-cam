//
//  ProgressPhotoViewController.swift
//  PixCam
//
//  Created by Orhan Erbas on 23.05.2021.
//

import UIKit
import SceneKit
import ARKit

class ProgressPhotoViewController: UIViewController, ARSCNViewDelegate {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var pageTitle: UILabel!
    @IBOutlet weak var itemsView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var analysingText: UILabel!
    @IBOutlet weak var backView: UIView!
    
    var takenPhoto: UIImage!
    var timer = Timer()
    var poseDuration = 20
    var indexProgressBar = 0
    var currentPoseIndex = 0
    var finishedProgress = false
    var effectId: Int?
    var isRequestEnded = false
    var effectTitle: String?
    var sourceType = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupProgressBar()
        drawOverlay()

        if sourceType == 1 {
            imageView.contentMode = .scaleAspectFill
        } else {
            imageView.contentMode = .scaleAspectFit
            backButton.setImage(UIImage(named: "back-button"), for: .normal)
        }
        
        analysingText.text = "Analyzing your face".localized()
        if let image = takenPhoto {
            imageView.image = image
        }
        pageTitle.text = effectTitle
        DispatchQueue.main.async {
            self.useToonify { endWithSuccess in
                if endWithSuccess {
                    self.setProgressBar()
                } else {
                    let alert = UIAlertController(title: "We couldn't find your face", message: "Please show your face or check your internet connection.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        switch action.style{
                            case .default:
                                self.dismiss(animated: true, completion: nil)
                            case .cancel:
                            print("cancel")
                            
                            case .destructive:
                            print("destructive")
                            
                        default:
                            break
                        }
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
        let backGesture = UITapGestureRecognizer(target: self, action: #selector(self.backGestureObserver))
        
        self.backView.addGestureRecognizer(backGesture)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        Definations.isPhotoSelected = false
    }

    func drawOverlay() {
        if !Definations.isPhotoSelected {
            view.backgroundColor = .black
            
            let width: CGFloat = view.frame.size.width
            let height: CGFloat = view.frame.size.height

            let path = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height), cornerRadius: 0)
            let rect = CGRect(x: width / 2 - 141.5, y: height / 2.5 - 100, width:  283, height: 356)
            let circlePath = UIBezierPath(ovalIn: rect)
            path.append(circlePath)
            path.usesEvenOddFillRule = true

            let fillLayer = CAShapeLayer()
            fillLayer.path = path.cgPath
            fillLayer.fillRule = .evenOdd
            fillLayer.fillColor = UIColor.black.cgColor
            fillLayer.opacity = 0.5
            view.layer.addSublayer(fillLayer)
            self.view.addSubview(self.backButton)
            self.view.addSubview(self.pageTitle)
            self.view.addSubview(self.itemsView)
            self.view.addSubview(self.backView)
        }
    }

    @IBAction func backAction(_ sender: Any) {
        if Definations.segueFromFacePercente {
            DispatchQueue.main.async {
                self.getNewScreen(boardName: "Main", vcName: Definations.lastVCIdentifier)
            }
        } else if Definations.userFromBanuba {
            DispatchQueue.main.async {
                self.getNewScreen(boardName: "Main", vcName: Definations.lastVCIdentifier)
                Definations.userFromBanuba = false
            }
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func setupProgressBar() {
        DispatchQueue.main.async { [self] in
            progressBar.layer.cornerRadius = 8
            progressBar.clipsToBounds = true
            
            progressBar.layer.sublayers![1].cornerRadius = 8
            progressBar.subviews[1].clipsToBounds = true
            
            progressBar.progress = 0.0
            
            progressBar.transform = CGAffineTransform(scaleX: 1, y: 8)
            
            timer = Timer.scheduledTimer(timeInterval: 0.50, target: self, selector: #selector(self.setProgressBar), userInfo: nil, repeats: true)
        }
    }
   
    
    //MARK: Back Gesture
    @objc func backGestureObserver() {
        if Definations.segueFromFacePercente {
            DispatchQueue.main.async {
                self.getNewScreen(boardName: "Main", vcName: Definations.lastVCIdentifier)
            }
        } else if Definations.userFromBanuba {
            DispatchQueue.main.async {
                self.getNewScreen(boardName: "Main", vcName: Definations.lastVCIdentifier)
                Definations.userFromBanuba = false
            }
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
}

//
//  PhotoCompletedViewController.swift
//  PixCam
//
//  Created by Orhan Erbas on 24.05.2021.
//

import UIKit

class PhotoCompletedViewController: UIViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var filteredPhoto: UIImageView!
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var mainViewBottomCons: NSLayoutConstraint!
    @IBOutlet weak var pageTitle: UILabel!
    @IBOutlet weak var effectComplateText: UILabel!
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var backView: UIView!
    
    
    var filteredImg: UIImage!
    var effectTitle: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        loadUI()
        pageTitle.text = effectTitle
        resultLabel.text = "Show the result".localized()
        effectComplateText.text =  "\(String(describing: effectTitle))"+"\n"+"Completed".localized()
        NotificationCenter.default.addObserver(self, selector: #selector(isPremium), name: NSNotification.Name("User is Premium"), object: nil)
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.goSubscription))
        self.buttonView.addGestureRecognizer(gesture)
        
        let secondGesture = UITapGestureRecognizer(target: self, action: #selector(self.backGestureOberver))
        self.backView.addGestureRecognizer(secondGesture)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        Definations.userFromBanuba = false
    }
    
    @objc func isPremium() {
        Definations.isPremiumUser = true
    }
    
    func loadUI() {
        filteredPhoto.image = self.filteredImg
        self.containerView.layer.borderWidth = 2
        self.containerView.layer.cornerRadius = 16
        self.containerView.layer.borderColor = UIColor.white.cgColor
        self.blurView.layer.cornerRadius = 16
        self.blurView.clipsToBounds = true
        self.filteredPhoto.layer.cornerRadius = 16
        self.buttonView.layer.cornerRadius = 30
        
        switch UIDevice().type {
            case .iPhone8, .iPhoneSE, .iPhoneSE2 :
                mainViewBottomCons.constant = 10
                //clockBottom.constant = -11
                self.buttonView.widthAnchor.constraint(equalTo: self.blurView.widthAnchor, constant: -70).isActive = true
            case .iPhone8Plus :
                mainViewBottomCons.constant = 20
                //clockBottom.constant = -11
            case .iPhone11ProMax :
                mainViewBottomCons.constant = 95
                //clockBottom.constant = 55
            default:
                mainViewBottomCons.constant = 65
                //clockBottom.constant = 40
        }
        
       // self.completedBackground.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        //self.completedBackground.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        //self.completedBackground.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        //self.completedBackground.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
    }
    
    @IBAction func backAction(_ sender: Any) {
        if Definations.userFromBanuba {
            DispatchQueue.main.async {
                self.getNewScreen(boardName: "Main", vcName: Definations.lastVCIdentifier)
                Definations.userFromBanuba = false
            }
        } else if Definations.segueFromFacePercente {
            DispatchQueue.main.async {
                self.getNewScreen(boardName: "Main", vcName: Definations.lastVCIdentifier)
            }
        } else {
            DispatchQueue.main.async {
                self.getNewScreen(boardName: "Main", vcName: Definations.lastVCIdentifier)
            }
        }
    }

    @objc func goSubscription(sender : UITapGestureRecognizer) {
        if Definations.isPremiumUser {
            DispatchQueue.main.async {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ShareViewController") as! ShareViewController
                vc.modalPresentationStyle = .fullScreen
                vc.takenPhoto = self.filteredImg
                vc.type = 9
                vc.effectTitle = self.effectTitle
                self.present(vc, animated: true, completion: nil)
            }
        } else {
            if Definations.subscriptionRewardedInterstitial {
                showPaywall(type: 1)
            } else {
                showPaywall(type: 2)
            }
        }
    }
    
    func showPaywall(type: Int){
        if Definations.subscriptionShowSingleButton == 1 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SubscriptionFirstViewController") as! SubscriptionFirstViewController
            vc.type = type
            vc.takenPhoto = self.filteredImg
            vc.effectTitle = self.effectTitle
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        } else if Definations.subscriptionShowSingleButton == 2 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SubscriptionViewController") as! SubscriptionViewController
            vc.type = type
            vc.takenPhoto = self.filteredImg
            vc.effectTitle = self.effectTitle
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        } else if Definations.subscriptionShowSingleButton == 3 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SubscriptionCViewController") as! SubscriptionCViewController
            vc.type = type
            vc.takenPhoto = self.filteredImg
            vc.effectTitle = self.effectTitle
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @objc func backGestureOberver() {
        if Definations.userFromBanuba {
            DispatchQueue.main.async {
                self.getNewScreen(boardName: "Main", vcName: Definations.lastVCIdentifier)
                Definations.userFromBanuba = false
            }
        } else if Definations.segueFromFacePercente {
            DispatchQueue.main.async {
                self.getNewScreen(boardName: "Main", vcName: Definations.lastVCIdentifier)
            }
        } else {
            DispatchQueue.main.async {
                self.getNewScreen(boardName: "Main", vcName: Definations.lastVCIdentifier)
            }
        }
    }
}


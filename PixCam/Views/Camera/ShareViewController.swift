//
//  ShareViewController.swift
//  PixCam
//
//  Created by Orhan Erbas on 26.05.2021.
//

import UIKit
import Foundation
import OneSignal

class ShareViewController: UIViewController {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var saveImage: UIView!
    @IBOutlet weak var pageTitle: UILabel!
    @IBOutlet weak var imageTopCons: NSLayoutConstraint!
    @IBOutlet var savedPhoto: UIView!
    @IBOutlet var blackView: UIView!
    @IBOutlet weak var savePhoto: UILabel!
    @IBOutlet weak var savedText: UILabel!
    
    //MARK: CountriesHiddenView
    @IBOutlet weak var mainHiddenView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var containerViewCons: NSLayoutConstraint!
    @IBOutlet weak var hiddenViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageViewConstraint: NSLayoutConstraint!
    
    //Sliders
    @IBOutlet weak var sliderOne: CustomLipsSlider!
    @IBOutlet weak var sliderTwo: CustomLipsSlider!
    @IBOutlet weak var sliderThree: CustomLipsSlider!
    @IBOutlet weak var sliderFour: CustomLipsSlider!
    //CountryNames
    @IBOutlet weak var countryNameOne: UILabel!
    @IBOutlet weak var countryNameTwo: UILabel!
    @IBOutlet weak var countryNameThree: UILabel!
    @IBOutlet weak var countryNameFour: UILabel!
    //SliderPercenteLabels
    @IBOutlet weak var percenteLabelOne: UILabel!
    @IBOutlet weak var percenteLabelTwo: UILabel!
    @IBOutlet weak var percenteLabelThree: UILabel!
    @IBOutlet weak var percenteLabelFour: UILabel!
    //SliderFlags
    @IBOutlet weak var flagLabelOne: UILabel!
    @IBOutlet weak var flagLabelTwo: UILabel!
    @IBOutlet weak var flagLabelThree: UILabel!
    @IBOutlet weak var flagLabelFour: UILabel!
    
    @IBOutlet weak var backView: UIView!
    
    
    var takenPhoto: UIImage!
    var controller: UIDocumentInteractionController = UIDocumentInteractionController()
    
    var effectTitle: String = ""
    var type = 0
    var firstCountryCode = ""
    
    var completionPercentes = [Int]()
    
    var percenteNames = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let countryLocale = NSLocale.current
        let countryCode = countryLocale.regionCode
        let country = (countryLocale as NSLocale).displayName(forKey: NSLocale.Key.countryCode, value: countryCode!)
        var checkDublicateCNames = [String]()
        
        completionPercentes = getRandomValues(amountOfValues: 4, totalAmount: 100)!
        
        firstCountryCode = countryCode!
        
        self.percenteNames.append("\(country!)-\(countryCode!)")
        checkDublicateCNames.append(country!)
        for _ in Definations.countries {
            let country = Definations.countries.randomElement()
            let splitetCname = country?.split(separator: "-")[0]
            if checkDublicateCNames.contains("\(splitetCname ?? "")") {
                print("contains")
            } else {
                if checkDublicateCNames.count == 4 {
                    print("end")
                } else {
                    checkDublicateCNames.append("\(splitetCname ?? "")")
                    self.percenteNames.append(country!)
                }
            }
        }
        
        pageTitle.text = self.effectTitle
        savePhoto.text = "Save to gallery".localized()
        savedText.text = "Your photo saved".localized()
        saveImage.layer.cornerRadius = 8
        imageView.layer.cornerRadius = 8
        
        switch UIDevice().type {
        case .iPhoneSE, .iPhoneSE2, .iPhone8, .iPhone8Plus, .iPhone7Plus, .iPhone7:
            imageTopCons.constant = 15
        default:
            imageTopCons.constant = 35
        }
        
        if let image = takenPhoto {
            imageView.image = image
            imageView.frame.size = CGSize(width: image.size.width, height: image.size.height)
        }
        
        self.saveImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(saveImageAction)))
        self.savedPhoto.isHidden = true
        self.savedPhoto.alpha = 0
        self.savedPhoto.isHidden = true
        self.savedPhoto.alpha = 0
        self.blackView.isHidden = true
        self.blackView.alpha = 0
        self.blackView.bounds = self.view.bounds
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.removeBlackView))
        self.blackView.addGestureRecognizer(gesture)
        
        self.imageView.contentMode = .scaleAspectFit
        let containerView = UIView(frame: CGRect(x:0,y:0,width:320,height:500))
          let imageView = UIImageView()
 
        if let image = self.takenPhoto {
            let ratio = image.size.width / image.size.height
            if containerView.frame.width > containerView.frame.height {
                let newHeight = containerView.frame.width / ratio
                imageView.frame.size = CGSize(width: containerView.frame.width, height: newHeight)
            }
            else{
                let newWidth = containerView.frame.height * ratio
                imageView.frame.size = CGSize(width: newWidth, height: containerView.frame.height)
                self.imageViewConstraint.constant = newWidth
                self.containerViewCons.constant = newWidth
            }
        }
        self.imageView.widthAnchor.constraint(equalTo: self.containerView.widthAnchor, constant: 0).isActive = true
        
        let backGesture = UITapGestureRecognizer(target: self, action: #selector(self.backGestureObserver))
        self.backView.addGestureRecognizer(backGesture)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        Definations.userFromBanuba = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.type == 5 {
            UIImageWriteToSavedPhotosAlbum(self.imageView.image!, self, #selector(savedImage), nil)
        } else if self.type == 8 {
            sharePhoto()
        }
        
        self.setupFaceCountries()
    }
    
    @IBAction func shareImageAction(_ sender: Any) {
        if Definations.isPremiumUser || self.type == 8 || self.type == 5 || self.type == 9 {
            sharePhoto()
        } else {
            if Definations.subscriptionRewardedInterstitial {
                showPaywall(type: 6)
            } else {
                showPaywall(type: 7)
            }
        }
    }
    
    @IBAction func saveImageAction(_ sender: Any) {
        if Definations.isPremiumUser || self.type == 5 || self.type == 8 || self.type == 9 {
            imageView.layer.cornerRadius = 0
            imageView.addSubview(self.mainHiddenView)
            let renderer = UIGraphicsImageRenderer(size: CGSize(width: imageView.frame.width, height: imageView.frame.height))
            let imageMain = renderer.image { ctx in
                imageView.drawHierarchy(in: CGRect(x: 0, y: 0, width: imageView.frame.width, height: imageView.frame.height), afterScreenUpdates: true)
                UIImageWriteToSavedPhotosAlbum(ctx.currentImage, self, #selector(savedImage), nil)
            }
        } else {
            if Definations.subscriptionRewardedInterstitial {
                showPaywall(type: 3)
            } else {
                showPaywall(type: 4)
            }
        }
    }
    
    func showPaywall(type: Int){
        if Definations.subscriptionShowSingleButton == 1 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SubscriptionFirstViewController") as! SubscriptionFirstViewController
            vc.type = type
            vc.takenPhoto = self.takenPhoto
            vc.effectTitle = self.effectTitle
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        } else if Definations.subscriptionShowSingleButton == 2 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SubscriptionViewController") as! SubscriptionViewController
            vc.type = type
            vc.takenPhoto = self.takenPhoto
            vc.effectTitle = self.effectTitle
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        } else if Definations.subscriptionShowSingleButton == 3 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SubscriptionCViewController") as! SubscriptionCViewController
            vc.type = type
            vc.takenPhoto = self.takenPhoto
            vc.effectTitle = self.effectTitle
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @objc func removeBlackView() {
        removePopup(desiredView: self.savedPhoto)
        removePopup(desiredView: self.blackView)
    }
    
    @objc func savedImage(_ im:UIImage, error:Error?, context:UnsafeMutableRawPointer?) {
        if let err = error {
            print(err)
            return
        }
        
        self.savedPhoto.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
        self.savedPhoto.alpha = 0
        self.savedPhoto.layer.cornerRadius = 16
        
        self.view.addSubview(self.blackView)
        
        self.blackView.center = self.view.center
        self.view.addSubview(self.savedPhoto)
       
        self.savedPhoto.center = self.view.center
        self.savedPhoto.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        self.savedPhoto.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
       
        
        UIView.animate(withDuration: 0.1, animations: {
            self.savedPhoto.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            self.savedPhoto.isHidden = false
            self.savedPhoto.alpha = 1
            self.blackView.isHidden = false
            self.blackView.alpha = 0.50
        })
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            OneSignal.promptForPushNotifications(userResponse: { accepted in
              print("User accepted notifications: \(accepted)")
            })
        }
    }
    
    func sharePhoto(){
        DispatchQueue.main.async { [self] in
            // image to share
            if Definations.segueFromFacePercente {
                imageView.layer.cornerRadius = 0
                imageView.addSubview(self.mainHiddenView)
                let renderer = UIGraphicsImageRenderer(size: CGSize(width: imageView.frame.width, height: imageView.frame.height))
                let imageMain = renderer.image { ctx in
                    containerView.drawHierarchy(in: CGRect(x: 0, y: 0, width: containerView.frame.width, height: containerView.frame.height), afterScreenUpdates: true)
                    
                    let image = [ ctx.currentImage ]
                    
                    if let currentImage = ctx.currentImage as? Any {
                        let activityViewController = UIActivityViewController(activityItems: image as [Any], applicationActivities: nil)
                        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
                 
                        // exclude some activity types from the list (optional)
                         activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]

                        // present the view controller
                        self.present(activityViewController, animated: true, completion: nil)
                    }
                }
            } else {
                if let imageT = self.takenPhoto {
                    let image = imageT
                    // set up activity view controller
                    let imageToShare = [ image ]
                    let activityViewController = UIActivityViewController(activityItems: imageToShare as [Any], applicationActivities: nil)
                    activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
             
                    // exclude some activity types from the list (optional)
                     activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]

                    // present the view controller
                    self.present(activityViewController, animated: true, completion: nil)
                } else {
                    self.makeAlertDialog(message: "Hello", title: "Selected photo already nil.", buttonTitle: "OK")
                }
            }
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        DispatchQueue.main.async {
            print("orhan", Definations.lastVCIdentifier)
            self.getNewScreen(boardName: "Main", vcName: Definations.lastVCIdentifier)
            Definations.userFromBanuba = false
        }
    }
    
    //MARK: FacePercentes
    func setupFaceCountries() {
        DispatchQueue.main.async { [self] in
            if Definations.segueFromFacePercente {
                mainHiddenView.fadeIn()
                getPercenteVariables()
            } else {
                mainHiddenView.fadeOut()
            }
        }
    }
    
    func getPercenteVariables() {
        //Sliders
        self.sliderOne.value = Float(self.completionPercentes[0])
        self.sliderTwo.value = Float(self.completionPercentes[1])
        self.sliderThree.value = Float(self.completionPercentes[2])
        self.sliderFour.value = Float(self.completionPercentes[3])
        //PercenteNames
        self.percenteLabelOne.text = "%\(self.completionPercentes[0])"
        self.percenteLabelTwo.text = "%\(self.completionPercentes[1])"
        self.percenteLabelThree.text = "%\(self.completionPercentes[2])"
        self.percenteLabelFour.text = "%\(self.completionPercentes[3])"
        //SliderFlags
        let flagOne = self.percenteNames[0].split(separator: "-")
        let flagTwo = self.percenteNames[1].split(separator: "-")
        let flagThree = self.percenteNames[2].split(separator: "-")
        let flagFour = self.percenteNames[3].split(separator: "-")
        self.flagLabelOne.text = flag(country: String(flagOne[1]))
        self.flagLabelTwo.text = flag(country: String(flagTwo[1]))
        self.flagLabelThree.text = flag(country: String(flagThree[1]))
        self.flagLabelFour.text = flag(country: String(flagFour[1]))
        
        //Names
        self.countryNameOne.text = String(flagOne[0])
        self.countryNameTwo.text = String(flagTwo[0])
        self.countryNameThree.text = String(flagThree[0])
        self.countryNameFour.text = String(flagFour[0])
        
        self.flagLabelOne.fadeIn()
        self.flagLabelTwo.fadeIn()
        self.flagLabelThree.fadeIn()
        self.flagLabelFour.fadeIn()
    }
    
    //MARK: GetCountryFlag
    func flag(country:String) -> String {
        let base : UInt32 = 127397
        var s = ""
        for v in country.unicodeScalars {
            s.unicodeScalars.append(UnicodeScalar(base + v.value)!)
        }
        return String(s)
    }
    
    //MARK: GetPercenteNumbers
    func getRandomValues(amountOfValues:Int, totalAmount:Int) -> [Int]?{
        if amountOfValues < 1{
            return nil
        }

        if totalAmount < 1{
            return nil
        }

        if totalAmount < amountOfValues{
            return nil
        }

        var values:[Int] = []
        var valueLeft = totalAmount

        for i in 0..<amountOfValues{

            if i == amountOfValues - 1{
                values.append(valueLeft)
                break
            }
            let value = Int(arc4random_uniform(UInt32(valueLeft - (amountOfValues - i))) + 1)
            valueLeft -= value
            values.append(value)
        }

        var shuffledArray:[Int] = []

        for _ in 0..<values.count {
            let rnd = Int(arc4random_uniform(UInt32(values.count)))
            shuffledArray.append(values[rnd])
            values.remove(at: rnd)
        }
        
        shuffledArray = shuffledArray.sorted { $0 > $1 } 
        return shuffledArray
    }
    
    //MARK: BackGesture
    @objc func backGestureObserver() {
        DispatchQueue.main.async {
            print("orhan", Definations.lastVCIdentifier)
            self.getNewScreen(boardName: "Main", vcName: Definations.lastVCIdentifier)
            Definations.userFromBanuba = false
        }
    }
}

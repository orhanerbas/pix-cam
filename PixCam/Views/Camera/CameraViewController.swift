//
//  CameraViewController.swift
//  PixCam
//
//  Created by Orhan Erbas on 22.05.2021.
//

import AVFoundation
import UIKit

class CameraViewController: UIViewController {
    
    // Capture Session
    var session: AVCaptureSession?
    // Photo Output
    var output = AVCapturePhotoOutput()
    // Video Preview
    let previewLayer = AVCaptureVideoPreviewLayer()
    // Shutter Button
    @IBOutlet weak var shutterButton: UIButton!
    // CaptureDevice
    var captureDevice: AVCaptureDevice!
    var effectId: Int?
    var effectTitle: String?
    static public var segueCompleted = false

    @IBOutlet weak var bottomCameraView: UIView!
    @IBOutlet weak var selectPhotos: UIButton!
    @IBOutlet weak var changeCamera: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var viewTitle: UILabel!
    @IBOutlet weak var takePhotoButton: UIButton!
    @IBOutlet weak var cameraTopCons: NSLayoutConstraint!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var rotateCameraView: UIView!
    @IBOutlet weak var getLibrayPhotoView: UIView!
    
    
    var usingFrontCamera = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
        Definations.isUserFromCamera = true
        Definations.lastVCIdentifier = "CameraViewController"
        
        if let effectName = Definations.lastEffectName as? String {
            self.effectTitle = effectName
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        session?.startRunning()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        session?.stopRunning()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        previewLayer.frame = view.bounds
    }

    @IBAction func didTapTakePhoto(_ sender: Any) {
        self.previewLayer.connection?.output?.connection(with: .video)?.isVideoMirrored = false
        if usingFrontCamera {
            output.capturePhoto(with: AVCapturePhotoSettings(), delegate: self)
        } else {
            output.capturePhoto(with: AVCapturePhotoSettings(), delegate: self)
        }
    }

    private func checkCameraPermissions() {
        switch AVCaptureDevice.authorizationStatus(for: .video) {
            case .notDetermined:
                // Request
                AVCaptureDevice.requestAccess(for: .video) { [weak self] granted in
                    guard granted else {
                        return
                    }
                    DispatchQueue.main.async {
                        self?.setUpCamera()
                    }
                }
            case .restricted:
                break
            case .denied:
                break
            case .authorized:
                setUpCamera()
            @unknown default:
                break
        }
    }

    func getFrontCamera() -> AVCaptureDevice?{
        let videoDevice = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera], mediaType: AVMediaType.video, position: .front).devices.first
          return videoDevice
      }

    func getBackCamera() -> AVCaptureDevice?{
        let videoDevice = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera], mediaType: AVMediaType.video, position: AVCaptureDevice.Position.back).devices.first
          return videoDevice
      }

    func setUpCamera() {
        let session = AVCaptureSession()
        let session1 = AVCaptureSession()
        let output = AVCapturePhotoOutput()
        let output1 = AVCapturePhotoOutput()

        if usingFrontCamera {
            if let device = getFrontCamera() {
                do {
                    let input = try AVCaptureDeviceInput(device: device)
                    if session.canAddInput(input) {
                        session.addInput(input)
                    }
                    
                    if session.canAddOutput(output) {
                        session.addOutput(output)
                    }
                    
                    previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
                    previewLayer.session = session
                    session.startRunning()
                    self.output = output
                    self.session = session
                } catch {
                    print(error)
                }
            }
        } else {
            if let device = getBackCamera() {
                do {
                    let input = try AVCaptureDeviceInput(device: device)
                    if (session1.canAddInput(input)) {
                        session1.addInput(input)
                    }

                    if (session1.canAddOutput(output1))  {
                        session1.addOutput(output1)
                    }
                    
                    previewLayer.videoGravity = .resizeAspectFill
                    previewLayer.session = session1
                    session1.startRunning()
                    self.output = output1
                    self.session = session1
                } catch {
                    print(error)
                }
            }
        }
    }

    @objc func didTAbBackButton(){
        Definations.isUserFromCamera = false
        self.dismiss(animated: true, completion: nil)
        DispatchQueue.main.async {
            self.getNewScreen(boardName: "Main", vcName: "HomeViewController")
        }
    }
    
    @objc func getLibraryPhotoView() {
        let libraryControllerFirst = UIImagePickerController()
                libraryControllerFirst.sourceType = .photoLibrary
                libraryControllerFirst.delegate = self
                libraryControllerFirst.allowsEditing = false
        present(libraryControllerFirst, animated: true, completion: nil)
        Definations.isPhotoSelected = true
    }
    
    @objc func didChangeViewPress() {
        usingFrontCamera = !usingFrontCamera
        setUpCamera()
    }

    @IBAction func backAction(_ sender: Any) {
        didTAbBackButton()
    }

    @IBAction func didSelectPhotos(_ sender: Any) {
        let libraryControllerFirst = UIImagePickerController()
                libraryControllerFirst.sourceType = .photoLibrary
                libraryControllerFirst.delegate = self
                libraryControllerFirst.allowsEditing = false
        present(libraryControllerFirst, animated: true, completion: nil)
        Definations.isPhotoSelected = true
    }

    @IBAction func didChangeCamera(_ sender: Any) {
        usingFrontCamera = !usingFrontCamera
        setUpCamera()
    }

    func setupView() {
        view.backgroundColor = .black
        view.layer.addSublayer(previewLayer)
        
        let width: CGFloat = view.frame.size.width
        let height: CGFloat = view.frame.size.height

        let path = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height), cornerRadius: 0)
        let rect = CGRect(x: width / 2 - 141.5, y: height / 2.5 - 100, width:  283, height: 356)
        let circlePath = UIBezierPath(ovalIn: rect)
        path.append(circlePath)
        path.usesEvenOddFillRule = true

        let backGesture = UITapGestureRecognizer(target: self, action: #selector(self.didTAbBackButton))
        self.backView.addGestureRecognizer(backGesture)
        
        let changeCameraGesture = UITapGestureRecognizer(target: self, action: #selector(self.didChangeViewPress))
        self.rotateCameraView.addGestureRecognizer(changeCameraGesture)
        
        let libraryGesture = UITapGestureRecognizer(target: self, action: #selector(self.getLibraryPhotoView))
        
        self.getLibrayPhotoView.addGestureRecognizer(libraryGesture)
        
        let fillLayer = CAShapeLayer()
        fillLayer.path = path.cgPath
        fillLayer.fillRule = .evenOdd
        fillLayer.fillColor = UIColor.black.cgColor
        fillLayer.opacity = 0.5
        view.layer.addSublayer(fillLayer)
        view.addSubview(backButton)
        view.addSubview(selectPhotos)
        view.addSubview(changeCamera)
        view.addSubview(takePhotoButton)
        view.addSubview(backView)
        checkCameraPermissions() 
    }
}

extension CameraViewController: AVCapturePhotoCaptureDelegate {
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        guard let data = photo.fileDataRepresentation() else {
            return
        }
        
        let image = UIImage(data: data)
        session?.stopRunning()
        let image2 = UIImage(cgImage: (image?.cgImage!)!, scale: 1.0, orientation: .leftMirrored)

        if Definations.segueFromFacePercente {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PhotoCompletedViewController") as! PhotoCompletedViewController
            vc.filteredImg = image2
            vc.effectTitle = self.effectTitle ?? ""
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        } else {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProgressPhotoViewController") as! ProgressPhotoViewController
            vc.takenPhoto = image2
            vc.effectId = self.effectId
            vc.effectTitle = self.effectTitle
            vc.sourceType = 1
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }
    }
}

extension CameraViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        DispatchQueue.main.async { [self] in
            picker.dismiss(animated: true, completion: nil)
            if let selectedImage = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerOriginalImage")] as? UIImage {
                Definations.isUserFromCamera = true
                if Definations.segueFromFacePercente {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "PhotoCompletedViewController") as! PhotoCompletedViewController
                    vc.filteredImg = selectedImage
                    vc.effectTitle = self.effectTitle ?? ""
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true, completion: nil)
                } else {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProgressPhotoViewController") as! ProgressPhotoViewController
                    vc.takenPhoto = selectedImage
                    vc.effectId = self.effectId
                    vc.effectTitle = self.effectTitle
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true, completion: nil)
                }
            }
        }
    }
        
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        Definations.isPhotoSelected = false
        picker.dismiss(animated: true, completion: nil)
    }
}

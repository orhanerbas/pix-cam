//
//  AppDelegate.swift
//  PhotoFilter
//
//  Created by Orhan Erbas on 18.05.2021.
//

import UIKit
import BanubaSdk
import Firebase
import OneSignal
import Purchases
import GoogleMobileAds
import Adjust

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var bgTask : UIBackgroundTaskIdentifier!
    var window: UIWindow?
    static let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        GADMobileAds.sharedInstance().start(completionHandler: nil)
       // GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers = [ "ce97e767423ff86bd69a8080635031e5" ]
        
        
        let yourAppToken = "97ovqicdhon4"
        let environment = ADJEnvironmentProduction
        let adjustConfig = ADJConfig(
            appToken: yourAppToken,
            environment: environment)
        adjustConfig?.logLevel = ADJLogLevelVerbose
        adjustConfig?.delegate = self
        adjustConfig?.delayStart = 5.5

        Adjust.appDidLaunch(adjustConfig)
        
        if #available(iOS 14, *) {
        } else {
            UserDefaults.standard.set(true, forKey: "appTracking")
        }
        
        Purchases.configure(withAPIKey: "fSSApOyzObmKjMAskNTWhYzwCwlTPada", appUserID: UIDevice.current.identifierForVendor?.uuidString)
        if UserDefaults.standard.bool(forKey: "appTracking") {
            Purchases.shared.collectDeviceIdentifiers()
        }
        // Set the Adjust Id on app launch if it exists
        if let adjustId = Adjust.adid() {
            Purchases.shared.setAdjustID(adjustId)
        }
        
        
        if UserDefaults.standard.bool(forKey: "isPremium") {
            Definations.isPremiumUser = UserDefaults.standard.bool(forKey: "isPremium")
        } else {
            Definations.isPremiumUser = false
        }
        
         DispatchQueue.main.async {
           if FileDownloader.showFiles(effectDirectory: "/MaskEffects") != [] {
              Definations.unzipedFiles = FileDownloader.showFiles(effectDirectory: "/MaskEffects")
            }
         } 

         // OneSignal initialization
         OneSignal.initWithLaunchOptions(launchOptions)
         OneSignal.setAppId("13538992-7847-4372-bc95-b244dd9fb64c")
        
        return true
    }
    
    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
            bgTask = application.beginBackgroundTask(expirationHandler: {
                self.bgTask = UIBackgroundTaskIdentifier.invalid
              
            })
           
        }
        
        func applicationWillEnterForeground(_ application: UIApplication) {
            if bgTask != nil {
                UIApplication.shared.endBackgroundTask(bgTask)
            }
        }
}

extension AppDelegate: AdjustDelegate {
    func adjustAttributionChanged(_ attribution: ADJAttribution?) {
        if let adjustId = attribution?.adid {
            Purchases.shared.setAdjustID(adjustId)
            if UserDefaults.standard.bool(forKey: "appTracking") {
                Purchases.shared.collectDeviceIdentifiers()
            }
        }
    }
}

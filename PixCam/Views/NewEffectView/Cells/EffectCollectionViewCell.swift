//
//  EffectCollectionViewCell.swift
//  PixCam
//
//  Created by Orhan Erbas on 12.07.2021.
//

import UIKit

class EffectCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var makeupName: UILabel!
    @IBOutlet weak var makeupImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        makeupName.alpha = 0.6
    }
    
    override var isSelected: Bool {
        didSet {
            makeupName.alpha = isSelected ? 1.0 : 0.6
        }
    }
    
    func loadImageFromPath(imgPath: String) {
        var imageTemplate = UIImage(named: imgPath)
        if imageTemplate == nil {
            imageTemplate = UIImage(named: "eyes_prod")
        }
        makeupImage.image = imageTemplate
    }
}

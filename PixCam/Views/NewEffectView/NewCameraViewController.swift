//
//  NewCameraViewController.swift
//  PixCam
//
//  Created by Orhan Erbas on 12.07.2021.
//

import UIKit
import BanubaSdk
import BanubaEffectPlayer
import ColorSlider

private struct Defaults {
    
    static let makeupArray = [
        ["Eyeshadow".localized(), "Eyeshadow.color", "Eyeshadow.clear", "eyeShadow"],
        ["Eye flare".localized(), "EyesFlare.strength", "EyesFlare.strength", "eyeflare"],
        ["Eyes whitening".localized(), "EyesWhitening.strength", "EyesWhitening.strength", "eyesWhitening"],
        ["Eyelashes".localized(), "Eyelashes.color", "Eyelashes.clear", "eyelashes"],
        ["EyeLiner".localized(), "Eyeliner.color", "Eyeliner.clear", "eyeliner"],
        ["Eyes morphing".localized(), "FaceMorph.eyes", "FaceMorph.eyes", "eyesMorphing"],
        ["Matt lipstick".localized(), "Lips.matt", "Lips.clear", "mattLipstick"],
        ["Shiny lipstick".localized(), "Lips.shiny", "Lips.clear", "shinnyLipstick"],
        ["Glitter lipstick".localized(), "Lips.glitter", "Lips.clear", "glitterLipstick"],
        ["Teeth whitening".localized(), "TeethWhitening.strength", "TeethWhitening.strength", "teethWhitening"],
        ["Face morphing".localized(), "FaceMorph.face", "FaceMorph.face", "hairColoringTwo"],
        ["Nose morphing".localized(), "FaceMorph.nose", "FaceMorph.nose", "hairColoringTree"],
        ["Hair coloring".localized(), "Hair.color", "Hair.clear", "hairColoring"],
        ["Skin softening".localized(), "SkinSoftening.strength", "SkinSoftening.strength", "skinSoftnening"],
        ["Highlighting".localized(), "Highlighter.color", "Highlighter.clear", "highlighting"],
        ["Contouring".localized(), "Contour.color", "Contour.clear", "contouring"],
        ["Blush".localized(), "Blush.color", "Blush.clear", "blush"],
        ["Softlight".localized(), "Softlight.strength", "Softlight.strength", "softlight"]
    ]
    
    
    //["Skin smoothing","Foundation.strength","Foundation.clear", "skinSmoothing"],
    static let facebeautyArray = [
        ["Teeth whitening", "TeethWhitening.strength", "TeethWhitening.strength"],
        ["Eyes morphing", "FaceMorph.eyes", "FaceMorph.eyes"],
        ["Face morphing", "FaceMorph.face", "FaceMorph.face"],
        ["Nose morphing", "FaceMorph.nose", "FaceMorph.nose"],
        ["Skin softening", "SkinSoftening.strength", "SkinSoftening.strength"],
        //["Skin coloring", "Skin.color", "Skin.clear"],
        ["Hair coloring", "Hair.color", "Hair.clear"],
        //["Eyes coloring", "EyesColor.color", "EyesColor.clear"],
        ["Eye flare", "EyesFlare.strength", "EyesFlare.strength"],
        ["Eyes whitening", "EyesWhitening.strength", "EyesWhitening.strength"]
    ]
    
    static let colorArray = [
        #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0),
        #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1),
        #colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1),
        #colorLiteral(red: 0.8549019694, green: 0.250980407, blue: 0.4784313738, alpha: 1),
        #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1),
        #colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1),
        #colorLiteral(red: 0.721568644, green: 0.8862745166, blue: 0.5921568871, alpha: 1),
        #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    ]
}

class NewCameraViewController: UIViewController {
    
    @IBOutlet weak var makeupCollection: UICollectionView!
    @IBOutlet weak var effectView: EffectPlayerView!
    @IBOutlet weak var alphaLabel: UILabel!
    @IBOutlet weak var colorLabel: UILabel!
    @IBOutlet weak var intensityLabel: UILabel!
    @IBOutlet weak var alphaSlider: UISlider!
    @IBOutlet weak var colorSlider: UISlider!
    @IBOutlet weak var intensitySlider: UISlider!
    @IBOutlet weak var importImageButton: UIButton!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var slidersStack: UIStackView!
    @IBOutlet weak var photoShotButton: UIButton!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var rotateView: UIView!
    private var colorSliderDemo: ColorSlider!
    
    private let imagePicker = UIImagePickerController()
    private var sdkManager = BanubaSdkManager()
    private var effectsArray = [[String]]()
    
    private var renderMode: EffectPlayerRenderMode = .photo
    private var currentEffect: BNBEffect? = nil
    private var cameraSessionType: CameraSessionType {
        if isFrontCamera {
            return renderMode == .photo ? .FrontCameraPhotoSession : .FrontCameraVideoSession
        } else {
            return renderMode == .photo ? .BackCameraPhotoSession : .BackCameraVideoSession
        }
    }
    private var selectedIndexPath = IndexPath(item: 0, section: 0)
    private var isFrontCamera = true
    private var currentEffectName: String = Defaults.makeupArray[0][0]
    private var currentColorMethod: String = Defaults.makeupArray[0][1]
    private var currentClearMethod: String = Defaults.makeupArray[0][2]
    private var currentIntensity: Float = .zero
    private var currentAlpha: Float = .zero
    var lastIndex : Int! = 0 {
        didSet {
            getEffect(at: lastIndex)
        }
    }
    
    private var currentColor: UIColor = .black {
        didSet {
            colorSlider.tintColor = currentColor
            self.getEffect(at: lastIndex)
        }
    }

    
    private var imagePath = ""
    private var documentPath = ""
    private var currentMakeUpName = ""
    @IBOutlet weak var rotateText: UILabel!
    @IBOutlet weak var backText: UILabel!
    @IBOutlet weak var resetText: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        rotateText.text = "Rotate".localized()
        backText.text = "Back".localized()
        resetText.text = "Reset".localized()
        colorLabel.text = "Color".localized()
        alphaLabel.text = "Opacity".localized()
        
        
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        
        var dc = paths[0]
        dc += "/MaskEffects/Makeup/2FMakeup/Makeup"
        
        imagePath = dc
        print("make up image files = 1 ", getDocumentsDirectoryImage())
        setupUI()
        
        documentPath = "/MaskEffects/"
        
        DispatchQueue.main.async { [self] in
            colorSlider.value = 1
            let currentColor = Defaults.colorArray[1]
            currentEffect?.callJsMethod(
                currentColorMethod,
                params: currentColor.getColorStringRepresentation(withAlpha: currentAlpha)
            )
            self.currentColor = currentColor
        }
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        sdkManager.destroy()
        sdkManager.input.stopCamera()
        sdkManager.stopEffectPlayer()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupSDKManager()
        sdkManager.input.startCamera()
    }
    
    private func setupSDKManager() {
        sdkManager.setup(configuration: EffectPlayerConfiguration(
                            renderMode: .photo))
        effectView?.effectPlayer = sdkManager.effectPlayer
        guard let layer = effectView?.layer as? CAEAGLLayer else { return }
        sdkManager.setRenderTarget(layer: layer, playerConfiguration: nil)
        currentEffect = sdkManager.loadEffect(imagePath, synchronous: false)
        sdkManager.startEffectPlayer()
    }
    
    @IBAction func backAction(_ sender: Any) {
        DispatchQueue.main.async {
            self.getNewScreen(boardName: "Main", vcName: "HomeViewController")
        }
    }
    
    @objc func backGestureObserver() {
        DispatchQueue.main.async {
            self.getNewScreen(boardName: "Main", vcName: "HomeViewController")
        }
    }
    
    var setonce = false
    private func hideControls(hideColors: Bool, hideIntensity: Bool) {
        colorLabel.isHidden = hideColors
        colorSlider.isHidden = hideColors
        colorSlider.isHidden = hideColors//demoyu actiktan sonra buray kapat alphayi sifirla
        /*
         if !setonce {
             colorSliderDemo = ColorSlider(orientation: .horizontal, previewSide: .top)
             colorSliderDemo.addTarget(self, action: #selector(changedColor(slider:)), for: .allEvents)
             setupConstraints()
             setonce = true
         }
         */
        //colorSliderDemo.isHidden = hideColors
        alphaLabel.isHidden = hideColors
        alphaSlider.isHidden = hideColors
        intensityLabel.isHidden = hideIntensity
        intensitySlider.isHidden = hideIntensity
    }
    
    func setupConstraints() {
        slidersStack.addSubview(colorSliderDemo)
        let colorSliderwidth = CGFloat(280)
        let colorSliderHeight = CGFloat(15)
        colorSliderDemo.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            colorSliderDemo.widthAnchor.constraint(equalToConstant: colorSliderwidth),
            colorSliderDemo.heightAnchor.constraint(equalToConstant: colorSliderHeight),
            colorSliderDemo.rightAnchor.constraint(equalTo: slidersStack.rightAnchor)
        ])
    }
    
    @objc func changedColor(slider: ColorSlider) {
        self.currentColor = slider.color
    }
    
    private func setupUI() {
        Definations.lastVCIdentifier = "NewCameraViewController"
        Definations.userFromBanuba = true
        importImageButton.isEnabled = true
        effectsArray = Defaults.makeupArray
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        effectView.layer.cornerRadius = 20
        effectView.layer.masksToBounds = true
        makeupCollection.selectItem(
            at: selectedIndexPath,
            animated: false,
            scrollPosition: .centeredVertically
        )
        hideControls(hideColors: false, hideIntensity: true)
        currentColor = Defaults.colorArray[Int(colorSlider.value)]
        currentAlpha = alphaSlider.value
        if #available(iOS 13.0, *) {
            segmentControl.setTitleTextAttributes([.foregroundColor: UIColor.black], for: .selected)
            segmentControl.selectedSegmentTintColor = .white
        } else {
            segmentControl.tintColor = .white
        }
        
        photoShotButton.backgroundColor = .clear
        photoShotButton.layer.borderWidth = 4
        photoShotButton.layer.borderColor = UIColor.white.withAlphaComponent(0.6).cgColor
        photoShotButton.frame = CGRect(x: 160, y: 100, width: 50, height: 50)
        photoShotButton.layer.cornerRadius = 0.8 * photoShotButton.bounds.size.width + 2
        photoShotButton.clipsToBounds = true
        
        self.alphaSlider.layer.cornerRadius = 5
        for sliderItems in alphaSlider.subviews {
            sliderItems.layer.cornerRadius = 5
        }
        
        self.alphaSlider.setThumbImage(UIImage(named: "thumbImage"), for: .normal)
        self.alphaSlider.setThumbImage(UIImage(named: "thumbImage"), for: .highlighted)
         
        self.colorSlider.layer.cornerRadius = 5
        self.colorSlider.subviews[0].layer.cornerRadius = 5
        self.colorSlider.setThumbImage(UIImage(named: "thumbImage"), for: .normal)
        self.colorSlider.setThumbImage(UIImage(named: "thumbImage"), for: .highlighted)
        
        self.intensitySlider.layer.cornerRadius = 5
        self.intensitySlider.subviews[0].layer.cornerRadius = 5
        self.intensitySlider.setThumbImage(UIImage(named: "thumbImage"), for: .normal)
        self.intensitySlider.setThumbImage(UIImage(named: "thumbImage"), for: .highlighted)
        
        let backGesture = UITapGestureRecognizer(target: self, action: #selector(self.backGestureObserver))
        self.backView.addGestureRecognizer(backGesture)
        
        let rotateGesture = UITapGestureRecognizer(target: self, action: #selector(self.rotateViewObserver))
        self.rotateView.addGestureRecognizer(rotateGesture)
    }
    
    private func getDocumentsDirectoryImage() -> URL {
        
        return URL(string: imagePath)!
    }
    
    @IBAction func makePhotoAction(_ sender: Any) {
        sdkManager.makeCameraPhoto(cameraSettings: .init(useStabilization: true, flashMode: .off)) { (image) in
            if let image = image {
                DispatchQueue.main.async {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ShareViewController") as! ShareViewController
                    vc.modalPresentationStyle = .fullScreen
                    vc.takenPhoto = image
                    vc.effectTitle = self.currentMakeUpName
                    self.present(vc, animated: true, completion: nil)
                }
            }
        }
    }
}

// MARK:- Actions
extension NewCameraViewController {
    @IBAction func colorChangeAction(_ sender: UISlider) {
        let currentColor = Defaults.colorArray[Int(sender.value)]
        self.currentColor = currentColor
        currentEffect?.callJsMethod(
            currentColorMethod,
            params: currentColor.getColorStringRepresentation(withAlpha: currentAlpha)
        )
    }
    
    @IBAction func alphaChangeAction(_ sender: UISlider) {
        currentAlpha = sender.value
        self.currentEffect?.callJsMethod(
            currentColorMethod,
            params: currentColor.getColorStringRepresentation(withAlpha: currentAlpha)
        )
    }
    
    @IBAction func intensityChangeAction(_ sender: UISlider) {
        currentIntensity = sender.value
        currentEffect?.callJsMethod(currentColorMethod, params: "\(currentIntensity)")
    }
    
    @IBAction func clearMakeupColor(_ sender: UIButton) {
        currentEffect?.callJsMethod(currentClearMethod, params: "")
    }
    
    @IBAction func importImage(_ sender: UIButton) {
        let alertController = UIAlertController(
            title: "\(currentEffectName)",
            message: "Select custom \(currentEffectName) texture in your Gallery",
            preferredStyle: .alert
        )
        let openGallery = UIAlertAction(title: "Open Gallery", style: .default) { UIAlertAction in
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        alertController.addAction(openGallery)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func rotateCamera(_ sender: UIButton) {
        isFrontCamera = !isFrontCamera
        sdkManager.input.switchCamera(to: cameraSessionType) {
            print("RotateCamera")
        }
    }
    
    @objc func rotateViewObserver() {
        isFrontCamera = !isFrontCamera
        sdkManager.input.switchCamera(to: cameraSessionType) {
            print("RotateCamera")
        }
    }
    
    @IBAction func resetPlayer(_ sender: UIButton) {
        sdkManager.destroyEffectPlayer()
        setupSDKManager()
    }
    
    @IBAction func makePhoto(_ sender: UIButton) {
        sdkManager.makeCameraPhoto(cameraSettings: .init(useStabilization: true, flashMode: .off)) { (image) in
            if let image = image {
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Image was successfully saved", message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true) {
                        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
                    }
                }
            }
        }
    }
    
    
    
    @IBAction func changeCategory(_ sender: UISegmentedControl) {
        switch segmentControl.selectedSegmentIndex {
        case 0:
            effectsArray = Defaults.makeupArray
            makeupCollection.reloadData()
        default:
            effectsArray = Defaults.facebeautyArray
            makeupCollection.reloadData()
        }
    }
}

// MARK:- UICollectionView extension
extension NewCameraViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return 0
        }
        else {
            return self.effectsArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "addNewEffectCell", for: indexPath)
            return cell
        } else {
            guard let cell = makeupCollection.dequeueReusableCell(withReuseIdentifier: "myCell", for: indexPath) as? EffectCollectionViewCell else { return EffectCollectionViewCell() }
//            cell.makeupName.text = self.effectsArray[indexPath.row][0]
            cell.backgroundColor = .white
            cell.layer.cornerRadius = 0.5 * cell.bounds.size.width
//            let imgPath = AppDelegate.documentsPath + "/MaskEffects/Makeup/2FMakeup/Makeup/" +  (self.effectsArray[indexPath.row][0])  + "/preview.png"
//            print("path image = ", imgPath)
            if indexPath.row == 1 {
                if let item = makeupCollection.indexPathsForVisibleItems.first {
                    makeupCollection.scrollToItem(at: item, at: .centeredHorizontally, animated: true)
                }
            } 
            cell.loadImageFromPath(imgPath: self.effectsArray[indexPath.row][3])
//            colorLabel.text = "\(self.effectsArray[indexPath.row][0]) color"
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 70, height: 70)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.makeupCollection.scrollToItem(
            at: indexPath,
            at: .centeredHorizontally,
            animated: true
        )
    
        lastIndex = indexPath.row
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        for index in makeupCollection.indexPathsForVisibleItems {
            let visibleRect = CGRect(origin: makeupCollection.contentOffset, size: makeupCollection.bounds.size)
            let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
            let visibleIndexPath = makeupCollection.indexPathForItem(at: visiblePoint)
            let generator = UINotificationFeedbackGenerator()
            generator.notificationOccurred(.success)
           
            DispatchQueue.main.async { [self] in
                if visibleIndexPath != nil {
                    makeupCollection.scrollToItem(at: visibleIndexPath ?? index, at: .centeredHorizontally, animated: true)
                    lastIndex = Int(visibleIndexPath?[1] ?? 0)
                }
            }
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        for index in makeupCollection.indexPathsForVisibleItems {
            let visibleRect = CGRect(origin: makeupCollection.contentOffset, size: makeupCollection.bounds.size)
            let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
            let visibleIndexPath = makeupCollection.indexPathForItem(at: visiblePoint)
            let generator = UINotificationFeedbackGenerator()
            generator.notificationOccurred(.success)
           
            DispatchQueue.main.async { [self] in
                if visibleIndexPath != nil {
                    makeupCollection.scrollToItem(at: visibleIndexPath ?? index, at: .centeredHorizontally, animated: true)
                    lastIndex = Int(visibleIndexPath?[1] ?? 0)
                }
            }
        }
    }
    
    func getEffect(at index: Int) {
        DispatchQueue.main.async { [self] in
            intensityLabel.text = "\(self.effectsArray[lastIndex][0])"
            colorLabel.text = "\(self.effectsArray[lastIndex][0]) color"
            currentEffectName = effectsArray[index][0]
            currentColorMethod = effectsArray[index][1]
            currentClearMethod = effectsArray[index][2]
            currentEffect?.callJsMethod(
                currentColorMethod,
                params: currentColor.getColorStringRepresentation(withAlpha: currentAlpha)
            )
            self.currentMakeUpName = effectsArray[index][0]
            switch effectsArray[index][0] {
            case "Teeth whitening", "Eyes morphing", "Face morphing", "Nose morphing", "Softlight", "Eye flare", "Eyes whitening", "Skin smoothing", "Skin softening":
                currentEffect?.callJsMethod(currentColorMethod, params: "\(currentIntensity)")
                importImageButton.isEnabled = false
                hideControls(hideColors: true, hideIntensity: false)
            case "Eyeshadow", "Eyeliner", "Eyelashes", "Contouring", "Highlighting", "Blush":
                importImageButton.isEnabled = true
                hideControls(hideColors: false, hideIntensity: true)
            default:
                importImageButton.isEnabled = false
                hideControls(hideColors: false, hideIntensity: true)
            }
        }
    }
}


extension NewCameraViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        try? FileManager.default.removeItem(at: getDocumentsDirectoryImage().appendingPathComponent("makeup.png"))
        let outputUrl = getDocumentsDirectoryImage().appendingPathComponent("makeup.png")
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            let imageData = pickedImage.pngData()
            try? imageData?.write(to: outputUrl)
            currentEffect?.callJsMethod(
                currentColorMethod,
                params: currentColor.getColorStringRepresentation(withAlpha: currentAlpha)
            )
            print("output url = ", outputUrl.path, currentEffectName)
            currentEffect?.callJsMethod("\(currentEffectName).set", params: outputUrl.path)
            
        }
        dismiss(animated: true, completion: nil)
    }
}

// MARK:- UIColor extension
extension UIColor {
    var redValue: CGFloat { return CIColor(color: self).red }
    var greenValue: CGFloat { return CIColor(color: self).green }
    var blueValue: CGFloat { return CIColor(color: self).blue }
    var alphaValue: CGFloat { return CIColor(color: self).alpha }
    
    func getColorStringRepresentation(withAlpha currentAlpha: Float) -> String {
        let red = String(Float(redValue))
        let green = String(Float(greenValue))
        let blue = String(Float(blueValue))
        let alpha = String(currentAlpha)
        return "\(red) \(green) \(blue) \(alpha)"
    }
}

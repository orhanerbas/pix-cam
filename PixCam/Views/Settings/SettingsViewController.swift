//
//  SettingsViewController.swift
//  PhotoFilter
//
//  Created by Orhan Erbas on 19.05.2021.
//

import UIKit
import BanubaSdk
import Purchases

class SettingsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet var popupContainer: UIView!
    @IBOutlet weak var popup: UIView!
    
    @IBOutlet weak var popupLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var clearButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    @IBOutlet weak var scrollView: IKScrollView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var proButton: UIView!
    @IBOutlet weak var pageTitle: UILabel!
    @IBOutlet weak var proText: UILabel!
    @IBOutlet weak var PopupTitle: UILabel!
    @IBOutlet weak var cancelText: UIButton!
    @IBOutlet weak var popupDescription: UILabel!
    @IBOutlet weak var clearText: UIButton!
//    var filePath = FileDownloader.showFiles(effectDirectory: "/MaskEffects")
//    let maskPath = AppDelegate.documentsPath + "/MaskEffects/"
//    var dataUrl: URL?
//    var size = Int64()
//    var fileSizeWithUnit: String = ""
    
    let menuItems = [
        SettingsMenu(id: 1, menuTitle: "PRIVACY POLICY".localized(), menuDescription: "Tap to view privacy policy".localized(), menuArrow: UIImage(named: "Arrow-Right")!, bgColor: .init(named: "SettingsMenuBg1")!, titleColor: .init(named: "tabBarColor")!),
        SettingsMenu(id: 2, menuTitle: "TERMS OF SERVICE".localized(), menuDescription: "Tap to view the terms".localized(), menuArrow: UIImage(named: "arrow-right2")!, bgColor: .init(named: "SettingsMenuBg2")!, titleColor: .init(named: "SettingsTitleColor2")!),
        SettingsMenu(id: 3, menuTitle: "FEEDBACK".localized(), menuDescription: "Have problem ? Give us feedback!".localized(), menuArrow: UIImage(named: "Arrow-Right")!, bgColor: .init(named: "SettingsMenuBg1")!, titleColor: .init(named: "SettingsTitleColor3")!),
//        SettingsMenu(id: 4, menuTitle: "CLEAR CACHE".localized(), menuDescription: "120 MB", menuArrow: UIImage(named: "arrow-right4")!, bgColor: .init(named: "SettingsMenuBg4")!, titleColor: .init(named: "SettingsTitleColor4")!)
        
        
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pageTitle.text = "Settings".localized()
        proText.text = "Pro".localized()
        PopupTitle.text = "Clear Cache".localized()
        popupDescription.text = "Are you sure to clear the cache file?".localized()
        cancelText.setTitle("Cancel".localized(), for: .normal)
        clearText.setTitle("Clear".localized(), for: .normal)
        NotificationCenter.default.addObserver(self, selector: #selector(isPremium), name: NSNotification.Name("User is Premium"), object: nil)
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        tableView.delegate = self
        tableView.dataSource = self
        self.proButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openSubscription)))
        
//        if FileDownloader.showFiles(effectDirectory: "/MaskEffects") != [] {
//            Definations.unzipedFiles = FileDownloader.showFiles(effectDirectory: "/MaskEffects")
//            dataUrl = getSizeFromUrl()
//            size = directorySize(url: dataUrl!)
//            fileSizeWithUnit = ByteCountFormatter.string(fromByteCount: self.size, countStyle: .file)
//       }
        proText.numberOfLines = 0
        proText.adjustsFontSizeToFitWidth = true
        proText.lineBreakMode = .byWordWrapping
        proText.textAlignment = .center
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if Definations.isPremiumUser {
            self.proButton.isHidden = true
        }
        
        Purchases.shared.purchaserInfo { (purchaserInfo, error) in
            if purchaserInfo?.entitlements.all["PixCam_Premium"]?.isActive == true {
                // User is "premium"
                self.isPremium()
            } else {
                print("not Premium")
            }
        }
    }
    
    //MARK: - Size Func
      func getSizeFromUrl() -> URL {
          let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
          var documentsDirectory = paths[0]
          documentsDirectory += "/MaskEffects/"
          let url = URL(fileURLWithPath: documentsDirectory)
          return url
      }
    
//      func removeOldFileIfExist() {
//          if FileManager.default.fileExists(atPath: maskPath) {
//              do {
//                  try FileManager.default.removeItem(atPath: maskPath)
//                  print("has been removed")
//                  fileSizeWithUnit = "0 MB"
//                defaults.set(false, forKey: "newEffectData")
//                if Definations.unzipedFiles.count != 0 { Definations.unzipedFiles.removeAll() }
//                if Definations.controllerArray.count != 0 { Definations.controllerArray.removeAll() }
//                
//                Definations.isMaskEffectsActive = true
//                Definations.isLipsEffectsActive = true
//                Definations.isMakeupEffectsActive = true
//                tableView.reloadData()
//              } catch {
//                  print("an error during a removing")
//              }
//          }
//      }
    
        @objc func isPremium() {
            Definations.isPremiumUser = UserDefaults.standard.bool(forKey: "isPremium")
        }
      
      func directorySize(url: URL) -> Int64 {
          let contents: [URL]
          do {
              contents = try FileManager.default.contentsOfDirectory(at: url, includingPropertiesForKeys: [.fileSizeKey, .isDirectoryKey])
          } catch {
              return 0
          }
          var size: Int64 = 0
          for url in contents {
              let isDirectoryResourceValue: URLResourceValues
              do {
                  isDirectoryResourceValue = try url.resourceValues(forKeys: [.isDirectoryKey])
              } catch {
                  continue
              }
              if isDirectoryResourceValue.isDirectory == true {
                  size += directorySize(url: url)
              } else {
                  let fileSizeResourceValue: URLResourceValues
                  do {
                      fileSizeResourceValue = try url.resourceValues(forKeys: [.fileSizeKey])
                  } catch {
                      continue
                  } 
                  size += Int64(fileSizeResourceValue.fileSize ?? 0)
              }
          }
          return size
      }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollView.sizeMatching = .Dynamic(
            width: { self.view.frame.width },
            height: {self.view.frame.height / 1 + 50}
        )
    }
   
    
    @objc fileprivate func openSubscription(){
        if Definations.subscriptionShowSingleButton == 1 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SubscriptionFirstViewController") as! SubscriptionFirstViewController
            vc.modalPresentationStyle = .fullScreen
            vc.type = 12
            self.present(vc, animated: true, completion: nil)
        } else if Definations.subscriptionShowSingleButton == 2 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SubscriptionViewController") as! SubscriptionViewController
            vc.modalPresentationStyle = .fullScreen
            vc.type = 12
            self.present(vc, animated: true, completion: nil)
        } else if Definations.subscriptionShowSingleButton == 3 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SubscriptionCViewController") as! SubscriptionCViewController
            vc.modalPresentationStyle = .fullScreen
            vc.type = 12
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @objc fileprivate func animateIn(){
        self.popup.transform = CGAffineTransform(translationX: 0, y: self.popupContainer.frame.height)
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
            self.popup.transform = .identity
        })
    }
    
    @objc fileprivate func animateOut(){
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
            self.popup.transform = CGAffineTransform(translationX: 0, y: self.popupContainer.frame.height)
        }) { (complete) in
            if complete {
                self.tabBarController?.tabBar.isHidden = false
                self.backButton.isHidden = false
                self.proButton.isHidden = false
                self.popupContainer.removeFromSuperview()
            }
        }
    }
    
    @objc func clearButtonAction() {
        self.animateOut()
    }
    
    func createPopup(){
        self.popupContainer.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(animateOut)))
        self.popupContainer.backgroundColor = UIColor.gray.withAlphaComponent(0.6)
        self.popupContainer.frame = UIScreen.main.bounds
        
        self.popup.translatesAutoresizingMaskIntoConstraints = false
        self.popup.backgroundColor = .white
        self.popup.layer.cornerRadius = 16
        
        self.cancelButton.layer.borderColor = UIColor(named: "tabBarColor")?.cgColor
        self.cancelButton.layer.cornerRadius = 15
        self.cancelButton.layer.borderWidth = 1
        self.clearButton.layer.cornerRadius = 15
        
        self.tabBarController?.tabBar.isHidden = true
        self.backButton.isHidden = true
        self.proButton.isHidden = true

        self.view.addSubview(popupContainer)
        
        animateIn()
    }
    
    func createSecondPopup(){
        self.popupContainer.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(animateOut)))
        self.popupContainer.backgroundColor = UIColor.gray.withAlphaComponent(0.6)
        self.popupContainer.frame = UIScreen.main.bounds
        
        self.popup.translatesAutoresizingMaskIntoConstraints = false
        self.popup.backgroundColor = .white
        self.popup.layer.cornerRadius = 16
        
        self.cancelButton.layer.borderColor = UIColor(named: "tabBarColor")?.cgColor
        self.cancelButton.layer.cornerRadius = 15
        self.cancelButton.layer.borderWidth = 1
        self.clearButton.layer.cornerRadius = 15
        
        self.tabBarController?.tabBar.isHidden = true
        self.backButton.isHidden = true
        self.proButton.isHidden = true
        
        self.popupLabel.text = "Clear succesfully."
        self.clearButton.setTitle("OK", for: .normal)

        self.view.addSubview(popupContainer)
        
        animateIn()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "menuCell", for: indexPath) as! SettingsTableViewCell
        cell.post = menuItems[indexPath.row]
        
//        if indexPath.row == 3 {
//             cell.menuDescription.text = fileSizeWithUnit
//           }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyViewController") as! PrivacyViewController
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        } else if indexPath.row == 1 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "TermsViewController") as! TermsViewController
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        } else if indexPath.row == 2 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "FeedBackViewController") as! FeedBackViewController
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        } else if indexPath.row == 3 {
            //createPopup()
        }
    }
    @IBAction func closePopup(_ sender: Any) {
        animateOut()
    }
    @IBAction func clearAction(_ sender: Any) {
//        DispatchQueue.main.async { [self] in
//            animateOut()
//            DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [self] in
//                popupLabel.text = "Are you sure to clear the cache file?"
//                clearButton.setTitle("Clear", for: .normal)
//                removeOldFileIfExist()
//                getNewScreen(boardName: "Main", vcName: "LoadingVC")
//              }
//            tableView.reloadData()
//        }
//        tableView.reloadData()
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

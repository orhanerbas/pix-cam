//
//  TermsViewController.swift
//  PixCam
//
//  Created by Orhan Erbas on 27.05.2021.
//

import UIKit
import WebKit

class TermsViewController: UIViewController, WKNavigationDelegate {
    @IBOutlet weak var wkWebView: WKWebView!
    @IBOutlet weak var pageTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pageTitle.text = "Terms of Service".localized()
        wkWebView.navigationDelegate = self

        let url = URL(string: "https://pixcam.app/term-of-use.html")!
        wkWebView.load(URLRequest(url: url))
        wkWebView.allowsBackForwardNavigationGestures = true
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

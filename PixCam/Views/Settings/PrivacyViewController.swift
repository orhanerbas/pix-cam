//
//  PrivacyViewController.swift
//  PixCam
//
//  Created by Orhan Erbas on 26.05.2021.
//

import UIKit
import WebKit

class PrivacyViewController: UIViewController, WKNavigationDelegate {
    @IBOutlet weak var wkWebView: WKWebView!
    @IBOutlet weak var pageTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        wkWebView.navigationDelegate = self
        
        pageTitle.text = "Privacy Policy".localized()
        let url = URL(string: "https://pixcam.app/privacy-policy.html")!
        wkWebView.load(URLRequest(url: url))
        wkWebView.allowsBackForwardNavigationGestures = true
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

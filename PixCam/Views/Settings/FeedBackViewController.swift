//
//  FeedBackViewController.swift
//  PixCam
//
//  Created by Orhan Erbas on 27.05.2021.
//

import UIKit
import MessageUI

class FeedBackViewController: UIViewController, MFMailComposeViewControllerDelegate {

    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var feedBackField: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    
    @IBOutlet weak var backButtonBottom: NSLayoutConstraint!
    @IBOutlet weak var pageTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sendButton.layer.cornerRadius = 25
        pageTitle.text = "Feed Back".localized()
        nameField.attributedPlaceholder = NSAttributedString(string: "Name Surname".localized(), attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray])
        emailField.attributedPlaceholder = NSAttributedString(string: "E-mail".localized(), attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray])
        feedBackField.attributedPlaceholder = NSAttributedString(string: "Feed back".localized(), attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray])
        sendButton.setTitle("Send".localized(), for: .normal)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
     
     
    @objc func keyboardWillShow(notification: NSNotification) {
        switch UIDevice().type {
        case .iPhone8, .iPhone8Plus, .iPhoneSE, .iPhoneSE2:
            if ((notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue) != nil {
                if self.view.frame.origin.y == 0 {
                    self.view.frame.origin.y -= 10
                    backButtonBottom.constant = 10
                }
            }
        default:
            return
        }

    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    @IBAction func sendFeedback(_ sender: Any) {
        sendEmail()
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func sendEmail() {
        let mailComposeViewController = configureMailComposer()
            if MFMailComposeViewController.canSendMail(){
                self.present(mailComposeViewController, animated: true, completion: nil)
                print("Email succesfully sended")
            }else{
                print("Can't send email")
            }
        
        func configureMailComposer() -> MFMailComposeViewController{
            let mailComposeVC = MFMailComposeViewController()
            let prepareMailBody = "\(self.feedBackField.text!) <br><br> \(self.emailField.text ?? "")"
            mailComposeVC.mailComposeDelegate = self
            mailComposeVC.setToRecipients(["support@pixcam.app"])
            mailComposeVC.setSubject(self.nameField.text!)
            mailComposeVC.setMessageBody(prepareMailBody, isHTML: true)
            return mailComposeVC
        }
        
    }

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.makeAlertDialog(message: "Sent".localized(), title: "We have received your message, we will return as soon as possible.".localized(), buttonTitle: "Done".localized())
        }
    }
}


extension UIViewController {
    func makeAlertDialog(message: String, title: String, buttonTitle: String ) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: buttonTitle, style: .default)
           alert.addAction(defaultAction)
           DispatchQueue.main.async(execute: {
             self.present(alert, animated: true)
           })
    }
}

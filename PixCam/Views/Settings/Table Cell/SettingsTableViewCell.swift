//
//  SettingsTableViewCell.swift
//  PixCam
//
//  Created by Orhan Erbas on 26.05.2021.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {

    @IBOutlet weak var menuContent: UIView!
    @IBOutlet weak var menuTitle: UILabel!
    @IBOutlet weak var menuDescription: UILabel!
    @IBOutlet weak var menuArrow: UIImageView!
    
    var post: SettingsMenu! {
        didSet {
            updateUI()
        }
    }
    
    func updateUI(){
        if let countryCode = Locale.current.languageCode {
            if countryCode == "it" || countryCode == "pt" {
                menuTitle.font = UIFont(name: "Poppins-ExtraBold", size: 19)
            }
        }
        menuContent.layer.cornerRadius = 16
        menuContent.backgroundColor = post.bgColor
        menuTitle.textColor = post.titleColor
        menuTitle.text = post.menuTitle
        menuDescription.text = post.menuDescription
        menuArrow.image = post.menuArrow
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  ViewController.swift
//  PhotoFilter
//
//  Created by Orhan Erbas on 18.05.2021.
//

import UIKit
import OneSignal
import AppTrackingTransparency
import Purchases
import StoreKit

class ViewController: UIViewController {
   
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var tryButton: UIButton!
    @IBOutlet weak var SplashHeader_1: UILabel!
    @IBOutlet weak var SplashHeaderDescription_1: UILabel!
    
    //Color Variables
    var ColorOne = ""
    var ColorTwo = ""
    var ColorTree = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 14, *) {
            appTracking()
        }
        rateUs()

        showPaywall()
        SplashHeader_1.text = Definations.SplashHeader_1
        SplashHeaderDescription_1.text = Definations.SplashHeaderDescription_1
        tryButton.layer.cornerRadius = 32
        tryButton.setTitle(Definations.SplashButtonText_1, for: .normal)
        tryButton.setTitleColor(hexStringToUIColor(hex: Definations.SplashButtonTextColor), for: .normal)
        
        if !Definations.SplashButtonColor.contains(",") {
            if Definations.SplashButtonColor.count > 0 {
                tryButton.backgroundColor = hexStringToUIColor(hex: Definations.SplashButtonColor)
            }
        } else {
            DispatchQueue.main.async { [self] in
                let yourString = Definations.SplashButtonColor
                 
                let splitedCodes = yourString.split(separator: ",")

                if splitedCodes.count > 3 || splitedCodes.count == 3 {
                    ColorOne = String(splitedCodes[0])
                    ColorTwo = String(splitedCodes[1])
                    ColorTree =  String(splitedCodes[2])
                    
                    tryButton.applyGradient(colours: [
                                                    hexStringToUIColor(hex: ColorOne),
                                                    hexStringToUIColor(hex: ColorTwo),
                                                    hexStringToUIColor(hex: ColorTree)
                    ])
                } else if splitedCodes.count > 2 || splitedCodes.count == 2 {
                    ColorOne = String(splitedCodes[0])
                    ColorTwo = String(splitedCodes[1])
                    
                    tryButton.applyGradient(colours: [
                                                    hexStringToUIColor(hex: ColorOne),
                                                    hexStringToUIColor(hex: ColorTwo),
                    ])
                }
            }
        }
        
    }
    
    func rateUs() {
        if #available(iOS 10.3, *) {
            SKStoreReviewController.requestReview()

        } else if let url = URL(string: "itms-apps://itunes.apple.com/app/" + "1571961521") {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)

            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {

    }
    
    @IBAction func nextBtnIcn(_ sender: Any) {
        nextPage()
    }
    
    @IBAction func nextButtonAction(_ sender: Any) {
        nextPage()
        
    }
    
    func showPaywall(){
        if !Definations.isPremiumUser {
            if Definations.subscriptionShowPaywallFirst {
                if !Definations.PaywallFirst {
                    Definations.PaywallFirst = true
                    if Definations.subscriptionShowSingleButton == 1 {
                        OperationQueue.main.addOperation {
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SubscriptionFirstViewController") as! SubscriptionFirstViewController
                            vc.modalPresentationStyle = .fullScreen
                            vc.type = 12
                            self.present(vc, animated: true, completion: nil)
                        }
                    } else if Definations.subscriptionShowSingleButton == 2 {
                        OperationQueue.main.addOperation {
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SubscriptionViewController") as! SubscriptionViewController
                            vc.modalPresentationStyle = .fullScreen
                            vc.type = 12
                            self.present(vc, animated: true, completion: nil)
                        }
                    } else if Definations.subscriptionShowSingleButton == 3 {
                        OperationQueue.main.addOperation {
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SubscriptionCViewController") as! SubscriptionCViewController
                            vc.modalPresentationStyle = .fullScreen
                            vc.type = 12
                            self.present(vc, animated: true, completion: nil)
                        }
                    }
                }
            }
        }
    }
    
    func nextPage() {
        guard let window = UIApplication.shared.windows.filter({$0.isKeyWindow}).first else {
            return
        }
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "secondVc")

        window.rootViewController = vc

        let options: UIView.AnimationOptions = .transitionCrossDissolve

        let duration: TimeInterval = 0.3

        UIView.transition(with: window, duration: duration, options: options, animations: {}, completion:
        { completed in
            // maybe do something on completion here
        })
    } 
    
    func presentDetail(_ viewControllerToPresent: UIViewController) {
            let transition = CATransition()
            transition.duration = 0.25
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        self.view.window!.layer.add(transition, forKey: .none)

            present(viewControllerToPresent, animated: false)
        }

        func dismissDetail() {
            let transition = CATransition()
            transition.duration = 0.25
            transition.type = CATransitionType.push
            transition.subtype = CATransitionSubtype.fromLeft
            self.view.window!.layer.add(transition, forKey: kCATransition)

            dismiss(animated: false)
        }
    
    @available(iOS 14, *)
    func appTracking(){
        ATTrackingManager.requestTrackingAuthorization { status in
            switch status {
            case .notDetermined:
                break
            case .restricted:
                break
            case .denied:
                UserDefaults.standard.set(false, forKey: "appTracking")
                break
            case .authorized:
                UserDefaults.standard.set(true, forKey: "appTracking")
                Purchases.shared.collectDeviceIdentifiers()
                break
            @unknown default:
                break
            }
        }
    }
    
}


extension UIPageViewController {
    func goToNextPage(animated: Bool = true, completion: ((Bool) -> Void)? = nil) {
        if let currentViewController = viewControllers?[0] {
            if let nextPage = dataSource?.pageViewController(self, viewControllerAfter: currentViewController) {
                setViewControllers([nextPage], direction: .forward, animated: animated, completion: completion)
            }
        }
    }

    func goToPreviousPage(animated: Bool = true, completion: ((Bool) -> Void)? = nil) {
        if let currentViewController = viewControllers?[0] {
            if let previousPage = dataSource?.pageViewController(self, viewControllerBefore: currentViewController){
                setViewControllers([previousPage], direction: .reverse, animated: true, completion: completion)
            }
        }
    }
    

}

//
//  SubscriptionsViewController.swift
//  PixCam
//
//  Created by Orhan Erbas on 25.05.2021.
//

import UIKit
import AVKit
import AVFoundation
import Purchases
import GoogleMobileAds
import NVActivityIndicatorView

class SubscriptionViewController: UIViewController {

    @IBOutlet weak var mainStackView: UIStackView!
    @IBOutlet weak var scrollView: IKScrollView!
    @IBOutlet weak var sliderView: UIView!
    @IBOutlet weak var yearlyContainerView: UIView!
    @IBOutlet weak var monthlyContainerView: UIView!
    @IBOutlet weak var yearlyButton: UIView!
    @IBOutlet weak var mounthlyButton: UIView!
    @IBOutlet weak var ContinueButton: UIButton!
    @IBOutlet var mainContentView: UIView!
    @IBOutlet weak var yearlyPrice: UILabel!
    @IBOutlet weak var monthlyPrice: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var subscriptionHeaderText: UILabel!
    @IBOutlet weak var subscriptionDescriptionText: UILabel!
    
    @IBOutlet weak var mountlyButtonCons: NSLayoutConstraint!
    @IBOutlet weak var yearlyButtonConst: NSLayoutConstraint!
    @IBOutlet weak var headerCons: NSLayoutConstraint!
    
    @IBOutlet weak var restoreText: UIButton!
    @IBOutlet weak var yearText: UILabel!
    @IBOutlet weak var yearSaveText: UILabel!
    @IBOutlet weak var monthText: UILabel!
    @IBOutlet weak var monthFreeText: UILabel!
    @IBOutlet weak var commintmentText: UILabel!
    @IBOutlet weak var PrivacyText: UIButton!
    @IBOutlet weak var TermsText: UIButton!
    @IBOutlet weak var yearlyBg: UIImageView!
    
    @IBOutlet weak var monthlyBg: UIImageView!
    
    @IBOutlet var watchVideoPopup: UIView!
    @IBOutlet var blackView: UIView!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var adPopupTitle: UILabel!
    @IBOutlet weak var adPopupDescription: UILabel!
    
    @IBOutlet weak var stackContentView: UIView!
    
    @IBOutlet weak var backView: UIView!
    
    //Color Variables
    var ColorOne = ""
    var ColorTwo = ""
    var ColorTree = ""

    var BgColorOne = ""
    var BgColorTwo = ""
    var BgColorTree = ""
    
    var player : AVPlayer!
    var avPlayerLayer : AVPlayerLayer!
    var packagesAvailableForPurchase = [Purchases.Package]()
    
    var selectedPackage = 0
    private var interstitial: GADInterstitialAd?
    var rewardedAd: GADRewardedAd?

    var type: Int?
    var takenPhoto: UIImage!
    var effectTitle: String = ""
    var adWatched = false
    var model = [SubscriptionModel]()
    var isMonthActive = false
    var isYearActive = false
    var duplicateArray = [""]
    
    let button = UIView()
    let secondButton = UIView()
    let thirdBorderButton = UIView()
    var backgroundBlackView : UIView!
    var isClicked = false
    var subscriptionType = 0
    var activityIndicator : NVActivityIndicatorView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.UISetup()
    }
    
    func UISetup() {
        self.backgroundBlackView = UIView()
        self.backgroundBlackView.bounds = self.view.bounds
        self.yearSaveText.fadeOut()
        self.monthFreeText.fadeOut()
        self.yearlyBg.fadeOut()
        self.monthlyBg.fadeOut()
        self.yearlyContainerView.fadeOut()
        self.monthlyContainerView.fadeOut()
        self.watchVideoPopup.isHidden = true
        self.watchVideoPopup.alpha = 0
        self.blackView.isHidden = true
        self.blackView.alpha = 0
        self.blackView.bounds = self.view.bounds
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.removeBlackView))
        self.blackView.addGestureRecognizer(gesture)
        let backGesture = UITapGestureRecognizer(target: self, action: #selector(self.backGestureObserver))
        self.backView.addGestureRecognizer(backGesture)
        playVideo()
        let request = GADRequest()
           GADInterstitialAd.load(withAdUnitID:"ca-app-pub-9196338242827894/8954200443",
                                       request: request,
                             completionHandler: { [self] ad, error in
                               if let error = error {
                                 print("Failed to load interstitial ad with error: \(error.localizedDescription)")
                                 return
                               }
                               interstitial = ad
                               interstitial?.fullScreenContentDelegate = self
                             }
           )
        getPayment()

        self.setupScrollLifeCycle()
        restoreText.setTitle("Restore".localized(), for: .normal)
        commintmentText.text = "No commitment. Cancel anytime".localized()
        PrivacyText.setTitle("Privacy Policy".localized(), for: .normal)
        TermsText.setTitle("Terms of Service".localized(), for: .normal)
        self.closeButton.isHidden = true
        _ = Timer.scheduledTimer(withTimeInterval: Definations.subscriptionCloseAppearTime, repeats: false) { timer in
            self.closeButton.isHidden = false
        }
        
       
        okButton.layer.cornerRadius = 15
        okButton.setTitle("Yes".localized(), for: .normal)
        cancelButton.setTitle("Cancel".localized(), for: .normal)
        adPopupTitle.text = "Free Access".localized()
        adPopupDescription.text = "Watch a video to get one-time access?".localized()
        subscriptionHeaderText.text = Definations.subscriptionHeader
        subscriptionDescriptionText.text = Definations.subscriptionDescription
        
        subscriptionHeaderText.textColor = hexStringToUIColor(hex: Definations.subscriptionCTATextColor)
        ContinueButton.setTitle(Definations.subscriptionCTATitle, for: .normal)
        if !Definations.subscriptionCTAButtonColor.contains(",") {
            if Definations.subscriptionCTAButtonColor.count > 0 {
                ContinueButton.backgroundColor = hexStringToUIColor(hex: Definations.subscriptionCTAButtonColor)
            }
        } else {
            DispatchQueue.main.async { [self] in
                let yourString = Definations.subscriptionCTAButtonColor
                 
                let splitedCodes = yourString.split(separator: ",")

                if splitedCodes.count > 3 || splitedCodes.count == 3 {
                    ColorOne = String(splitedCodes[0])
                    ColorTwo = String(splitedCodes[1])
                    ColorTree =  String(splitedCodes[2])
                    
                    ContinueButton.applyGradient(colours: [
                                                    hexStringToUIColor(hex: ColorOne),
                                                    hexStringToUIColor(hex: ColorTwo),
                                                    hexStringToUIColor(hex: ColorTree)
                    ])
                } else if splitedCodes.count > 2 || splitedCodes.count == 2 {
                    ColorOne = String(splitedCodes[0])
                    ColorTwo = String(splitedCodes[1])
                    
                    ContinueButton.applyGradient(colours: [
                                                    hexStringToUIColor(hex: ColorOne),
                                                    hexStringToUIColor(hex: ColorTwo),
                    ])
                }
            }
        }
        
        if Definations.subscriptionBGcolor.count > 0 {
            if !Definations.subscriptionBGcolor.contains(",") {
                self.view.backgroundColor = hexStringToUIColor(hex: Definations.subscriptionBGcolor)
            } else {
                DispatchQueue.main.async { [self] in
                    let yourString = Definations.subscriptionBGcolor
                     
                    let splitedCodes = yourString.split(separator: ",")

                    if splitedCodes.count > 3 || splitedCodes.count == 3 {
                        BgColorOne = String(splitedCodes[0])
                        BgColorTwo = String(splitedCodes[1])
                        BgColorTree =  String(splitedCodes[2])
                        
                        self.view.applyGradient(colours: [
                                                        hexStringToUIColor(hex: BgColorOne),
                                                        hexStringToUIColor(hex: BgColorTwo),
                                                        hexStringToUIColor(hex: BgColorTree)
                        ])
                    } else if splitedCodes.count > 2 || splitedCodes.count == 2 {
                        BgColorOne = String(splitedCodes[0])
                        BgColorTwo = String(splitedCodes[1])
                        
                        self.view.applyGradient(colours: [
                                                        hexStringToUIColor(hex: BgColorOne),
                                                        hexStringToUIColor(hex: BgColorTwo),
                        ])
                    }
                }
            }
            
        } else {
            let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
            backgroundImage.image = UIImage(named: "Gradient")
            backgroundImage.contentMode = .scaleAspectFill
            self.view.insertSubview(backgroundImage, at: 0)
        }
        
        self.ContinueButton.setTitle(Definations.subscriptionCTATitle, for: .normal)
        self.mainContentView.bounds = self.scrollView.bounds
        scrollView.sizeMatching = .Dynamic( width: { self.view.frame.width },
                                             height: { self.calculateHeight() }
        )
        
        /*
         let rect = CGRect(x: 0, y: 0, width: sliderView.frame.width , height: sliderView.frame.height)
         let firstImage = UIImage(named: "after")!
         let secondImage = UIImage(named: "before")!
         */
       
        let yearlyGesture = UITapGestureRecognizer(target: self, action: #selector(self.yearlyButtonAction(_:)))
        let monthlyGesture = UITapGestureRecognizer(target: self, action: #selector(self.monthlyButtonAction(_:)))
        
        self.yearlyButton.addGestureRecognizer(yearlyGesture)
        self.mounthlyButton.addGestureRecognizer(monthlyGesture)
        
        setupYearly()
        
        self.yearlyButton.layer.cornerRadius = 8
        self.mounthlyButton.layer.cornerRadius = 8
        self.ContinueButton.layer.cornerRadius = 8
        
        TermsText.titleLabel?.numberOfLines = 0
        TermsText.titleLabel?.adjustsFontSizeToFitWidth = true
        TermsText.titleLabel?.lineBreakMode = .byWordWrapping
        TermsText.titleLabel?.textAlignment = .center
        
        PrivacyText.titleLabel?.numberOfLines = 0
        PrivacyText.titleLabel?.adjustsFontSizeToFitWidth = true
        PrivacyText.titleLabel?.lineBreakMode = .byWordWrapping
        PrivacyText.titleLabel?.textAlignment = .center
    }
    
    func calculateHeight() -> CGFloat {
        var height = self.sliderView.frame.height
        
        height = height + self.mounthlyButton.frame.height + self.ContinueButton.frame.height + 400
        
        return height
    }
    
    @IBAction func backAction(_ sender: Any) {
        if self.type == 1 || self.type == 3 || self.type == 6 {
            self.watchVideoPopup.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
            self.watchVideoPopup.alpha = 0
            self.watchVideoPopup.layer.cornerRadius = 16
            
            self.view.addSubview(self.blackView)
            
            self.blackView.center = self.view.center
            self.view.addSubview(self.watchVideoPopup)
            self.watchVideoPopup.center = self.view.center
            self.watchVideoPopup.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
            self.watchVideoPopup.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
                self.watchVideoPopup.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                self.watchVideoPopup.isHidden = false
                self.watchVideoPopup.alpha = 1
                self.blackView.isHidden = false
                self.blackView.alpha = 0.50
            })
        } else if self.type == 2 || self.type == 4 || self.type == 7 {
            if interstitial != nil {
                interstitial?.present(fromRootViewController: self)
             } else {
                self.dismiss(animated: true, completion: nil)
             }
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func backGestureObserver() {
        if self.type == 1 || self.type == 3 || self.type == 6 {
            self.watchVideoPopup.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
            self.watchVideoPopup.alpha = 0
            self.watchVideoPopup.layer.cornerRadius = 16
            
            self.view.addSubview(self.blackView)
            
            self.blackView.center = self.view.center
            self.view.addSubview(self.watchVideoPopup)
            self.watchVideoPopup.center = self.view.center
            self.watchVideoPopup.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
            self.watchVideoPopup.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
                self.watchVideoPopup.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                self.watchVideoPopup.isHidden = false
                self.watchVideoPopup.alpha = 1
                self.blackView.isHidden = false
                self.blackView.alpha = 0.50
            })
        } else if self.type == 2 || self.type == 4 || self.type == 7 {
            if interstitial != nil {
                interstitial?.present(fromRootViewController: self)
             } else {
                self.dismiss(animated: true, completion: nil)
             }
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func removeBlackView() {
        removePopup(desiredView: self.watchVideoPopup)
        removePopup(desiredView: self.blackView)
    }
    
    
    func setupAndShowIndicatorForAD() {
        DispatchQueue.main.async { [self] in
            backgroundBlackView.backgroundColor = .black.withAlphaComponent(0.50)
            backgroundBlackView.center = self.view.center
            self.view.addSubview(backgroundBlackView)
            
            let xAxis = self.view.bounds.size.width / 2;
            let yAxis = self.view.bounds.size.height / 3;

            let frame = CGRect(x: xAxis-20, y: yAxis, width: 25, height: 25)
            activityIndicator = NVActivityIndicatorView(frame: frame, type: .lineScale, color: .black)
            
            activityIndicator.startAnimating()
            
            self.backgroundBlackView.addSubview(activityIndicator)
            activityIndicator.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: 0).isActive = true
            activityIndicator.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
            self.activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        }
    }
    
   func setupGoogleAD() {
        if self.type == 12 {
            print(type)
        } else {
            self.view.isUserInteractionEnabled = false
            setupAndShowIndicatorForAD()
            loadRewardedAd()
        }
    }
    
    @IBAction func restoreAction(_ sender: Any) {
        setupAndShowIndicator()
        Purchases.shared.restoreTransactions { (purchaserInfo, error) in
            //... check purchaserInfo to see if entitlement is now active
            self.view.isUserInteractionEnabled = true
            self.activityIndicator.stopAnimating()
            self.activityIndicator.removeFromSuperview()
            self.backgroundBlackView.removeFromSuperview()
            self.backgroundBlackView.alpha = 0
            if purchaserInfo?.entitlements.all["PixCam_Premium"]?.isActive == true {
                // Unlock that great "pro" content
                NotificationCenter.default.post(name: NSNotification.Name("User is Premium"), object: nil)
                UserDefaults.standard.set(true, forKey: "isPremium")
                Definations.isPremiumUser = UserDefaults.standard.bool(forKey: "isPremium")
                if self.type == 1 || self.type == 2 || self.type == 4 || self.type == 7 {
                    DispatchQueue.main.async {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ShareViewController") as! ShareViewController
                        vc.modalPresentationStyle = .fullScreen
                        vc.takenPhoto = self.takenPhoto
                        vc.effectTitle = self.effectTitle
                        self.present(vc, animated: true, completion: nil)
                    }
                } else if self.type == 3 {
                    DispatchQueue.main.async {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ShareViewController") as! ShareViewController
                        vc.modalPresentationStyle = .fullScreen
                        vc.takenPhoto = self.takenPhoto
                        vc.effectTitle = self.effectTitle
                        vc.type = 5
                        self.present(vc, animated: true, completion: nil)
                    }
                } else if self.type == 6 {
                    DispatchQueue.main.async {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ShareViewController") as! ShareViewController
                        vc.modalPresentationStyle = .fullScreen
                        vc.takenPhoto = self.takenPhoto
                        vc.effectTitle = self.effectTitle
                        vc.type = 8
                        self.present(vc, animated: true, completion: nil)
                    }
                } else {
                    self.dismiss(animated: true, completion: nil)
                }
            } else {
                self.view.isUserInteractionEnabled = true
                self.activityIndicator.removeFromSuperview()
                self.backgroundBlackView.removeFromSuperview()
                self.backgroundBlackView.alpha = 0
                self.activityIndicator.stopAnimating()
            }
        }
    }
    
    @IBAction func continueAction(_ sender: UIView) {
        setupAndShowIndicator()
        let package = self.packagesAvailableForPurchase[self.selectedPackage]
        Purchases.shared.purchasePackage(package) { (transaction, purchaserInfo, error, userCancelled) in
            self.view.isUserInteractionEnabled = true
            if userCancelled {
                self.activityIndicator.stopAnimating()
                self.activityIndicator.removeFromSuperview()
                self.backgroundBlackView.removeFromSuperview()
                self.backgroundBlackView.alpha = 0
            }
            if purchaserInfo?.entitlements.all["PixCam_Premium"]?.isActive == true {
                // Unlock that great "pro" content
                NotificationCenter.default.post(name: NSNotification.Name("User is Premium"), object: nil)
                UserDefaults.standard.set(true, forKey: "isPremium")
                Definations.isPremiumUser = UserDefaults.standard.bool(forKey: "isPremium")
                if self.type == 1 || self.type == 2 || self.type == 4 || self.type == 7 {
                    DispatchQueue.main.async {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ShareViewController") as! ShareViewController
                        vc.modalPresentationStyle = .fullScreen
                        vc.takenPhoto = self.takenPhoto
                        vc.effectTitle = self.effectTitle
                        self.present(vc, animated: true, completion: nil)
                    }
                } else if self.type == 3 {
                    DispatchQueue.main.async {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ShareViewController") as! ShareViewController
                        vc.modalPresentationStyle = .fullScreen
                        vc.takenPhoto = self.takenPhoto
                        vc.effectTitle = self.effectTitle
                        vc.type = 5
                        self.present(vc, animated: true, completion: nil)
                    }
                } else if self.type == 6 {
                    DispatchQueue.main.async {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ShareViewController") as! ShareViewController
                        vc.modalPresentationStyle = .fullScreen
                        vc.takenPhoto = self.takenPhoto
                        vc.effectTitle = self.effectTitle
                        vc.type = 8
                        self.present(vc, animated: true, completion: nil)
                    }
                } else {
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    func loadRewardedAd() {
            let adUnitID = "ca-app-pub-9196338242827894/1227753661"
            let request = GADRequest()
            GADRewardedAd.load(withAdUnitID: adUnitID, request: request) { [weak self](rewardedAd, error) in
               
            if let error = error {
                print("Failed to load rewarded ad with error: \(error.localizedDescription)")
                return
            }
               
            self?.rewardedAd = rewardedAd

            self?.activityIndicator.stopAnimating()
                
            self?.activityIndicator.fadeOut()
                
            self?.view.isUserInteractionEnabled = true
                
            self?.rewardedAd?.fullScreenContentDelegate = self
                
            self?.presentRewardedVideo()
        }
    }

    func presentRewardedVideo() {
       guard let rewardedAd = rewardedAd else { return }

       rewardedAd.present(fromRootViewController: self) {
           let reward = rewardedAd.adReward
           print("Reward received with currency \(reward.amount), amount \(reward.amount.doubleValue)")

           print("*** User Watched the Entire Video - Now Do Something ***")
           self.adWatched = true
       }
    }
    
    @IBAction func watchAdVideo(_ sender: Any) {
        setupGoogleAD()
    }
    
    @IBAction func closeAdPopup(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func privacyAction(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyViewController") as! PrivacyViewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func termsAction(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TermsViewController") as! TermsViewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    //MARK: Make Video
    private func playVideo() {
        guard let path = Bundle.main.path(forResource: "subscriptionVideo", ofType:"mp4") else {
            debugPrint("not found")
            return
        }
        player = AVPlayer(url: URL(fileURLWithPath: path))
        avPlayerLayer = AVPlayerLayer(player: player)
        avPlayerLayer.videoGravity = AVLayerVideoGravity.resize
        avPlayerLayer.repeatCount = 400
        avPlayerLayer.repeatDuration = 0
        avPlayerLayer.frame = sliderView.layer.bounds
        sliderView.layer.addSublayer(avPlayerLayer)
        NotificationCenter.default.addObserver(forName: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil, queue: nil) { notification in
            self.player.seek(to: CMTime.zero)
            self.player.play()
        }
        player.play()
     }
    
    //MARK: SlideLifeCycle
    func setupScrollLifeCycle() {
        DispatchQueue.main.async {
            switch UIDevice().type {
            case .iPhone8, .iPhoneSE2, .iPhoneSE, .iPhone8Plus, .iPhone6, .iPhone6S, .iPhone6SPlus, .iPhone7, .iPhone7Plus:
                DispatchQueue.main.async { [self] in
                    mountlyButtonCons.constant = self.view.frame.width / 2.0 - 28
                    yearlyButtonConst.constant = self.view.frame.width / 2.0 - 28
                    
                    headerCons.constant = 12
                }
                self.scrollView.isScrollEnabled = true
            default:
                DispatchQueue.main.async { [self] in
                    mountlyButtonCons.constant = self.view.frame.width / 2.0 - 28
                    yearlyButtonConst.constant = self.view.frame.width / 2.0 - 28
                }
                self.scrollView.isScrollEnabled = false
            }
        }
    }
    
    //MARK: ButtonsActivity
    @objc func monthlyButtonAction(_ sender: UIView) {
        self.selectedPackage = 1
        setupMonthly()
    }
    
    @objc func yearlyButtonAction(_ sender: UIView) {
        self.selectedPackage = 0
        setupYearly()
    }
    
    func setupMonthly() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.35) {
                self.monthlyContainerView.layer.borderWidth = 1
                self.monthlyContainerView.layer.cornerRadius = 8
                self.monthlyContainerView.layer.borderColor = UIColor.white.cgColor
                
                self.yearlyContainerView.layer.borderColor = UIColor.clear.cgColor
            }
        }
    }
    
    func setupYearly() {
        DispatchQueue.main.async {
            self.yearlyContainerView.layer.borderWidth = 1
            self.yearlyContainerView.layer.cornerRadius = 8
            self.yearlyContainerView.layer.borderColor = UIColor.white.cgColor
            
            self.monthlyContainerView.layer.borderColor = UIColor.clear.cgColor
        }
    }
    
    //MARK: SetupButtons
    func getPayment() {
        DispatchQueue.main.async { [self] in
            self.setupPaymentItems { isEndWithSuccess in
                if isEndWithSuccess {
                    self.mainStackView.spacing = 3
                    for item in model {
                        print("orhan = ", item)
                        self.createSubscriptionButtons(subsModel: item)
                    }
                } else {
                    print("Orhan Error")
                }
            }
        }
    }
    
    func setupAndShowIndicator() {
        DispatchQueue.main.async { [self] in
            backgroundBlackView.backgroundColor = .black.withAlphaComponent(0.50)
            backgroundBlackView.center = self.view.center
            self.backgroundBlackView.alpha = 1
            self.view.addSubview(backgroundBlackView)
            
            let xAxis = self.view.bounds.size.width / 2;
            let yAxis = self.view.bounds.size.height / 3;

            let frame = CGRect(x: xAxis-20, y: yAxis, width: 25, height: 25)
            activityIndicator = NVActivityIndicatorView(frame: frame, type: .lineScale, color: .black)
            
            activityIndicator.startAnimating()
            self.view.isUserInteractionEnabled = false

            self.ContinueButton.addSubview(activityIndicator)
            self.activityIndicator.rightAnchor.constraint(equalTo: self.ContinueButton.rightAnchor, constant: -16).isActive = true
            self.activityIndicator.centerYAnchor.constraint(equalTo: self.ContinueButton.centerYAnchor, constant: 0).isActive = true
            self.activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        }
    }
}


extension SubscriptionViewController: GADFullScreenContentDelegate {
    
    /// Tells the delegate that the ad failed to present full screen content.
    func ad(_ ad: GADFullScreenPresentingAd, didFailToPresentFullScreenContentWithError error: Error) {
      print("Ad did fail to present full screen content.")
    }

    /// Tells the delegate that the ad presented full screen content.
    func adDidPresentFullScreenContent(_ ad: GADFullScreenPresentingAd) {
      print("Ad did present full screen content.")
    }
    
    /// Tells the delegate that an impression has been recorded for the ad.
    func adDidRecordImpression(_ ad: GADFullScreenPresentingAd) {
        print("0. impression recorded")
    }

    
    /// Tells the delegate that the ad will dismiss full screen content.
    func adWillDismissFullScreenContent(_ ad: GADFullScreenPresentingAd) {
        if self.adWatched && self.type == 1 {
            DispatchQueue.main.async {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ShareViewController") as! ShareViewController
                vc.modalPresentationStyle = .fullScreen
                vc.takenPhoto = self.takenPhoto
                vc.effectTitle = self.effectTitle
                vc.type = 9
                self.present(vc, animated: true, completion: nil)
            }
        } else if self.type == 2 {
            DispatchQueue.main.async {
                self.dismiss(animated: true, completion: nil)
            }
        } else if self.adWatched && self.type == 3 {
            DispatchQueue.main.async {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ShareViewController") as! ShareViewController
                vc.modalPresentationStyle = .fullScreen
                vc.takenPhoto = self.takenPhoto
                vc.effectTitle = self.effectTitle
                vc.type = 5
                self.present(vc, animated: true, completion: nil)
            }
        } else if self.adWatched && self.type == 4 {
            DispatchQueue.main.async {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ShareViewController") as! ShareViewController
                vc.modalPresentationStyle = .fullScreen
                vc.takenPhoto = self.takenPhoto
                vc.effectTitle = self.effectTitle
                self.present(vc, animated: true, completion: nil)
            }
        } else if self.adWatched && self.type == 6 {
            DispatchQueue.main.async {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ShareViewController") as! ShareViewController
                vc.modalPresentationStyle = .fullScreen
                vc.takenPhoto = self.takenPhoto
                vc.effectTitle = self.effectTitle
                vc.type = 8
                self.present(vc, animated: true, completion: nil)
            }
        } else if self.adWatched && self.type == 7 {
            DispatchQueue.main.async {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ShareViewController") as! ShareViewController
                vc.modalPresentationStyle = .fullScreen
                vc.takenPhoto = self.takenPhoto
                vc.effectTitle = self.effectTitle
                self.present(vc, animated: true, completion: nil)
            }
        } else {
            DispatchQueue.main.async {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    /// Tells the delegate that a click has been recorded for the ad.
    func adDidRecordClick(_ ad: GADFullScreenPresentingAd) {
        print("4. impression click detected")
    } 
}

 
